import React from "react";

const LoadingSpinne = () => {
  return (
    <div className="LoadingQuotes">
      <div col-4>
        <img width="80px" src="/images/spinner.gif" alt="" />
        <h4>Processing</h4>
      </div>
    </div>
  );
};

export default LoadingSpinne;

import React from "react";
import { Link } from "react-router-dom";
import ClearIcon from "@material-ui/icons/Clear";
import MenuIcon from "@material-ui/icons/Menu";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";

import { useSelector } from "react-redux";
import {
  SETLIFEPROCESS,
  SETCARPROCESSINFO,
  SETHEALTHSURANCEPROCESSINFO,
  SETTRAVELINSURANCEPROCESSINFO,SETINSURANCETYPE
} from "../redux/action";
const SideNav = styled.div`
  z-index: 3;
  width: 300px;
  height: 100vh;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: 20px;
  padding-top: 3px;
  position: absolute;
  background-color: white;
  right: 0;
  top: 0;
  animation: apper 1.2s ease;
  animation-iteration-count: 1;

  @keyframes apper {
    from {
      opacity: 0;
    }
    to {
      top: 1;
    }
  }
`;
const Listing = styled.ul`
  display: flex;
  margin: 0;
  list-style-type: none;
  padding: 0;

  flex-direction: column;
  align-items: flex-start;
  width: 98%;
  li {
    display: flex;
    font-size: 16px;
    border-bottom: 0.1px solid silver;
    width: 100%;
    color: black;
    padding: 10px;
    // &:nth-child(1) {
    //   font-size: 20px;
    //   color: grey;
    // }
    // &:nth-child(2) {
    //   font-size: 30px;
    //   color: black;
    //   font-weight: 600;
    //   @media (max-width: 1100px) {
    //     font-size: 30px;
    //   }
    // }
    // &:nth-child(3) {
    //   font-size: 20px;
    //   color: grey;
    //   font-weight: 500;
    //   @media (max-width: 1100px) {
    //     font-size: 20px;
    //   }
    // }
  }
`;
const MenuIconDiv = styled.div`
  // margin-right: 20px;
  cursor: pointer;
  @media (min-width: 930px) {
    display: none;
  }
`;
const HideMobile = styled.span`
  @media (max-width: 930px) {
    display: none;
  }
`;
const Filter = styled.div`
  width: 100%;
  height: 100vh;
  background-color: grey;
  position: absolute;
  z-index: 1;
  opacity: 0.6;
  top: 0;
  left: 0;
`;
const HeaderOne = () => {
  const [sidebarVisible, setSidebarVisible] = React.useState(false);
  const [focusePane, setFocusedPane] = React.useState('personal');
  const [footbarVisible, setFootbarVisible] = React.useState(false);
  const history = useHistory();

  const dispatch = useDispatch();
  const handleOpenSideBar = () => {
    setSidebarVisible(!sidebarVisible);
  };
  const currentUser = useSelector(({ user }) => user.currentUser);
  const HandleSelection = (value, link) => {
    history.push({
      pathname: link,
      state: value,
    });
    dispatch(SETLIFEPROCESS({ cover_type: value }));
  };

  const handleSwicthPane=()=>{
    focusePane==="business"&&setFocusedPane('personal')
    focusePane==="business"&&dispatch(SETINSURANCETYPE("personal-insurance"));
    focusePane==="personal"&&setFocusedPane('business')
     focusePane==="personal"&&dispatch(SETINSURANCETYPE("business-insurance"));
   
  }
 
  const PersonalPage=()=>    <div  class="products-overlay">
                 
  <ul className="">
    <a href="/life-insurance/#choose-us">
      <li>
        <img
          width="30px"
          src="/images/life.png"
          alt=""
          style={{ marginRight: "10px" }}
        />
        Life Insurance
      </li>
    </a>
    <li
      onClick={HandleSelection.bind(
        this,
        "Term Life Insurance",
        "/life-insurance-step1"
      )}
    >
      Term Life Insurance
    </li>
    <li
      onClick={HandleSelection.bind(
        this,
        "Permanent Insurance",
        "/life-insurance-step1"
      )}
    >
      Permanent Insurance
    </li>
  </ul>
  <ul className="">
    <a href="/auto-insurance/#choose-us">
      <li>
        <img
          width="30px"
          src="/images/auto.png"
          alt=""
          style={{ marginRight: "10px" }}
        />
        Auto Insurance
      </li>
    </a>
    <li
      onClick={() => {
        history.push({
          pathname: "/car-insurance-step1",
        });
        dispatch(
          SETCARPROCESSINFO({
            product_type: "commercial  Vehicle",
            insurance_type: "commercial Vehicle",
          })
        );
      }}
      href="/car-insurance-step1"
    >
      Commercial Vehicle
      <br />
      Insurance
    </li>
    <li
      onClick={() => {
        history.push({
          pathname: "/car-insurance-step1",
        });

        dispatch(
          SETCARPROCESSINFO({
            product_type: "non_commercial  Vehicle",
            insurance_type: "non_commercial Vehicle",
          })
        );
      }}
    >
      {" "}
      Private Vehicle
      <br /> Insurance
    </li>
    <li
      onClick={() => {
        history.push({
          pathname: "/bike-insurance-step0",
        });

        // dispatch(
        //   SETCARPROCESSINFO({
        //     product_type: "non_commercial  Vehicle",
        //     insurance_type: "non_commercial Vehicle",
        //   })
        // );
      }}
    >
      Bike Insurance
    </li>
    <li
      onClick={() => {
        history.push({
          pathname: "/three-insurance-step0",
        });

        // dispatch(
        //   SETCARPROCESSINFO({
        //     product_type: "non_commercial  Vehicle",
        //     insurance_type: "non_commercial Vehicle",
        //   })
        // );
      }}
    >
      Three wheeler Insurance
    </li>
  </ul>
  <ul className="">
    <a href="/Gadget-insurance/#choose-us">
      <li>
        {" "}
        <img
          width="30px"
          src="/images/gadget.png"
          alt=""
          style={{ marginRight: "10px" }}
        />
        Gadget Insurance
      </li>
    </a>
    <li
      onClick={HandleSelection.bind(
        this,
        "mobile phone",
        "/Gadget-insurance-step1"
      )}
    >
      Mobile Phone Insurance
    </li>
    <li
      onClick={HandleSelection.bind(
        this,
        "Laptop",
        "/Gadget-insurance-step1"
      )}
    >
      Laptop Insurance
    </li>
    <li
      onClick={HandleSelection.bind(
        this,
        "Digital Camera",
        "/Gadget-insurance-step1"
      )}
    >
      Digital Camera Insurance
    </li>
    <li
      onClick={HandleSelection.bind(
        this,
        "Tablets",
        "/Gadget-insurance-step1"
      )}
    >
      Tablets Insurance
    </li>
    <li
      onClick={HandleSelection.bind(
        this,
        "Pc",
        "/Gadget-insurance-step1"
      )}
    >
      Pc Insurance
    </li>
    <li
      onClick={HandleSelection.bind(
        this,
        "Ipods",
        "/Gadget-insurance-step1"
      )}
    >
      Ipod Insurance
    </li>
  </ul>
  <ul className="">
    <a href="/health-insurance/#choose-us">
      <li>
        {" "}
        <img
          width="30px"
          src="/images/health.png"
          alt=""
          style={{ marginRight: "10px" }}
        />
        Health Insurance
      </li>
    </a>
    <li
      onClick={() => {
        history.push({
          pathname: "/health-insurance-step1",
          state: "Preferred Provider Organization",
        });
        dispatch(
          SETHEALTHSURANCEPROCESSINFO({
            insurance_type:
              " Preferred Provider Organization",
          })
        );
      }}
    >
      Preferred Provider Organization
    </li>
    <li
      onClick={() => {
        history.push("/health-insurance-step1");
        dispatch(
          SETHEALTHSURANCEPROCESSINFO({
            insurance_type: "Health Maintenance Organization",
          })
        );
      }}
    >
      Health Maintenance Organization
    </li>
    <li
      onClick={() => {
        history.push({
          pathname: "/health-insurance-step1",
          state: "Point of Service",
        });
        dispatch(
          SETHEALTHSURANCEPROCESSINFO({
            insurance_type: "Point of Service",
          })
        );
      }}
    >
      Point of Service
    </li>
    <li
      onClick={() => {
        history.push({
          pathname: "/health-insurance-step1",
          state: "Exclusive Provider  Organization",
        });
        dispatch(
          SETHEALTHSURANCEPROCESSINFO({
            insurance_type:
              "Exclusive Provider  Organization",
          })
        );
      }}
    >
      Exclusive Provider Organization
    </li>
    <li
      onClick={() => {
        history.push({
          pathname: "/health-insurance-step1",
          state: "Indemnity",
        });
        dispatch(
          SETHEALTHSURANCEPROCESSINFO({
            insurance_type: "Indemnity",
          })
        );
      }}
    >
      Indemnity Insurance
    </li>
    <li
      onClick={() => {
        history.push({
          pathname: "/health-insurance-step1",
          state: "Health Savings Account",
        });
        dispatch(
          SETHEALTHSURANCEPROCESSINFO({
            insurance_type: "Health Savings Account",
          })
        );
      }}
    >
      Health Savings Account
    </li>
  </ul>
  <ul className="">
    <a href="/Home-insurance/#choose-us">
      <li>
        <img
          width="30px"
          src="/images/home.png"
          alt=""
          style={{ marginRight: "10px" }}
        />
        Home Insurance
      </li>
    </a>
    <a href="/home-insurance-step1">
      <li>Contents Insurance</li>
    </a>
    <a href="/home-insurance-step1">
      <li>Buildings Insurance</li>
    </a>
    <a href="/home-insurance-step1">
      <li>Combined Insurancee</li>
    </a>
  </ul>
  <ul className="">
    <a href="/Travel-insurance/#choose-us">
      <li>
        {" "}
        <img
          width="30px"
          src="/images/travel.png"
          alt=""
          style={{ marginRight: "10px" }}
        />
        Travel Insurance
      </li>
    </a>

    <li
      onClick={() => {
        history.push("/travel-insurance-step1");
        dispatch(
          SETTRAVELINSURANCEPROCESSINFO({
            insurance_type: "Single Trip Plan",
          })
        );
      }}
    >
      Single Trip Plan
    </li>

    <li
      onClick={() => {
        history.push("/travel-insurance-step1");
        dispatch(
          SETTRAVELINSURANCEPROCESSINFO({
            insurance_type: "Multi Trip Plan",
          })
        );
      }}
    >
      Multi Trip Plan
    </li>
  </ul>
</div>

const BusinessPage=()=>    <div  class="products-overlay">
                 

<ul className="">
  <a href="/auto-insurance/#choose-us">
    <li>
      <img
        width="30px"
        src="/images/auto.png"
        alt=""
        style={{ marginRight: "10px" }}
      />
      Auto Insurance
    </li>
  </a>
  <li
    onClick={() => {
      history.push({
        pathname: "/car-insurance-step1",
      });
      dispatch(
        SETCARPROCESSINFO({
          product_type: "commercial  Vehicle",
          insurance_type: "commercial Vehicle",
        })
      );
    }}
    href="/car-insurance-step1"
  >
    Commercial Vehicle
    <br />
    Insurance
  </li>

  <li
    onClick={() => {
      history.push({
        pathname: "/bike-insurance-step0",
      });

      // dispatch(
      //   SETCARPROCESSINFO({
      //     product_type: "non_commercial  Vehicle",
      //     insurance_type: "non_commercial Vehicle",
      //   })
      // );
    }}
  >
    Bike Insurance
  </li>
  <li
    onClick={() => {
      history.push({
        pathname: "/three-insurance-step0",
      });

      // dispatch(
      //   SETCARPROCESSINFO({
      //     product_type: "non_commercial  Vehicle",
      //     insurance_type: "non_commercial Vehicle",
      //   })
      // );
    }}
  >
    Three wheeler Insurance
  </li>
</ul>





<ul className="">
  <a href="/Travel-insurance/#choose-us">
    <li>
      {" "}
      <img
        width="30px"
        src="/images/travel.png"
        alt=""
        style={{ marginRight: "10px" }}
      />
      Travel Insurance
    </li>
  </a>

  <li
    onClick={() => {
      history.push("/travel-insurance-step1");
      dispatch(
        SETTRAVELINSURANCEPROCESSINFO({
          insurance_type: "Single Trip Plan",
        })
      );
    }}
  >
    Single Trip Plan
  </li>

  <li
    onClick={() => {
      history.push("/travel-insurance-step1");
      dispatch(
        SETTRAVELINSURANCEPROCESSINFO({
          insurance_type: "Multi Trip Plan",
        })
      );
    }}
  >
    Multi Trip Plan
  </li>
</ul>
<ul className="">
  <a href="/health-insurance-step1">
    <li>
      {" "}
      <img
        width="30px"
        src="/images/health.png"
        alt=""
        style={{ marginRight: "10px" }}
      />
      Medical Insurance
    </li>
  </a>
  {/* <li
    onClick={() => {
      history.push({
        pathname: "/health-insurance-step1",
        state: "Preferred Provider Organization",
      });
      dispatch(
        SETHEALTHSURANCEPROCESSINFO({
          insurance_type:
            " Preferred Provider Organization",
        })
      );
    }}
  >
    Preferred Provider Organization
  </li> */}
 
 

  
</ul>
<ul className="">
  <a href="/Home-insurance/#choose-us">
    <li>
      <img
        width="30px"
        src="/images/home.png"
        alt=""
        style={{ marginRight: "10px" }}
      />
      Home Insurance
    </li>
  </a>
  <a href="/home-insurance-step1">
    <li>Contents Insurance</li>
  </a>
  <a href="/home-insurance-step1">
    <li>Buildings Insurance</li>
  </a>
  <a href="/home-insurance-step1">
    <li>Combined Insurancee</li>
  </a>
</ul>
</div>

  return (
    <header>
      {/* how-it-works */}
      <nav className="nav-bar align-items-center">
        {sidebarVisible ? <Filter onClick={handleOpenSideBar} /> : null}
        <Link to="/">
          <h4 className="navTitle">iPolicyMart</h4>
        </Link>
        <div className="option-section align-items-center">
          <div className="nav-options">
            <ul>
              {/* <li className="active">Home</li> */}
              <a
                onMouseOver={() => setFootbarVisible(true)}
                onMouseLeave={() => setFootbarVisible(false)}
              >
                <li className="">
                  Products
                  {!footbarVisible ? (
                    <ExpandMoreIcon />
                  ) : (
                    <KeyboardArrowUpIcon />
                  )}
                </li>
                {footbarVisible ? (
               <div className='product-overlay-main'>
              <div className='col-5'>
           <div style={{marginLeft:'100px'}} className='verticalCenterRow '>
           <button onClick={handleSwicthPane}  className={`btn2 curved-Top ${focusePane==='personal'?"overlayFocused":null}`}>Personnal insurance</button>
           <button onClick={handleSwicthPane} className={`btn2 curved-Top ${focusePane==='business'?"overlayFocused":null}`}>Business Insurance</button>
           </div>
              </div>
                 {/* <div style={{width:"100%",height:"1px",backgroundColor:"#eee"}}/> */}
                {focusePane==='personal'?PersonalPage():BusinessPage()}
               </div>
                ) : null}
              </a>
              <a href="/#how-it-works">
                <li>How it works</li>
              </a>
              <Link to="/insurance-claims">
                <li>File a Claim </li>
              </Link>
              <Link to="/renew-policy-step1">
                <li>Renew</li>
              </Link>
              <li>Blog</li>
              <li>Contact us</li>
              <a href="/#about-us">
                <li>About us</li>
              </a>
              {currentUser ? (
                <Link to="/users-dashboard">
                  <li>
                    <AccountCircleIcon />
                  </li>
                </Link>
              ) : (
                <Link to="/login">
                  <li className="btn6">Login</li>
                </Link>
              )}
            </ul>
          </div>

          <div>
            <HideMobile>
              <a href="/#choose-us">
                {" "}
                <button className="btn1">Get Started</button>
              </a>
            </HideMobile>
          </div>
        </div>
        <MenuIconDiv
          onClick={() => {
            handleOpenSideBar();
          }}
        >
          <MenuIcon size="large" style={{ fontSize: "40px" }} />
        </MenuIconDiv>
        {sidebarVisible ? (
          <SideNav>
            <ClearIcon
              onClick={() => {
                handleOpenSideBar();
              }}
              style={{
                alignSelf: "flex-end",
                margin: "10px",
                fontSize: "40px",
                cursor: "pointer",
              }}
              size="large"
            />

            <ul
              onClick={() => {
                handleOpenSideBar();
              }}
              className="Nav-Colapse-listings"
            >
              {/* <li className="active">Home</li> */}
              <li className="">Home</li>
              <a href="/#how-it-works">
                <li>How it works</li>
              </a>
              <Link to="/insurance-claims">
                <li>File a Claim</li>
              </Link>
              <Link to="/renew-policy-step1">
                <li>Renew</li>
              </Link>

              <li>Contact us</li>
              <li>About us</li>
              {currentUser ? (
                <Link to="/users-dashboard">
                  <li>
                    <AccountCircleIcon />
                  </li>
                </Link>
              ) : (
                <Link to="/login">
                  <li>Login</li>
                </Link>
              )}
            </ul>

            <br />
            <br />
            <br />
            <br />

            <button className="btn1">Get Started</button>
          </SideNav>
        ) : null}
      </nav>
    </header>
  );
};

export default HeaderOne;

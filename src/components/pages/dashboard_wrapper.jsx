import React from "react";
// import PlayArrowIcon from "@material-ui/icons/PlayArrow";
// import CheckCircleIcon from "@material-ui/icons/CheckCircle";
// import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
// import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
// import HeadsetMicIcon from "@material-ui/icons/HeadsetMic";
// import DescriptionIcon from "@material-ui/icons/Description";
// import BeenhereIcon from "@material-ui/icons/Beenhere";
// import ListAltIcon from "@material-ui/icons/ListAlt";
// import AllInboxIcon from "@material-ui/icons/AllInbox";
import HeaderTwo from "../headertwo";
import { Link } from "react-router-dom";
import HomeIcon from "@material-ui/icons/Home";
import AccountTreeIcon from "@material-ui/icons/AccountTree";
import { useDispatch } from "react-redux";
import { LOGINOUTUSER } from "../../redux/action";
import Alert from "@material-ui/lab/Alert";
import { useHistory, useLocation } from "react-router-dom";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import AssignmentIcon from "@material-ui/icons/Assignment";
import { useSelector } from "react-redux";
import AccountBox from "@material-ui/icons/AccountBox";
const Dashboardwrapper = (props) => {
  const dispatch = useDispatch();
  const currentUser = useSelector(({ user }) => user.currentUser);
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const { location } = useHistory();
  const handLogout = () => {
    dispatch(LOGINOUTUSER());
  };
  // React.useEffect(() => console.log(location.pathname));
  return (
    <div className="dashboard-section">
      <HeaderTwo />
      <main>
        <div style={{ height: "15px" }} />
        <section className="dashboard-row row">
          <div className="left-dashboard-content">
            <ul>
              {/* <p>loremipsum@yahoo.com</p> */}

              <Link to="/users-dashboard">
                <li
                  className={`${
                    location.pathname === "/users-dashboard" ? "focused" : null
                  }`}
                >
                  <HomeIcon className="dash-icon" />
                  <p className="mobile-hide-left">Dashboard</p>
                </li>
              </Link>
              <Link to="/users-policies">
                <li
                  className={` ${
                    location.pathname === "/users-policies" ? "focused" : null
                  }`}
                >
                  <AssignmentIcon className="dash-icon" />
                  <p className="mobile-hide-left">Policies</p>
                </li>
              </Link>
          { currentUser.user.isAdmin? <Link to="/users-insurance-entities">
                <li
                  className={` ${
                    location.pathname === "/users-insurance-entities"
                      ? "focused"
                      : null
                  }`}
                >
                  <AssignmentIcon className="dash-icon" />
                  <p className="mobile-hide-left">Insurers</p>
                </li>
              </Link>:null}
              <Link to="/users-claims">
                <li
                  className={` ${
                    location.pathname === "/users-claims" ? "focused" : null
                  }`}
                >
                  <AccountTreeIcon className="dash-icon" />
                  <p className="mobile-hide-left">Claims</p>
                </li>
              </Link>
              <Link to="/users-profile">
                <li
                  className={` ${
                    location.pathname === "/users-profile" ? "focused" : null
                  }`}
                >
                  <AccountBox className="dash-icon" />
                  <p className="mobile-hide-left">Profile</p>
                </li>
              </Link>
              <Link to="/users-document">
                <li
                  className={` ${
                    location.pathname === "/users-document" ? "focused" : null
                  }`}
                >
                  <AccountTreeIcon className="dash-icon" />
                  <p className="mobile-hide-left">Documents</p>
                </li>
              </Link>
              <Link to="/users-payments">
                <li
                  className={` ${
                    location.pathname === "/users-payments" ? "focused" : null
                  }`}
                >
                  <AccountTreeIcon className="dash-icon" />
                  <p className="mobile-hide-left">Payments</p>
                </li>
              </Link>

              <li onClick={handLogout}>
                <ExitToAppIcon className="dash-icon" />
                <p className="mobile-hide-left"> Log Out</p>
              </li>
            </ul>
          </div>
          <div className="right-dashboard-content">{props.children}</div>
        </section>
      </main>
    </div>
  );
};

export default Dashboardwrapper;

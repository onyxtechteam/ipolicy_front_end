import React from "react";
import StarsIcon from "@material-ui/icons/Stars";
import {
  CarPolicyCard,
  TricycleCard,
  GadgetCard,
  BikeCard,
  TravelCard,
  LifeCard,
  HealthCard,
  MedicalCard,
  HomeCard,
} from "./policycards";
import axios from "axios";
import { useSelector } from "react-redux";
import CircularProgress from "@material-ui/core/CircularProgress";

const Policies = () => {
  const [fetching, setFetching] = React.useState(false);
  const [policieResult, setPoliciesResult] = React.useState([]);
  const { token } = useSelector(({ user }) => user.currentUser);
  const [pagination, setPagination] = React.useState({ total: 0, limit: "" });
  // console.log(token);
  var FetchInsuranceEntity = async () => {
    setFetching(true);
    await axios
      .get(`/api/v1/ListMypolicy`, { headers: { authorization: token } })
      .then((response) => {
        setTimeout(() => {
          setFetching(false);
        }, 1000);

        console.log(response.data);
        response.data.userData &&
          response.data.userData.length > 0 &&
          setPoliciesResult(response.data.userData);
        response.data.userData &&
          response.data.userData.length > 0 &&
          setPagination({
            ...pagination,
            limit: response.data.limit,
            total: response.data.total,
          });
      })
      .catch((err) => {
        setFetching(false);
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };
  const MapPoliciyResult = () => {
    return (
      policieResult.length > 0 &&
      policieResult.map((xxx) => {
        return xxx.productCatalogue === "Gadget insurance" ? (
          <GadgetCard data={xxx} />
        ) : xxx.productCatalogue === "Tricycle insurance" ? (
          <TricycleCard data={xxx} />
        ) : xxx.productCatalogue === "Bike insurance" ? (
          <BikeCard data={xxx} />
        ) : xxx.productCatalogue === "Car insurance" ? (
          <CarPolicyCard data={xxx} />
        ) : xxx.productCatalogue === "Travel insurance" ? (
          <TravelCard data={xxx} />
        ) : xxx.productCatalogue === "Life insurance" ? (
          <LifeCard data={xxx} />
        ) : xxx.productCatalogue === "Health insurance" ? (
          <HealthCard data={xxx} />
        ) : xxx.productCatalogue === "Home insurance" ? (
          <HomeCard data={xxx} />
        ) : null;
      })
    );
  };
  React.useEffect(() => {
    FetchInsuranceEntity();
  }, []);
  return (
    <div className="container-fluid">
      <div className="row page-title">
        <div className="col-md-12">
          <nav aria-label="breadcrumb" className="float-right mt-1">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">IPolicyMart</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/users-claims">Policies</a>
              </li>
            </ol>
          </nav>
          <h3 className="mb-1 mt-0">My Policies</h3>
          <span>
            ({policieResult.length}/{pagination.total})
          </span>
        </div>
      </div>
      <div style={{ height: "20px" }} />
      {/* rendered in empty policy array */}

      {fetching ? (
        <div className="row">
          <div className="col text-center">
            <CircularProgress color="secondary" />
          </div>
        </div>
      ) : policieResult.length < 1 ? (
        <div className="row">
          <div className="col-10 text-center">
            <a href="/#choose-us">
              <button className="btn1">Buy Policy Now</button>
            </a>
            <img height="100%" src="/images/notfound.png" alt="" />
            <h5>No Policy Found </h5>
          </div>
        </div>
      ) : (
        <div className="row ">
          {/* start single card */}
          {MapPoliciyResult()}
          {/* <CarPolicyCard />
          <CarPolicyCard />
          <CarPolicyCard />
          <TricycleCard /> */}

          {/* end single card */}
        </div>
      )}
      {/* renderd in non-empty policy array */}
    </div>
  );
};

export default Policies;

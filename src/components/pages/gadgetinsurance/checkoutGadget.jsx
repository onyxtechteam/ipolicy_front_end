import React from "react";
// import PlayArrowIcon from "@material-ui/icons/PlayArrow";
// import CheckCircleIcon from "@material-ui/icons/CheckCircle";
// import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
// import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
// import HeadsetMicIcon from "@material-ui/icons/HeadsetMic";
// import DescriptionIcon from "@material-ui/icons/Description";
// import BeenhereIcon from "@material-ui/icons/Beenhere";
// import ListAltIcon from "@material-ui/icons/ListAlt";
// import AllInboxIcon from "@material-ui/icons/AllInbox";
import HeaderTwo from "../../headertwo";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { usePaystackPayment } from "react-paystack";
import { CircularProgress } from "@material-ui/core";
import axios from "axios";
// import axios from "axios";
// import LoadingQuotes from "../../LoadingQuotes";
// import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
// import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
// import ArrowRightIcon from "@material-ui/icons/ArrowRight";

const CheckoutGadget = (props) => {
  const { location } = useHistory();
  const {
    state: { insuranceEntity, quoteRamdom },
  } = location;
  const history = useHistory();
  // const [insuranceEntity, setInsuranceEntity] = React.useState([]);
  const [activePage, setactivePage] = React.useState(4);
  const [fetching, setFetching] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const GadgetInsuranceReducer = useSelector(
    ({ GadgetInsuranceReducer }) => GadgetInsuranceReducer
  );

  const handlePanel = (value) => setactivePage(value);

  const PurchaseInsurance = async (data) => {
    setLoading(true);
    await axios
      .post(`/api/v1/PurchaseInsurance`, data)
      .then((response) => {
        console.log(response.data);
        setLoading(false);
        if ((response.data.status = "success")) {
          handleSuccesspay();
        }
        // response.data.userData &&
        //   response.data.userData.models.length > 0 &&
        //   setCarmodels(response.data.userData.models);
      })
      .catch((err) => {
        setLoading(false);
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };

  const config = {
    reference: new Date().getTime(),
    email: GadgetInsuranceReducer.email,
    amount: parseInt(insuranceEntity.gadgetQuotePrice) * 100,
    publicKey: process.env.REACT_APP_PAYSTACK_KEY,
  };
  const initializePayment = usePaystackPayment(config);

  const onSuccess = (reference) => {
    // Implementation for whatever you want to do with reference and after success call.
    console.log(reference);
    PurchaseInsurance({
      ...GadgetInsuranceReducer,
      otherData: GadgetInsuranceReducer,
      insuranceEntity: insuranceEntity,
      paymentDetails: reference,
    });
    // reference object structure --
    // message: "Approved"
    // ​
    // reference: "1624028019546"
    // ​
    // status: "success"
    // ​
    // trans: "1179280143"
    // ​
    // transaction: "1179280143"
    // ​
    // trxref: "1624028019546"
  };

  // you can call this function anything
  const onClose = () => {
    // implementation for  whatever you want to do when the Paystack dialog closed.
    console.log("closed");
  };

  const handlePaynow = () => {
    initializePayment(onSuccess, onClose);
  };

  const handleSuccesspay = () => {
    history.replace("/purchase-success");
    setTimeout(() => {
      history.push("/");
    }, 5000);
  };
  // React.useEffect(() => {
  //   console.log(insuranceEntity);
  // }, []);
  const carDetails = () => {
    return (
      <div className="col-5">
        <ul className="checkout-listing-details">
          <li>
            <span>Car Make</span>
            <span>{GadgetInsuranceReducer.carBrand}</span>
          </li>
          <li>
            <span>Car model</span>
            <span>{GadgetInsuranceReducer.carModel}</span>
          </li>
          <li>
            <span>Model Details</span>
            <span>{GadgetInsuranceReducer.carVarient}</span>
          </li>
          <li>
            <span>Car Year</span>
            <span>{GadgetInsuranceReducer.carRegisterYear}</span>
          </li>
          <li>
            <span>Starting Date</span>
            <span></span>
          </li>
        </ul>
      </div>
    );
  };
  const InsuranceDetails = () => {
    return (
      <div className="col-7">
        <ul className="checkout-listing-details">
          {/* <h6>
            Device Value :{" "}
            <span style={{ color: "dodgerblue" }}>
              {GadgetInsuranceReducer.}
            </span>
          </h6> */}
          <li>
            <span> Device Value:</span>
            <span>{GadgetInsuranceReducer.gadget_price_value}</span>
          </li>
          {/* <li>
            <span> Movable Items Value :</span>
            <span>{GadgetInsuranceReducer.movable_items_value}</span>
          </li> */}
        </ul>
      </div>
    );
  };
  // const InsuranceInformation = () => {
  //   return GadgetInsuranceReducer.member1 ? (
  //     <div className="col-5">
  //       <ul className="checkout-listing-details">
  //         <h6>
  //           Type :{" "}
  //           <span style={{ color: "dodgerblue" }}>
  //             {GadgetInsuranceReducer.insurance_type}
  //           </span>
  //         </h6>
  //         <li># 1 MEMBER </li>
  //         <li>
  //           <span>Date of Birth</span>
  //           <span>
  //             {GadgetInsuranceReducer.member1.dob_day}-
  //             {GadgetInsuranceReducer.member1.dob_month}-
  //             {GadgetInsuranceReducer.member1.dob_year}
  //           </span>
  //         </li>
  //         <li>
  //           <span>Gender</span>
  //           <span>{GadgetInsuranceReducer.member1.sex}</span>
  //         </li>
  //         <li>
  //           <span>Marital Status</span>
  //           <span>{GadgetInsuranceReducer.member1.marital_status}</span>
  //         </li>
  //         <li>
  //           <span>Relationship With Sponsor</span>
  //           <span>
  //             {GadgetInsuranceReducer.member1.relationship_with_sponsor}
  //           </span>
  //         </li>
  //       </ul>
  //     </div>
  //   ) : null;
  // };
  const ValidateProcessData = () => {
    const { email, firstName, lastName } = GadgetInsuranceReducer;
    if (!email || !firstName || !lastName) {
      history.push("/");

      return false;
    } else return true;
  };

  React.useEffect(() => {
    ValidateProcessData();
    console.log(GadgetInsuranceReducer);
  }, []);
  React.useEffect(() => {
    console.log(GadgetInsuranceReducer);
  }, []);
  const PersonalDetails = () => {
    return (
      <div className="col-5">
        <ul className="checkout-listing-details">
          <li>
            <span>First Name</span>
            <span>{GadgetInsuranceReducer.firstName}</span>
          </li>
          <li>
            <span>Last Name</span>
            <span>{GadgetInsuranceReducer.lastName}</span>
          </li>
          <li>
            <span>Email</span>
            <span>{GadgetInsuranceReducer.email}</span>
          </li>
          <li>
            <span>Mobile Phone</span>
            <span>{GadgetInsuranceReducer.mobileNumber}</span>
          </li>
        </ul>
      </div>
    );
  };
  const GadgetDetails = () => {
    return (
      <div className="col-5">
        <ul className="checkout-listing-details">
          <li>
            <span>Gadget Name</span>
            <span>{GadgetInsuranceReducer.Gadget_name}</span>
          </li>
          <li>
            <span>Gadget brand</span>
            <span>{GadgetInsuranceReducer.gadGet_brand}</span>
          </li>
          <li>
            <span>Gadget Value</span>
            <span>{GadgetInsuranceReducer.gadget_price_value}</span>
          </li>
        </ul>
      </div>
    );
  };
  return (
    <div>
      <HeaderTwo />
      {loading ? (
        <div className="loaderOverLay">
          <CircularProgress
            fontSize="large"
            style={{ color: "orange", marginRight: "10px" }}
          />
          <h6>Please wait..</h6>
        </div>
      ) : null}
      <main style={{ backgroundColor: "#fbfbfd" }}>
        <div className="Qoutes-header-Count-first verticalCenterRow justify-content-between">
          <h4>Gadget insurance</h4>
          <a href="/">
            {" "}
            <button className="btn2">Back to home</button>
          </a>
        </div>
        <div className="Qoutes-header-Count">
          <div className="container">
            <p className="bunow">Buy Policy Now</p>
          </div>
        </div>
        <div style={{ marginTop: "20px" }} className="container">
          <section className="Car-section row">
            <div className="col-lg-9">
              <div className=" checkout-user-details ">
                <div className="container">
                  <div className="verticalCenterRow">
                    <div className="col-4">
                      <img width="150px" src={insuranceEntity.logoImg} alt="" />
                    </div>
                    <div className="checkout-user-details-header col-8">
                      {/* <h4>{insuranceEntity.name}</h4>
                      <span style={{ color: "dodgerblue" }}>
                        {GadgetInsuranceReducer.firstName}-{quoteRamdom}
                      </span> */}
                      <p>
                        Quote Reference:{" "}
                        <span style={{ color: "dodgerblue" }}>
                          {GadgetInsuranceReducer.firstName}-{quoteRamdom}
                        </span>
                      </p>
                      <button className="btn1">
                        Contact Insurance company
                      </button>
                    </div>
                  </div>
                  <div className="checkout-user-info">
                    <h5>Submitted Information</h5>
                  </div>
                  <div className="checkout-user-info-items verticalCenterRow ">
                    <div col-3 onClick={handlePanel.bind(this, 3)}>
                      <div
                        className={`card ${activePage === 3 ? "active" : null}`}
                      >
                        <div className="card-body">
                          <h6>Gadget Details</h6>
                        </div>
                      </div>
                    </div>
                    <div col-3 onClick={handlePanel.bind(this, 4)}>
                      <div
                        className={`card ${activePage === 4 ? "active" : null}`}
                      >
                        <div className="card-body">
                          <h6>insurance Details</h6>
                        </div>
                      </div>
                    </div>
                    <div col-3 onClick={handlePanel.bind(this, 5)}>
                      <div
                        className={`card ${activePage === 5 ? "active" : null}`}
                      >
                        <div className="card-body">
                          <h6>Personal Infomation</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="checkout-detail-content-area">
                    <div className="container">
                      {activePage === 3
                        ? GadgetDetails()
                        : activePage === 5
                        ? PersonalDetails()
                        : activePage === 4
                        ? InsuranceDetails()
                        : null}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 ">
              <div className="checkout-price-details">
                <div className="container">
                  <div style={{ height: "20px" }} />
                  <h6>Payment Details</h6>
                  <div style={{ height: "30px" }} />
                  <p style={{ lineHeight: 0.2 }}>Do you you have promo code</p>
                  <div className="promo-now verticalCenterRow">
                    <input placeholder="code" />
                    <button>Apply</button>
                  </div>
                  <ul className="checkout-listing">
                    <li>
                      <span>Base Price</span>
                      <span>N{insuranceEntity.gadgetQuotePrice}</span>
                    </li>
                    <li>
                      <span>Vat</span>
                      <span>N43.00</span>
                    </li>
                    <li>
                      <span>Discount</span>
                      <span>N3.89</span>
                    </li>
                    <li>
                      <span>To Pay</span>
                      <span>
                        N {parseInt(insuranceEntity.gadgetQuotePrice) + 43}
                      </span>
                    </li>
                  </ul>
                  <button onClick={handlePaynow} className="btn5">
                    Confirm
                  </button>
                  <p className="smalltext">
                    Some secureity information Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit, sed
                  </p>
                </div>
              </div>
            </div>
          </section>
        </div>
      </main>
    </div>
  );
};

export default CheckoutGadget;

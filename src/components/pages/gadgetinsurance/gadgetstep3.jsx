import React from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { SETGADGETPROCESS } from "../../../redux/action";
import Alert from "@material-ui/lab/Alert";
const Gadgetstep3 = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [formResponse, setFormresponse] = React.useState({
    firstName: "",
    lastName: "",
    mobileNumber: "",
    email: "",
    address: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });

  function validateEmail(email) {
    const re =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const handleBack = () => history.goBack();
  const handleNext = () => {
    const { firstName, email, lastName, mobileNumber } = formResponse;
    console.log(formResponse);
    if (!firstName || !email || !lastName || !mobileNumber) {
      return setErrorAlert({
        status: true,
        message: "All fields are required",
      });
    }
    if (!validateEmail(formResponse.email)) {
      return setErrorAlert({
        status: true,
        message: "Invalid email supplied,",
      });
    }
    dispatch(SETGADGETPROCESS(formResponse));
    history.push("/Gadget-insurance-Quotes");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Personal Information</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Enter Your Personal Information :</p>
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        {/* <div className="col"> */}
        <div className="row">
          <div className="col-lg-6 ">
            <input
              onChange={(e) =>
                setFormresponse({ ...formResponse, firstName: e.target.value })
              }
              type="text"
              className="input-cars"
              placeholder="First Name"
              value={formResponse.firstName}
            />
          </div>
          <div className=" col-lg-6 ">
            <input
              value={formResponse.lastName}
              onChange={(e) =>
                setFormresponse({ ...formResponse, lastName: e.target.value })
              }
              type="text"
              className="input-cars"
              placeholder="Last Name"
            />
          </div>
          <div style={{ height: "20px" }} />

          <div className=" col-lg-12 ">
            <input
              onChange={(e) =>
                setFormresponse({
                  ...formResponse,
                  address: e.target.value,
                })
              }
              value={formResponse.address}
              type="text"
              className="input-cars"
              placeholder="Address"
            />
          </div>
          <div style={{ height: "20px" }} />
          <div className=" col-lg-12 ">
            <input
              onChange={(e) =>
                setFormresponse({ ...formResponse, email: e.target.value })
              }
              value={formResponse.email}
              type="text"
              className="input-cars"
              placeholder="Email"
            />
          </div>
          <div style={{ height: "20px" }} />
          <div className=" col-lg-12 ">
            <input
              onChange={(e) =>
                setFormresponse({
                  ...formResponse,
                  mobileNumber: e.target.value,
                })
              }
              value={formResponse.mobileNumber}
              type="text"
              className="input-cars"
              placeholder="Phone number"
            />
          </div>

          {/* </div> */}
        </div>
        <div style={{ height: "20px" }} />
        <div className="row carsModel">{/* begin card */}</div>
        <div style={{ height: "40px" }} />

        <div style={{ height: "40px" }} />

        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Get A Quote
        </button>
      </div>
    </div>
  );
};

export default Gadgetstep3;

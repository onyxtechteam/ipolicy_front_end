import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETGADGETPROCESS } from "../../../redux/action";

const Gadgetjson = [
  { name: "Sony", imgUrl: "/images/gadgetlogo/sony.png" },
  // { name: "Nikon", imgUrl: "/images/gadgetlogo/Nikon.png" },
  { name: "LG", imgUrl: "/images/gadgetlogo/lg.png" },
  { name: "canon", imgUrl: "/images/gadgetlogo/canon.png" },

  { name: "samsung", imgUrl: "/images/gadgetlogo/samsung.png" },
  { name: "apple", imgUrl: "/images/gadgetlogo/apple.png" },
  { name: "dell", imgUrl: "/images/gadgetlogo/dell.png" },
  { name: "hp", imgUrl: "/images/gadgetlogo/hp.png" },
  { name: "others", imgUrl: "/images/gadgetlogo/others.png" },
];

const Gadgetcard = (props) => (
  <div className=" gadget-card col-lg-2 col-md-7">
    <div
      onClick={props.handlefocused.bind(this, props.data.name)}
      className={`card ${
        props.focused === props.data.name ? "focused" : null
      } `}
    >
      <div className="card-body">
        <img width="75px" src={props.data.imgUrl} alt="" />
      </div>
    </div>
  </div>
);
const Gadgetstep1 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const handleBack = () => history.goBack();
  const item = history.location.state;
  const [focused, setFocused] = React.useState(null);
  const handlefocused = (value) => setFocused(value);
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handleNext = () => {
    if (!focused) {
      return setErrorAlert({
        status: true,
        message: "No selection made",
      });
    }
    dispatch(SETGADGETPROCESS({ gadGet_brand: focused, gadGet_type: item }));
    history.push("/Gadget-insurance-step2");
  };
  const MapGadgetJson = () => {
    return Gadgetjson.map((xxx) => (
      <Gadgetcard
        data={xxx}
        t
        handlefocused={handlefocused}
        focused={focused}
      />
    ));
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>
          Gadget Details
          <span style={{ color: "dodgerblue", textTransform: "capitalize" }}>
            {" "}
            - {item}
          </span>
        </h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Select Brand:</p>
        <div className="col-lg-4">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <input className="input-cars" placeholder="" />
        <div style={{ height: "20px" }} />
        <div className="row carsList">
          {/* begin card */}
          {MapGadgetJson()}
          {/* end card */}
        </div>
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Gadgetstep1;

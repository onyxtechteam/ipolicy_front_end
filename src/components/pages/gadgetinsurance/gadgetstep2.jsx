import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch } from "react-redux";
import { SETGADGETPROCESS } from "../../../redux/action";
const Gadgetstep2 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const [formresponse, setformresponse] = React.useState({
    // non_movable_items_value: "",
    // movable_items_value: "",
    gadget_price_value: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  // const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    const { gadget_price_value } = formresponse;
    console.log(formresponse);
    if (!gadget_price_value) {
      return setErrorAlert({
        status: true,
        message: "You did not select the required field",
      });
    }
    dispatch(SETGADGETPROCESS(formresponse));
    history.push("/Gadget-insurance-step3");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Insurance Details</h5>
        <div style={{ height: "20px" }} />
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <p className="midtext1">What Is The Estimated Value Of Your Gadget ?</p>

        <div style={{ height: "20px" }} />
        <div className="col-10">
          <select
            // defaultValue={formresponse.members_nos}
            className="input-cars"
            id="countries-list"
            value={formresponse.gadget_price_value}
            onChange={(e) => {
              e.target.value &&
                setformresponse({
                  ...formresponse,
                  gadget_price_value: e.target.value,
                });
            }}
            style={
              {
                // width: "80%",
                // border: "none",
                // borderRadius: "4px",
                // height: "40px",
              }
            }
          >
            <option value="">Select Value</option>
            <option value="N10k-N50k">N10k-N50k</option>
            <option value="N50k-N150k">N50k-N150k</option>
            <option value="N150k-N500K">N150k-N500K</option>
            <option value="N500k-N1 MILLION">N500k-N1 MILLION</option>
          </select>
        </div>

        <div style={{ height: "20px" }} />
        {/* <p className="midtext1">
          What Is The Value Of Your Movable Personal Belongings ?
        </p> */}
        <div style={{ height: "10px" }} />
        {/* <div className="col-10">
          <select
            // defaultValue={formresponse.members_nos}
            className="input-cars"
            id="countries-list"
            value={formresponse.movable_items_value}
            onChange={(e) => {
              e.target.value &&
                setformresponse({
                  ...formresponse,
                  movable_items_value: e.target.value,
                });
            }}
            style={
              {
                // width: "80%",
                // border: "none",
                // borderRadius: "4px",
                // height: "40px",
              }
            }
          >
            <option value="Select Value">Select Value</option>
            <option value="N1-N1 Million">N1-N1 Million</option>
            <option value="N2 Million-N10 Million">
              N2 Million-N10 Million
            </option>
            <option value="N20 Million-N40 Million">
              N20 Million-N40 Million
            </option>
            <option value="N40 Million-N200 Million">
              N40 Million-N200 Million
            </option>
          </select>
        </div> */}

        <div style={{ height: "40px" }} />
        <div className="row carsModel">
          {/* begin healthd */}

          {/* end healthd */}
          {/* end healthd */}
        </div>
        <div style={{ height: "10px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleBack} className="btn2 ">
            back
          </button>
          <button
            style={{ width: "150px" }}
            onClick={handleNext}
            className="btn1 healthNextbtn"
          >
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Gadgetstep2;

import React from "react";
import HeaderTwo from "../headertwo";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import {useHistory} from 'react-router-dom'
const SuccessClaim = () => {
const history = useHistory()
  const handleGoHome=()=>{
history.replace('/users-dashboard')
  }
  return (
    <div className="">
      <HeaderTwo />
      <main className="successPurchase">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-10 text-center">
              <CheckCircleOutlineIcon
                style={{ color: "#51AF33", fontSize: "100px" }}
                fontSize="large"
              />
              <div style={{ height: "20px" }} />
              <h1>
               Thanks !.
            
              </h1>
              <div style={{ height: "30px" }} />
              <h4 className='sub-title'>
              Your request has been submited And  will be reviewed,
                <br /> You will get A response In a shot period ,usually within 24 hours.
              </h4>
              <div style={{ height: "40px" }} />
              <button onClick={handleGoHome} className="btn1">Go to Dashboard</button>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default SuccessClaim ;

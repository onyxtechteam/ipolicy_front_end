import React from "react";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
// import CheckCircleIcon from "@material-ui/icons/CheckCircle";
// import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
// import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
// import HeadsetMicIcon from "@material-ui/icons/HeadsetMic";
// import DescriptionIcon from "@material-ui/icons/Description";
// import BeenhereIcon from "@material-ui/icons/Beenhere";
// import ListAltIcon from "@material-ui/icons/ListAlt";
// import AllInboxIcon from "@material-ui/icons/AllInbox";
import HeaderOne from "../headerone";
import { Link, useHistory } from "react-router-dom";
import Footer from "./Footer";
const PersonalHome = () => {
  const history = useHistory();
  return (
    <div>
      <HeaderOne />
      <main className="mainBody">
        <div className="Main-section">
          <div className="container">
            <div className="verticalCenterRow">
              <div className="col-lg-5 col-sm-8 Main-content">
                <h3>Compare, Buy, Save</h3>
                <h1 className="Main-content-add">
                  Smart way to buy
                  <br />
                  Insurance
                </h1>
                <p>
                  Compare insurance policies, rate, detail, and <br />
                  buy instantly with IPolicyMart's best <br />
                  insurance companies in less
                  <br /> than five minutes!
                  <br />
                </p>
                <button className="btn1">Compare now</button>
                <div className="watch-video-main">
                  <p className="title4">
                    {" "}
                    <PlayArrowIcon />
                    Watch the intro video Get
                  </p>
                </div>
              </div>
              <div className="col-lg-6 col-sm-4 main-image-div">
                <img className src="/images/mansmile.png" alt="" />
              </div>
            </div>
            <div className="Main-img" />
          </div>
        </div>
        {/* end main-section */}
        <section className="partners">
          <div className="container">
            <div className="partnersRow verticalCenterRow">
              <div className="">
                <img src="/images/p6.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p1.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p2.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p3.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p4.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p5.png" alt="" />
              </div>
            </div>
          </div>
        </section>
        {/* begin choose us section */}
        <div className="choose-us">
          <div className="container">
            <div className="verticalCenterRow justify-content-center">
              <div className="choose-us-main col-lg-8 col-sm-9">
                <h2 className="title1">Choose your insurance</h2>
                <p className="sub-title">
                  Keep Your Life,Smile,Safe and Wealthy
                </p>
                <div className="row  choose-us-options justify-content-center">
                  {/* begin card */}
                  <div
                    onClick={() => history.push("/auto-insurance")}
                    className="col-lg-4 col-sm-2"
                  >
                    <div className="card ">
                      <div className="card-body  justify-content-between">
                        <span className="verticalCenterRow ">
                          <h5>
                            Auto
                            <br />
                            Insurance
                          </h5>
                          <img
                            width="50px"
                            style={{ marginLeft: "40px" }}
                            className
                            src="/images/auto.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Auto insurance is designed to protect yourself and
                          others against many various risks.
                          <br />
                          <br />
                          <br />
                        </p>
                      </div>
                    </div>
                  </div>
                  {/* end card */}
                  {/* begin card */}
                  <div
                    onClick={() => history.push("/Gadget-insurance")}
                    className="col-lg-4 col-sm-2"
                  >
                    <div className="card ">
                      <div className="card-body  justify-content-between">
                        <span className="verticalCenterRow ">
                          <h5>
                            Gadget
                            <br />
                            Insurance
                          </h5>
                          <img
                            width="30px"
                            style={{ marginLeft: "40px" }}
                            className
                            src="/images/gadget.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Protect all your home devices in less than 5 minutes
                          <br />
                          <br />
                          <br />
                          <br />
                        </p>
                      </div>
                    </div>
                  </div>
                  {/* end card */}
                  {/* begin card */}

                  <div
                    onClick={() => history.push("/health-insurance")}
                    className="col-lg-4 col-sm-2"
                  >
                    <div className="card ">
                      <div className="card-body  justify-content-between">
                        <span className="verticalCenterRow ">
                          <h5>
                            Health
                            <br />
                            Insurance
                          </h5>
                          <img
                            width="50px"
                            style={{ marginLeft: "40px" }}
                            className
                            src="/images/medical.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Health is the most important thing we should protect,
                          and when we can no longer take care of ourselves, then
                          insurance will help.
                          <br />
                        </p>
                      </div>
                    </div>
                  </div>
                  {/* end card */}
                  {/* begin card */}
                  <div
                    onClick={() => history.push("/Home-insurance")}
                    className="col-lg-4 col-sm-2"
                  >
                    <div className="card ">
                      <div className="card-body  justify-content-between">
                        <span className="verticalCenterRow ">
                          <h5>
                            Home
                            <br />
                            Insurance
                          </h5>
                          <img
                            width="50px"
                            style={{ marginLeft: "40px" }}
                            className
                            src="/images/home.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Providing a coverage for a private residence has never
                          been so easy
                          <br />
                          <br />
                          <br />
                        </p>
                      </div>
                    </div>
                  </div>
                  {/* end card */}
                  {/* begin card */}
                  <div
                    onClick={() => history.push("/life-insurance")}
                    className="col-lg-4 col-sm-2"
                  >
                    <div className="card ">
                      <div className="card-body  justify-content-between">
                        <span className="verticalCenterRow ">
                          <h5>
                            Life
                            <br />
                            Insurance
                          </h5>
                          <img
                            alt=""
                            width="50px"
                            style={{ marginLeft: "40px" }}
                            className
                            src="/images/life.png"
                          />
                        </span>
                        <p>
                          Prepare for your family's future in the case of an
                          unexpected tragedy. <br />
                          <br />
                          <br />
                        </p>
                      </div>
                    </div>
                  </div>
                  {/* end card */}
                  <div
                    onClick={() => history.push("/Travel-insurance")}
                    className="col-lg-4 col-sm-2"
                  >
                    <div className="card">
                      <div className="card-body">
                        <span className="verticalCenterRow">
                          <h5>
                            Travel
                            <br />
                            Insurance
                          </h5>
                          <img
                            width="45px"
                            style={{ marginLeft: "40px" }}
                            className
                            src="/images/travel.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Insurance that plans cover trip cancellation, travel
                          medical & more <br />
                          <br />
                          <br />
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style={{ height: "60px" }} />
        <div className="get-app">
          <div className="container">
            <div className="verticalCenterRow justify-content-between get-app-container">
              <div style={{ padding: "20px" }} className="col-lg-6 col-sm-7">
                <h4 className="title1">
                  Get the iPolicyMart's App
                  <br />
                </h4>
                <p>Get control of all your insurance needs anywhere,anytime</p>
                <ul>
                  <li>
                    Compare different insurance policies
                    <br />
                  </li>
                  <li>
                    Buy, store and share all your policies online
                    <br />
                  </li>
                  <li>
                    Track your Policy status on the go <br />
                  </li>
                  <li>
                    Download your policy with a single tap <br />
                    savings are
                  </li>
                </ul>
                <div className="verticalCenterRow align-items-center">
                  <button
                    style={{
                      paddingLeft: "30px",
                      paddingRight: "30px",
                      marginRight: "10px",
                      backgroundColor: "#fff",
                      borderRadius: "10px",
                      color: "black",
                    }}
                    className="btn1"
                  >
                    <img
                      style={{ marginRight: "7px" }}
                      width="20px"
                      src="/images/playstore.png"
                      alt=""
                    />
                    Play store
                  </button>
                  <button
                    style={{
                      paddingLeft: "30px",
                      paddingRight: "30px",
                      marginRight: "30px",
                      border: "1px solid #fff",
                      borderRadius: "10px",
                    }}
                    className="btn1"
                  >
                    <img width="20px" src="/images/appstore.png" alt="" />
                    App store
                  </button>
                </div>
              </div>
              <div
                style={{ alignSelf: "flex-end" }}
                className="col-lg-5 col-sm-7 about-us-img"
              >
                <img width="110%" src="/images/mobiles.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </main>
  <Footer/>
    </div>
  );
};

export default PersonalHome;

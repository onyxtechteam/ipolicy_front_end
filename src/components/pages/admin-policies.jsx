import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import axios from "axios";
import { useSelector } from "react-redux";
import { Link, Route, Switch, useLocation } from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import {
  CarDetailCard,
  BikeDetailCard,
  LifeDetailCard,
  HomeDetailCard,
  TravelDetailCard,
  HealthDetailCard,
  GadgetDetailCard,
  ThreeDetailCard,
} from "./admin-policy-cards";

const DetailPage = (props) => {
  const { HandleGoback } = props;
  const location = useLocation();
  const { state } = location;
  console.log(state);
  const RenderCards = () => {
    return state.productCatalogue === "Car insurance" ? (
      <CarDetailCard data={state} />
    ) : state.productCatalogue === "Bike insurance" ? (
      <BikeDetailCard data={state} />
    ) : state.productCatalogue === "Life insurance" ? (
      <LifeDetailCard data={state} />
    ) : state.productCatalogue === "Home insurance" ? (
      <HomeDetailCard data={state} />
    ) : state.productCatalogue === "Travel insurance" ? (
      <TravelDetailCard data={state} />
    ) : state.productCatalogue === "Health insurance" ? (
      <HealthDetailCard data={state} />
    ) 
    : state.productCatalogue === "Gadget insurance" ? (
      <GadgetDetailCard data={state} />
    ) 
    : state.productCatalogue === "Tricycle insurance" ? (
      <ThreeDetailCard data={state} />
    ) 
    : null;
  };
  return (
    <div
      style={{ minHeight: "450px", backgroundColor: "#f2f2f2" }}
      className="col-12"
    >
      <div className="verticalCenterRow">
        <div className="col3">
          <button className="btn2" onClick={HandleGoback}>
            <ArrowBackIcon /> back
          </button>
        </div>
        <div className="col-4">
          <h2 style={{ textAlign: "center" }}>Details</h2>
        </div>
      </div>
      <div className="row">{RenderCards()}</div>
    </div>
  );
};

const AdminPolicies = (props) => {
  const [fetching, setFetching] = React.useState(false);
  const { token } = useSelector(({ user }) => user.currentUser);
  const [policieResult, setPoliciesResult] = React.useState([]);
  const [pagination, setPagination] = React.useState({ total: 0, limit: "" });
  const [pageNo, setpageNo] = React.useState(0);
  const {
    match: { path },
  } = props;
  var FetchPolicies = async () => {
    setFetching(true);
    await axios
      .get(`/api/v1/ListAllpolicy?pageNo=${pageNo}`, {
        headers: { authorization: token },
      })
      .then((response) => {
        setFetching(false);

        console.log(response.data);
        response.data.userData &&
          response.data.userData.length > 0 &&
          setPoliciesResult(response.data.userData);
        response.data.userData &&
          response.data.userData.length > 0 &&
          setPagination({
            ...pagination,
            limit: response.data.limit,
            total: response.data.total,
          });
        response.data.userData.length > 0 && setpageNo(pageNo + 1);
      })
      .catch((err) => {
        setFetching(false);
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };
  const handleNextPage = () => {
    setPoliciesResult([]);
    FetchPolicies();
  };
  const mapPolicyResult = () => {
    return policieResult.map((xxx) => (
      <tr key={xxx._id}>
        <td>
          {xxx.firstName} {xxx.lastName}
        </td>

        <td>{xxx.otherData.address || "-"}</td>
        {/* <td>Dom</td> */}
        <td>{xxx.mobileNumber || "-"}</td>
        <td>
          {xxx.dob_day || "-"}/{xxx.dob_month || "-"}/{xxx.dob_year || "-"}
        </td>
        <td>{xxx.productCatalogue || "-"}</td>
        <td>{xxx.HomeQuotePrice || "-"}</td>
        <td>{xxx.insuranceEntity.name || "-"}</td>
        <td>{xxx.travelQuotePrice || "-"}</td>
        <td>{xxx.otherData.insurance_type || "-"}</td>
        {/* <td>{xxx.gadgetQuotePrice || "-"}</td> */}
        <td>
          {/* <MenuEntity
            DeleteInsuranceEntity={DeleteInsuranceEntity}
            insureId={xxx._id}
          /> */}
          <small>
            <Link to={{ pathname: `${path}/details`, state: xxx }}>
              Details
            </Link>
          </small>
        </td>
      </tr>
    ));
  };
  React.useEffect(() => {
    FetchPolicies();
  }, []);
  const HandleGoback = () => {
    props.history.goBack();
  };
  return (
    <div className="container-fluid">
      <div className="row page-title">
        <div className="col-md-12">
          <nav aria-label="breadcrumb" className="float-right mt-1">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">IPolicyMart</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/users-policies">Admin - Policies</a>
              </li>
            </ol>
          </nav>
          <h3 className="mb-1 mt-0">
            <b> Purchased Policies list</b>
          </h3>
          <span>
            {/* (
            {policieResult.length < pagination.total &&pagination.total>pagination.limit
              ? policieResult.length 
              : pagination.total + pagination.limit} */}
            Total {pagination.total}
            <div style={{ height: "4px" }} />
            {pagination.limit * pageNo < pagination.total ? (
              <div>
                <button onClick={handleNextPage} className="btn3">
                  Next
                </button>
              </div>
            ) : null}
          </span>
        </div>
      </div>
      <div className="row justify-content-end">
        {fetching ? (
          <div className="text-center">
            <CircularProgress
              size={20}
              // color="secondary"
            />
          </div>
        ) : null}
        {/* <div style={{ float: "right" }} className="col-2">
          <button onClick={handleOpen} className="btn1">
            Add <AddCircleOutlineIcon />
          </button>
        </div> */}
      </div>
      <Switch>
        <Route exact path={`${path}`}>
          <div
            style={{ MaxWidth: "100%", overflowX: "scroll" }}
            className="row"
          >
            <div className="col-12">
              <table class="styled-table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>

                    {/* <th>Developer Portal</th> */}
                    <th>Mobile No.</th>
                    <th>Date Of Birth</th>
                    <th>Product Catalogue</th>
                    <th>Cover Type</th>
                    <th>Insurer</th>
                    <th>Quote Price</th>
                    <th>Insurance Type</th>
                    {/* <th>Purchased Date</th> */}
                    <th>-</th>
                  </tr>
                </thead>

                <tbody>{mapPolicyResult()}</tbody>
              </table>
            </div>
          </div>
        </Route>
        <Route path={`${path}/details`}>
          <DetailPage HandleGoback={HandleGoback} />
        </Route>
      </Switch>
    </div>
  );
};

export default AdminPolicies;

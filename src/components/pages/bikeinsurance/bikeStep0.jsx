import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETBIKEPROCESSINFO } from "../../../redux/action";

const Bikestep0 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const [formResponse, setFormresponse] = React.useState({
    carNumber: "",
  });
  const handlefocused = (value) => setFocused(value);
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handleNext = () => {
    if (!formResponse.carNumber) {
      return setErrorAlert({
        status: true,
        message:
          "provide a vehicle registration number or click proceed without bike number",
      });
    }
    dispatch(SETBIKEPROCESSINFO({ bikeNumber: formResponse.carNumber }));
    history.push("/bike-insurance-step1");
  };
  const handleNextnoNumber = () => {
    dispatch(SETBIKEPROCESSINFO({ bikeNumber: "not provided" }));
    history.push("/bike-insurance-step1");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Bike Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">What Is Your Bike Number:</p>
        <div className="col-lg-4">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <input
          onChange={(e) =>
            setFormresponse({ ...formResponse, carNumber: e.target.value })
          }
          value={formResponse.carNumber}
          className="input-cars"
          placeholder="Bike Number"
        />
        <div style={{ height: "20px" }} />
        {/* <div className="row carsList">
        
        </div> */}
        <div style={{ height: "20px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleNextnoNumber} className="btn1 ">
            Proceed Without Bike Number
          </button>
          <button onClick={handleNext} className="btn1 ">
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Bikestep0;

import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETBIKEPROCESSINFO } from "../../../redux/action";
const bikeRegistrationYear = [
  { name: "brand new bike" },
  { name: "2020" },
  { name: "2019" },
  { name: "2018" },
  { name: "2017" },
  { name: "2016" },
  { name: "2015" },
  { name: "2014" },
  { name: "2013" },
  { name: "2012" },
  { name: "2011" },
  { name: "2010" },
  { name: "2009" },
  { name: "2008" },
  { name: "2007" },
  { name: "2006" },
  { name: "2005" },
  { name: "2004" },
  { name: "2003" },
  { name: "2002" },
  { name: "2001" },
  { name: "2000" },
  { name: "1999" },
  { name: "1998" },
  { name: "1997" },
  { name: "1996" },
  { name: "1995" },
];
const Biketep3 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [formresponse, setFormresponse] = React.useState({
    bikeRegisterYear: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handleBack = () => history.goBack();
  const handleNext = () => {
    if (!formresponse.bikeRegisterYear) {
      return setErrorAlert({
        status: true,
        message: "You have not made a selection",
      });
    }
    dispatch(SETBIKEPROCESSINFO(formresponse));
    history.push("/bike-insurance-step5");
  };
  const mapCarYears = () => {
    return bikeRegistrationYear.map((xxx) => (
      <div
        onClick={() => setFormresponse({ bikeRegisterYear: xxx.name })}
        className={`cardB ${
          formresponse.bikeRegisterYear === xxx.name ? "focused" : null
        } col-lg-3 col-md-3 col-sm-3`}
      >
        <button style={{ width: "100%" }} className="btn3">
          {xxx.name}
        </button>
      </div>
    ));
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Bike Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Year:</p>
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>

        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          {/* begin card */}

          {mapCarYears()}
          {/* end card */}
          {/* end card */}
        </div>

        <div style={{ height: "20px" }} />

        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleBack} className="btn2 ">
            back
          </button>
          <button onClick={handleNext} className="btn1 carNextbtn">
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Biketep3;

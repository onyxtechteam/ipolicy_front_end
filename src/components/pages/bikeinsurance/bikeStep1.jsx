import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETBIKEPROCESSINFO } from "../../../redux/action";

const BikeJson = [
  {
    name: "Suzuki",
    imgUrl: "/images/bikelogo/suzuki.png",
  },
  {
    name: "Honda",
    imgUrl: "/images/bikelogo/honda.png",
  },
  {
    name: "TVS",
    imgUrl: "/images/bikelogo/tvs.png",
  },
  {
    name: "Kawasaki",
    imgUrl: "/images/bikelogo/kawasaki.png",
  },
  {
    name: "BMW",
    imgUrl: "/images/bikelogo/bmw.png",
  },
  {
    name: "Trek",
    imgUrl: "/images/bikelogo/trek.png",
  },
  {
    name: "Bajaj",
    imgUrl: "/images/bikelogo/bajaj.png",
  },
  {
    name: "Yamaha",
    imgUrl: "/images/bikelogo/yamaha.png",
  },
];

const BikeCard = (props) => {
  return (
    <div className=" car-card col-lg-3 ">
      <div
        onClick={props.handlefocused.bind(this, props.data.name)}
        className={`card ${
          props.focused === props.data.name ? "focused" : null
        } `}
      >
        <div className="card-body">
          <img
            style={{ height: "50px", objectFit: "contain" }}
            src={props.data.imgUrl}
            alt=""
          />
        </div>
      </div>
    </div>
  );
};

const Bikestep1 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const handleBack = () => history.goBack();
  const [focused, setFocused] = React.useState(null);
  const handlefocused = (value) => setFocused(value);
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handleNext = () => {
    if (!focused) {
      return setErrorAlert({
        status: true,
        message: "No selection made",
      });
    }
    dispatch(SETBIKEPROCESSINFO({ bikeBrand: focused }));
    history.push("/bike-insurance-step2");
  };

  const MapGadgetJson = () => {
    return BikeJson.map((xxx) => (
      <BikeCard data={xxx} t handlefocused={handlefocused} focused={focused} />
    ));
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Bike Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Bike Brand:</p>
        <div className="col-lg-4">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <input className="input-cars" placeholder="" />
        <div style={{ height: "20px" }} />
        <div className="row carsList">
          {/* begin card */}
          {MapGadgetJson()}
          {/* end card */}
          {/* begin card */}

          {/* end card */}
        </div>
        <div style={{ height: "20px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleBack} className="btn2 ">
            back
          </button>
          <button onClick={handleNext} className="btn1 carNextbtn">
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Bikestep1;

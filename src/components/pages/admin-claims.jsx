import React from "react";
import axios from 'axios';
import {useSelector} from 'react-redux'
import CircularProgress from "@material-ui/core/CircularProgress";

const AdminClaims = () => {
  const [fetching, setFetching] = React.useState(false);
  const { token } = useSelector(({ user }) => user.currentUser);
  const [policieResult, setPoliciesResult] = React.useState([]);
  const [pagination, setPagination] = React.useState({ total: 0, limit: "" });
  const [pageNo, setpageNo] = React.useState(0);


  const handleNextPage = () => {
    setPoliciesResult([]);
    FetchClaims();
  };
  var FetchClaims = async () => {
    setFetching(true);
    await axios
      .get(`/api/v1/ListAllClaims?pageNo=${pageNo}`, {
        headers: { authorization: token },
      })
      .then((response) => {
        setFetching(false);

        console.log(response.data);
        response.data.userData &&
          response.data.userData.length > 0 &&
          setPoliciesResult(response.data.userData);
        response.data.userData &&
          response.data.userData.length > 0 &&
          setPagination({
            ...pagination,
            limit: response.data.limit,
            total: response.data.total,
          });
        response.data.userData.length > 0 && setpageNo(pageNo + 1);
      })
      .catch((err) => {
        setFetching(false);
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };

  const mapClaimsResult = () => {
    return policieResult.map((xxx) => (
      <tr key={xxx._id}>
        <td>
          {xxx.firstName} {xxx.lastName}
        </td>

        <td>{xxx.email || "-"}</td>
        {/* <td>Dom</td> */}
        <td>{xxx.mobileNumber || "-"}</td>
        <td>{xxx.policyNumber || "-"}</td>
        <td>{xxx.productCatalogue || "-"}</td>
       
        <td>{xxx.startDate || "-"}</td>
        <td>{xxx.endDate || "-"}</td>
        <td>{xxx.previousClaims || "-"}</td>
        <td>{xxx.Provide_No_Claims || "-"}</td>
     
        <td>
          {/* <MenuEntity
            DeleteInsuranceEntity={DeleteInsuranceEntity}
            insureId={xxx._id}
          /> */}
          <small>
         -
          </small>
        </td>
      </tr>
    ));
  };

  React.useEffect(()=>{
    FetchClaims()
  },[])
  return (
    <div className="container-fluid">
      <div className="row page-title">
        <div className="col-md-12">
          <nav aria-label="breadcrumb" className="float-right mt-1">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">IPolicyMart</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/admin-policies">Admin - Claims</a>
              </li>
            </ol>
          </nav>
          <h3 className="mb-1 mt-0">
            <b> Claims</b>
          </h3>
          <span>
            {/* (
            {policieResult.length < pagination.total &&pagination.total>pagination.limit
              ? policieResult.length 
              : pagination.total + pagination.limit} */}
            Total {pagination.total}
            <div style={{ height: "4px" }} />
            {pagination.limit * pageNo < pagination.total ? (
              <div>
                <button onClick={handleNextPage} className="btn3">
                  Next
                </button>
              </div>
            ) : null}
          </span>
        </div>
      </div>
{/* spinner start */}
<div className="row justify-content-end">
        {fetching ? (
          <div className="text-center">
            <CircularProgress
              size={20}
              // color="secondary"
            />
          </div>
        ) : null}
        {/* <div style={{ float: "right" }} className="col-2">
          <button onClick={handleOpen} className="btn1">
            Add <AddCircleOutlineIcon />
          </button>
        </div> */}
      </div>
{/* spinner end */}
      {/* table  here */}
      <div style={{ MaxWidth: "100%", overflowX: "scroll" }} className="row">
        <div className="col-12">
          <table class="styled-table">
            <thead>
              <tr>
                <th>Name</th>
                <th>email </th>
                {/* <th>Developer Portal</th> */}
                <th>Mobile Number</th>
                <th>Policy Number</th>
                <th>Catalogue</th>
                <th>Start Data</th>
                <th>End Date</th>
                <th>Previous Claims?</th>
                <th>Provide A No Claims.</th>
                <th>-</th>
              </tr>
            </thead>

            <tbody>{mapClaimsResult()}</tbody>
          </table>
        </div>
      </div>
    
    </div>
  );
};

export default AdminClaims;

import React from "react";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { useSelector, useDispatch } from "react-redux";
import { CircularProgress } from "@material-ui/core";
import { SYNCUSERDATA } from "../../redux/action";
import axios from "axios";

const Profile = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector(({ user }) => user.currentUser);
  const [loading, setLoading] = React.useState(false);
  const { token } = useSelector(({ user }) => user.currentUser);
  const [userProfileData, setUserprofileData] = React.useState({
    firstName: "",
    lastName: "",
    Email: "",
    maritalStatus: "",
    mobileNumber: "",
    dateOfBirth: "",
  });
  var updateProfile = async () => {
    setLoading(true);
    await axios
      .post(
        `/api/v1/updateProfile`,
        { ...userProfileData },
        { headers: { authorization: token } }
      )
      .then((response) => {
        setLoading(false);

        console.log(response.data);
        response.data.userData &&
          dispatch(SYNCUSERDATA(response.data.userData));
        alert("profile updated");
      })
      .catch((err) => {
        setLoading(false);
        alert("Fail");
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };
  React.useEffect(() => {
    setUserprofileData({
      ...userProfileData,
      firstName: currentUser.user.firstName,
      lastName: currentUser.user.lastName,
      Email: currentUser.user.Email,
      maritalStatus: currentUser.user.maritalStatus,
      mobileNumber: currentUser.user.mobileNumber,
      dateOfBirth: currentUser.user.dateOfBirth,
    });
  }, []);
  return (
    <div className="container-fluid">
      {loading ? (
        <div className="loaderOverLay2">
          <CircularProgress
            fontSize="large"
            style={{ color: "orange", marginRight: "10px" }}
          />
          <h6>Please wait..</h6>
        </div>
      ) : null}
      <div className="row page-title">
        <div className="col-md-12">
          <nav aria-label="breadcrumb" className="float-right mt-1">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">IPolicyMart</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/users-profile">Profile</a>
              </li>
            </ol>
          </nav>
          <h3 className="mb-1 mt-0">
            <b>Profile</b>
          </h3>
        </div>
      </div>
      <div style={{ height: "10px" }} />

      <div className="row">
        <div className="col-lg-10">
          <div className="verticalCenterRow">
            <AccountCircleIcon
              style={{ fontSize: "80px", marginRight: "15px" }}
            />
            <div>
              <h5>
                {currentUser.user.firstName} {currentUser.user.lastName}
              </h5>
            </div>
          </div>
          <div className="col-lg-3 col-sm-4 ">
            <div className="card card-with-shadow">
              <div className="card-body">
                <div style={{ height: "5px" }} />
                <h6>Personnal Details</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style={{ height: "40px" }} />
      <div className="row">
        <div className="col-lg-5 user-profile-input">
          <small>Your First Name</small>
          <input
            type="text"
            className="input-cars"
            onChange={(e) =>
              setUserprofileData({
                ...userProfileData,
                firstName: e.target.value,
              })
            }
            value={userProfileData.firstName}
          />
        </div>
        <div className="col-lg-5 user-profile-input">
          <small>Your Last Name</small>
          <input
            type="text"
            className="input-cars"
            onChange={(e) =>
              setUserprofileData({
                ...userProfileData,
                lastName: e.target.value,
              })
            }
            value={userProfileData.lastName}
          />
        </div>
        <div className="col-lg-5 user-profile-input">
          <small>Your Mobile Number</small>
          <input
            type="text"
            onChange={(e) =>
              setUserprofileData({
                ...userProfileData,
                mobileNumber: e.target.value,
              })
            }
            value={userProfileData.mobileNumber}
            className="input-cars"
          />
        </div>
        <div className="col-lg-5 user-profile-input">
          <small>Your Email</small>
          <input
            type="text"
            onChange={(e) =>
              setUserprofileData({ ...userProfileData, Email: e.target.value })
            }
            className="input-cars"
            value={userProfileData.Email}
          />
        </div>
        <div className="col-lg-5 user-profile-input">
          <small>Your Date Of Birth</small>
          <input
            type="date"
            className="input-cars"
            onChange={(e) =>
              setUserprofileData({
                ...userProfileData,
                dateOfBirth: e.target.value,
              })
            }
            value={userProfileData.dateOfBirth}
          />
        </div>
        <div className="col-lg-5 user-profile-input">
          <small>Marital Status</small>
          <input
            type="text"
            className="input-cars"
            value={currentUser.user.martiatStatus}
          />
        </div>
      </div>
      <div className="col-4">
        <button onClick={updateProfile} className="btn1">
          Save
        </button>
      </div>
    </div>
  );
};

export default Profile;

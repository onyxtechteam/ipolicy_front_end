import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETTRICYCLEPROCESSINFO } from "../../../redux/action";

const Threestep0 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const handlefocused = (value) => setFocused(value);
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const [formResponse, setFormresponse] = React.useState({
    tricycleNumber: "",
  });
  const handleNext = () => {
    if (!formResponse.tricycleNumber) {
      return setErrorAlert({
        status: true,
        message:
          "Provide your Tricycle Re. Number or click 'Proceed without..' ",
      });
    }
    dispatch(
      SETTRICYCLEPROCESSINFO({ tricycleNumber: formResponse.tricycleNumber })
    );
    history.push("/three-insurance-step1");
  };
  const handleNextnoNumber = () => {
    dispatch(SETTRICYCLEPROCESSINFO({ tricycleNumber: "Not provided" }));
    history.push("/three-insurance-step1");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Tricycle Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">What Is Your Tricycle Number:</p>
        <div className="col-lg-4">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <input
          onChange={(e) =>
            setFormresponse({ ...formResponse, tricycleNumber: e.target.value })
          }
          value={formResponse.tricycleNumber}
          className="input-cars"
          placeholder="Tricycle Number"
        />
        <div style={{ height: "20px" }} />
        {/* <div className="row carsList">
        
        </div> */}
        <div style={{ height: "20px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleNextnoNumber} className="btn1 ">
            Proceed Without Tricycle Number
          </button>
          <button onClick={handleNext} className="btn1 ">
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Threestep0;

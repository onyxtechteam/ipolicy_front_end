import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETTRICYCLEPROCESSINFO } from "../../../redux/action";
const Threestep2 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const handleBack = () => history.goBack();
  const [focused, setFocused] = React.useState(null);
  const [formresponse, setformresponse] = React.useState({
    tricycleFuelType: "",

    tricycleVarient: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handlefocused = (value) => setFocused(value);

  const handleNext = () => {
    console.log(formresponse);
    if (!formresponse.tricycleFuelType || !formresponse.tricycleVarient) {
      return setErrorAlert({
        status: true,
        message: "You did not select all fields",
      });
    }
    dispatch(SETTRICYCLEPROCESSINFO(formresponse));
    history.push("/three-insurance-step3");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Tricycle Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Fuel Type:</p>
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>

        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          {/* begin card */}
          <div
            onClick={() =>
              setformresponse({ ...formresponse, tricycleFuelType: "Petrol" })
            }
            className={`cardB ${
              formresponse.tricycleFuelType === "Petrol" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Petrol
            </button>
          </div>

          <div
            onClick={() =>
              setformresponse({ ...formresponse, tricycleFuelType: "diesel" })
            }
            className={`cardB ${
              formresponse.tricycleFuelType === "diesel" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Diesel
            </button>
          </div>
          {/* end card */}
          {/* end card */}
        </div>

        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Variant Type:</p>
        <div style={{ height: "10px" }} />
        <div className="row carsModel">
          {/* begin card */}
          <div
            onClick={() =>
              setformresponse({ ...formresponse, tricycleVarient: "automatic" })
            }
            className={`cardB ${
              formresponse.tricycleVarient === "automatic" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Automatic
            </button>
          </div>

          <div
            onClick={() =>
              setformresponse({ ...formresponse, tricycleVarient: "manual" })
            }
            className={`cardB ${
              formresponse.tricycleVarient === "manual" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Manual
            </button>
          </div>
          {/* end card */}
          {/* end card */}
        </div>
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Threestep2;

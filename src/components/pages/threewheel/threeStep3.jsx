import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETTRICYCLEPROCESSINFO } from "../../../redux/action";
const bikeRegistrationYear = [
  { name: "brand new tricycle" },
  { name: "2020" },
  { name: "2019" },
  { name: "2018" },
  { name: "2017" },
  { name: "2016" },
  { name: "2015" },
  { name: "2014" },
  { name: "2013" },
  { name: "2012" },
  { name: "2011" },
  { name: "2010" },
  { name: "2009" },
  { name: "2008" },
  { name: "2007" },
  { name: "2006" },
  { name: "2005" },
  { name: "2004" },
  { name: "2003" },
  { name: "2002" },
  { name: "2001" },
  { name: "2000" },
  { name: "1999" },
  { name: "1998" },
  { name: "1997" },
  { name: "1996" },
  { name: "1995" },
];
const Threetep3 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const handleBack = () => history.goBack();
  const [formresponse, setFormresponse] = React.useState({
    tricycleRegisterYear: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });

  const handleNext = () => {
    if (!formresponse.tricycleRegisterYear) {
      return setErrorAlert({
        status: true,
        message: "You have not made a selection",
      });
    }
    dispatch(SETTRICYCLEPROCESSINFO(formresponse));
    history.push("/three-insurance-step5");
  };
  const mapCarYears = () => {
    return bikeRegistrationYear.map((xxx) => (
      <div
        onClick={() => setFormresponse({ tricycleRegisterYear: xxx.name })}
        className={`cardB ${
          formresponse.tricycleRegisterYear === xxx.name ? "focused" : null
        } col-lg-3 col-md-3 col-sm-3`}
      >
        <button style={{ width: "100%" }} className="btn3">
          {xxx.name}
        </button>
      </div>
    ));
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Tricycle Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Year:</p>
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>

        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          {/* begin card */}

          {mapCarYears()}
          {/* end card */}
          {/* end card */}
        </div>

        <div style={{ height: "20px" }} />
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Threetep3;

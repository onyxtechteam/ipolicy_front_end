import React from "react";
// import PlayArrowIcon from "@material-ui/icons/PlayArrow";
// import CheckCircleIcon from "@material-ui/icons/CheckCircle";
// import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
// import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
// import HeadsetMicIcon from "@material-ui/icons/HeadsetMic";
// import DescriptionIcon from "@material-ui/icons/Description";
// import BeenhereIcon from "@material-ui/icons/Beenhere";
// import ListAltIcon from "@material-ui/icons/ListAlt";
// import AllInboxIcon from "@material-ui/icons/AllInbox";
import HeaderTwo from "../../headertwo";
import { useHistory, useLocation } from "react-router-dom";

const Threewrapper = (props) => {
  const { location } = useHistory();

  return (
    <div>
      <HeaderTwo />
      <main>
        <section className="Car-section row">
          <div className="left-content">
            <ul>
              <li>
                <h4>
                  Three Wheeler{" "}
                  <span className="mobile-hide-left">insurance</span>
                </h4>
              </li>
              <li
                className={`${
                  location.pathname === "/three-insurance-step0" ||
                  location.pathname === "/three-insurance-step1" ||
                  location.pathname === "/three-insurance-step2" ||
                  location.pathname === "/three-insurance-step3"
                    ? //  ||
                      //  location.pathname === "/bike-insurance-step0"
                      "focused"
                    : null
                }`}
              >
                <img
                  className="left-side-img"
                  src="/images/cardetails.png"
                  alt=""
                />
                <span className="mobile-hide-left">Tricycle Details</span>
              </li>
              {/* <li
                className={`${
                  location.pathname === "/three-insurance-step4"
                    ? "focused"
                    : null
                }`}
              >
                <img
                  className="left-side-img"
                  src="/images/driverdetails.png"
                  alt=""
                />
                <span className="mobile-hide-left"> Driver Details</span>
              </li> */}
              <li
                className={`${
                  location.pathname === "/three-insurance-step5"
                    ? "focused"
                    : null
                }`}
              >
                <img
                  className="left-side-img"
                  width="20px"
                  src="/images/claimdetails.png"
                  alt=""
                />
                <span className="mobile-hide-left">Claim Details</span>
              </li>
              <li     className={`${
                  location.pathname === "/three-insurance-step5s"
                    ? "focused"
                    : null
                }`}>
                <img
                  className="left-side-img"
                  src="/images/insurancedetails.png"
                  alt=""
                />
                <span className="mobile-hide-left">Insurance Details</span>
              </li>
              <li
                className={`${
                  location.pathname === "/three-insurance-step6"
                    ? "focused"
                    : null
                }`}
              >
                <img
                  className="left-side-img"
                  src="/images/personaldetails.png"
                  alt=""
                />
                <span className="mobile-hide-left">Personal Details</span>
              </li>
            </ul>
          </div>
          <div className="right-content">{props.children}</div>
        </section>
      </main>
    </div>
  );
};

export default Threewrapper;

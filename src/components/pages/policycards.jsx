import React from "react";
import StarsIcon from "@material-ui/icons/Stars";
import CircularProgress from "@material-ui/core/CircularProgress";

export const CarPolicyCard = (props) => {
  const { data } = props;
  return (
    <div
      style={{ marginBottom: "20px", textTransform: "capitalize" }}
      className="col-sm-3  col-lg-4"
    >
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Car Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.coverType}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.product_type}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.carBrand}/{data.otherData.carModel}/
              {data.otherData.carRegisterYear}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};
export const TricycleCard = (props) => {
  const { data } = props;
  return (
    <div style={{ marginBottom: "20px" }} className="col-sm-3  col-lg-4">
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Tricycle Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.coverType}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.tricycleNumber}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.tricycleBrand}/
              {data.otherData.tricycleRegisterYear}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};
export const BikeCard = (props) => {
  const { data } = props;
  return (
    <div
      style={{ marginBottom: "20px", textTransform: "capitalize" }}
      className="col-sm-3  col-lg-4"
    >
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Bike Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.coverType}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.product_type}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.bikeBrand}/{data.otherData.bikeVarient}/
              {data.otherData.bikeRegisterYear}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};
export const GadgetCard = (props) => {
  const { data } = props;
  return (
    <div style={{ marginBottom: "20px" }} className="col-sm-3  col-lg-4">
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Gadget Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.Gadget_name} / {data.otherData.gadGet_brand}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Value - {data.otherData.gadget_price_value}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Brand: {data.otherData.gadGet_brand}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};
export const HealthCard = (props) => {
  const { data } = props;
  return (
    <div
      style={{ marginBottom: "20px", textTransform: "capitalize" }}
      className="col-sm-3  col-lg-4"
    >
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Health Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.insurance_type}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              person(s): {data.otherData.members_nos}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Purchased for: {data.otherData.Purchase_for}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};
export const HomeCard = (props) => {
  const { data } = props;
  return (
    <div style={{ marginBottom: "20px" }} className="col-sm-3  col-lg-4">
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Home Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.insurance_type}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Value(movable items) - {data.otherData.movable_items_value}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Value(non-mov. items) - {data.otherData.non_movable_items_value}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};
export const LifeCard = (props) => {
  const { data } = props;
  return (
    <div style={{ marginBottom: "20px" }} className="col-sm-3  col-lg-4">
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Life Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.cover_type}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Start Date : {data.otherData.startDate}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Start Date : {data.otherData.endDate}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};
export const TravelCard = (props) => {
  const { data } = props;
  return (
    <div style={{ marginBottom: "20px" }} className="col-sm-3  col-lg-4">
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Travel Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.insurance_type}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.country1} {data.otherData.country2}
              {","}
              {data.otherData.country3}
              {","}
              {data.otherData.country4}
              {","}
              {data.otherData.country5}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Trip Date - {data.otherData.tripdate_day}/
              {data.otherData.tripdate_month}/{data.otherData.tripdate_year}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};
export const MedicalCard = (props) => {
  const { data } = props;
  return (
    <div
      style={{ marginBottom: "20px", textTransform: "capitalize" }}
      className="col-sm-3  col-lg-4"
    >
      <div className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">Medical Insurance</h4>
          <div className="sub-header">
            <h6 style={{ color: "dodgerblue" }}>{data.insuranceEntity.name}</h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              {data.otherData.insurance_type}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              person(s): {data.otherData.members_nos}
            </h6>
            <h6>
              <StarsIcon style={{ color: "orange", marginRight: "4px" }} />
              Purchased for: {data.otherData.Purchase_for}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};

export const AdminDashCards = (props) => {
  const { count, title, loading } = props;
  return (
    <div
      style={{ marginBottom: "20px", textTransform: "capitalize" }}
      className="col-sm-3  col-lg-4 text-center"
    >
      <div style={{ minHeight: "150px" }} className="card card-with-shadow">
        <div className="card-body">
          <h4 className="header-title mt-0 mb-1">{title}</h4>
          <div style={{ height: "10px" }} />
          <div className="sub-header">
            {loading ? (
              <div className="">
                <CircularProgress size={20} color="secondary" />
              </div>
            ) : (
              <h1 style={{ color: "dodgerblue" }}>{count}</h1>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

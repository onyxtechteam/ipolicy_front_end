import React from "react";
import { useSelector } from "react-redux";
import ThumbUpAltIcon from "@material-ui/icons/ThumbUpAlt";

const Dashboard = () => {
  const currentUser = useSelector(({ user }) => user.currentUser);
  return (
    // <div className="container dashboard-right-component">
    //   <div className="col-9">
    //     <h5>
    //       Hey, {currentUser.user.firstName}{" "}
    //       <span>
    //         <ThumbUpAltIcon style={{ color: "orange", fontSize: "24px" }} />
    //       </span>
    //     </h5>
    //   </div>
    //   <div style={{ height: "30px" }} />
    //   <div style={{ minHeight: "100px" }} className="row ">
    //     <div className="col-9">
    //       <p>Recent Policies</p>
    //     </div>
    //   </div>
    //   <div style={{ height: "70px" }} />
    //   <div className="row">
    //     <div className="col-9">
    //       <p>Recent Searchs</p>
    //     </div>
    //   </div>
    // </div>

    <div className="">
      {/* Start Content*/}
      <div className="container-fluid">
        <div className="row page-title">
          <div className="col-md-12">
            <nav aria-label="breadcrumb" className="float-right mt-1">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="/">IPolicyMart</a>
                </li>
                <li className="breadcrumb-item">
                  <a href="/users-dashboard">Dashboard</a>
                </li>
              </ol>
            </nav>
            <h3 className="mb-1 mt-0">
              Hey, {currentUser.user.firstName}{" "}
              <span>
                <ThumbUpAltIcon style={{ color: "orange", fontSize: "24px" }} />
              </span>
            </h3>
          </div>
        </div>
        <div style={{ height: "30px" }} />
        <h5 className="mb-1 mt-0">Next Step</h5>
        <div style={{ height: "30px" }} />
        <div className="row">
          <div className="col-lg-6">
            <div className="card card-with-shadow">
              <div className="card-body">
                <h4 className="header-title mt-0 mb-1">Car Insurance</h4>
                <p className="sub-header">
                  Be ready for future claims by uploading mandatory documents.
                </p>
              </div>{" "}
              {/* end card-body*/}
            </div>{" "}
            {/* end card*/}
          </div>{" "}
          {/* end col*/}
          {/* end col*/}
        </div>
        {/* end row */}
        <div style={{ height: "30px" }} />
        <h5 className="mb-1 mt-0">Your Recent Policies</h5>
        {/* end row */}

        {/* end row */}
      </div>{" "}
      {/* container-fluid */}
    </div>
  );
};

export default Dashboard;

import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch } from "react-redux";
import { SETHOMEINSURANCEPROCESSINFO } from "../../../redux/action";
const Homestep1 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const item = history.location.state;
  const [focused, setFocused] = React.useState(null);
  const [formresponse, setformresponse] = React.useState({
    non_movable_items_value: "",
    movable_items_value: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    const { non_movable_items_value, movable_items_value } = formresponse;
    console.log(formresponse);
    if (!non_movable_items_value || !movable_items_value) {
      return setErrorAlert({
        status: true,
        message: "You did not select all fields",
      });
    }
    dispatch(SETHOMEINSURANCEPROCESSINFO(formresponse));
    history.push("/home-insurance-step2");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>
          Home Belongings{" "}
          <span style={{ color: "dodgerblue", textTransform: "capitalize" }}>
            {" "}
            - {item}
          </span>
        </h5>
        <div style={{ height: "20px" }} />
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <p className="midtext1">
          What Is The Value Of Your Non-Movable Home Contents ?
        </p>

        <div style={{ height: "20px" }} />
        <div className="col-10">
          <select
            // defaultValue={formresponse.members_nos}
            className="input-cars"
            id="countries-list"
            value={formresponse.non_movable_items_value}
            onChange={(e) => {
              e.target.value &&
                setformresponse({
                  ...formresponse,
                  non_movable_items_value: e.target.value,
                });
            }}
            style={
              {
                // width: "80%",
                // border: "none",
                // borderRadius: "4px",
                // height: "40px",
              }
            }
          >
            <option value="Select Value">Select Value</option>
            <option value="N1-N1 Million">N1-N1 Million</option>
            <option value="N2 Million-N10 Million">
              N2 Million-N10 Million
            </option>
            <option value="N20 Million-N40 Million">
              N20 Million-N40 Million
            </option>
            <option value="N40 Million-N200 Million">
              N40 Million-N200 Million
            </option>
          </select>
        </div>

        <div style={{ height: "20px" }} />
        <p className="midtext1">
          What Is The Value Of Your Movable Personal Belongings ?
        </p>
        <div style={{ height: "10px" }} />
        <div className="col-10">
          <select
            // defaultValue={formresponse.members_nos}
            className="input-cars"
            id="countries-list"
            value={formresponse.movable_items_value}
            onChange={(e) => {
              e.target.value &&
                setformresponse({
                  ...formresponse,
                  movable_items_value: e.target.value,
                });
            }}
            style={
              {
                // width: "80%",
                // border: "none",
                // borderRadius: "4px",
                // height: "40px",
              }
            }
          >
            <option value="Select Value">Select Value</option>
            <option value="N1-N1 Million">N1-N1 Million</option>
            <option value="N2 Million-N10 Million">
              N2 Million-N10 Million
            </option>
            <option value="N20 Million-N40 Million">
              N20 Million-N40 Million
            </option>
            <option value="N40 Million-N200 Million">
              N40 Million-N200 Million
            </option>
          </select>
        </div>

        <div style={{ height: "40px" }} />
        <div className="row carsModel">
          {/* begin healthd */}

          {/* end healthd */}
          {/* end healthd */}
        </div>
        <div style={{ height: "10px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleBack} className="btn2 ">
            back
          </button>
          <button
            style={{ width: "150px" }}
            onClick={handleNext}
            className="btn1 healthNextbtn"
          >
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Homestep1;

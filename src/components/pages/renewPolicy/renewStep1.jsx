import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETRENEWPROCESS } from "../../../redux/action";

const Renewstep1 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [policyNumber, setpolicyNumber] = React.useState("");
  const handleBack = () => history.goBack();
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handleNext = () => {
    if (!policyNumber) {
      return setErrorAlert({
        status: true,
        message: "Provide policy number",
      });
    }
    dispatch(SETRENEWPROCESS({ policyNumber: policyNumber }));
    history.push("/renew-policy-step2");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Insurance Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">What Is Your Policy Number:</p>
        <div className="col-lg-4">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <input
          onChange={(e) => setpolicyNumber(e.target.value)}
          value={policyNumber}
          className="input-cars"
          placeholder="Policy Number"
        />
        <div style={{ height: "20px" }} />

        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Renewstep1;

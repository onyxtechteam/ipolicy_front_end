import React from "react";

const Claims = () => {
  return (
    // <div className="container dashboard-right-component">
    //   <div className="col-9">
    //     <h5>Claims</h5>
    //   </div>
    // </div>

    <div>
      {/* Start Content*/}
      <div className="container-fluid">
        <div className="row page-title">
          <div className="col-md-12">
            <nav aria-label="breadcrumb" className="float-right mt-1">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="/">IPolicyMart</a>
                </li>
                <li className="breadcrumb-item">
                  <a href="/users-claims">Claims</a>
                </li>
              </ol>
            </nav>
            <h3 className="mb-1 mt-0">
              <b>Claims</b>
            </h3>
          </div>
        </div>

        <div className="row ">
          <div className="col-lg-6">
            <div className="">
              <div className="card-body" style={{ backgroundColor: "inherit" }}>
                <span style={{ fontSize: "20px" }} className="mb-1 mt-0">
                  Need help on <h1>claims settlement ?</h1>
                </span>
                <h3 className="header-title mt-0 mb-1">
                  We've got you covered
                </h3>
                <p className="sub-header">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Quis ipsum suspendisse ultrices gravida.
                </p>
                <div style={{ height: "30px" }} />
              </div>{" "}
              {/* end card-body*/}
            </div>{" "}
            {/* end card*/}
          </div>{" "}
          <div className="col-lg-5">
            <div className="card card-with-shadow">
              <div style={{ textAlign: "center" }} className="card-body ">
                <img width="60%" src="/images/customer.png" alt="" />
                <h4 className="header-title mt-0 mb-1">
                  Got questions regarding Insurance claim?
                </h4>

                <p className="sub-header">Talk to IpolicyMart claims expert</p>
                <a href="tel:+23480-ipolicymart">
                  <button className="btn1">Call +23480-ipolicymart</button>
                </a>

                <p className="sub-header"></p>
              </div>{" "}
              {/* end card-body*/}
            </div>{" "}
            {/* end card*/}
          </div>{" "}
          {/* end col*/}
          {/* end col*/}
        </div>
        {/* end row */}

        {/* end row */}

        {/* end row */}
      </div>{" "}
      {/* container-fluid */}
    </div>
  );
};

export default Claims;

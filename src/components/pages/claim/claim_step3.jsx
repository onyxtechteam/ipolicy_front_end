import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETCLAIMPROCESS } from "../../../redux/action";
const Claimsstep3 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    if (!focused) {
      return setErrorAlert({
        status: true,
        message: "You have not made a selection",
      });
    }
    dispatch(SETCLAIMPROCESS({ policyType: focused }));
    history.push("/insurance-claims4");
  };
  return (
    <div className="container">
      <div className="col-lg-10">
        <h5>Insurance Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Policy Type:</p>
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <div className="row carsModel">
          {/* begin card */}
          <div
            onClick={() => setFocused("Life")}
            className={`cardB ${
              focused === "Life" ? "focused" : null
            } col-lg-2 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Life
            </button>
          </div>
          {/* end card */}
          <div
            onClick={() => setFocused("Bike")}
            className={`cardB ${
              focused === "Bike" ? "focused" : null
            } col-lg-2 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Bike
            </button>
          </div>
          {/* begin card */}
          {/* <div
            onClick={() => setFocused("Home")}
            className={`cardB ${
              focused === "Home" ? "focused" : null
            } col-lg-2 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Home
            </button>
          </div> */}
          {/* begin card */}
          <div
            onClick={() => setFocused("Gadget")}
            className={`cardB ${
              focused === "Gadget" ? "focused" : null
            } col-lg-2 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Gadget
            </button>
          </div>
          {/* emd card */}
          {/* begin card */}

          {/* emd card */}
          <div
            onClick={() => setFocused("Auto")}
            className={`cardB ${
              focused === "Auto" ? "focused" : null
            } col-lg-2 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Auto
            </button>
          </div>
          {/* end card */}
          {/* end card */}
          {/* begin card */}
          {/* begin card */}
          <div
            onClick={() => setFocused("Travel")}
            className={`cardB ${
              focused === "Travel" ? "focused" : null
            } col-lg-2 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Travel
            </button>
          </div>
          {/* end card */}
          {/* end card */}
          {/* begin card */}
          {/* begin card */}
          <div
            onClick={() => setFocused("Medical")}
            className={`cardB ${
              focused === "Medical" ? "focused" : null
            } col-lg-2 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Medical
            </button>
          </div>
          {/* end card */}
          {/* end card */}
        </div>
        <div style={{ height: "40px" }} />
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Claimsstep3;

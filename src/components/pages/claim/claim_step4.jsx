import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETCLAIMPROCESS } from "../../../redux/action";
const Claimsstep4 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const handleBack = () => history.goBack();
  // const [focused, setFocused] = React.useState(null);
  const [formresponse, setFormresponse] = React.useState({
    startDate: "",
    endDate: "",
  });

  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  // const handlefocused = (value) => setFocused(value);

  const handleNext = () => {
    if (!formresponse.startDate || !formresponse.endDate) {
      return setErrorAlert({
        status: true,
        message: "You did not select all fields",
      });
    }
    dispatch(SETCLAIMPROCESS(formresponse));
    history.push("/insurance-claims5");
  };
  return (
    <div className="container">
      <div className="col-lg-10">
        <h5>Insurance Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Enter The Start Date Or End Date:</p>
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          <div className="col-lg-2 col-md-3 col-sm-3">
            <input
              onChange={(e) =>
                setFormresponse({ ...formresponse, startDate: e.target.value })
              }
              value={formresponse.startDate}
              className="input-cars"
              type="date"
            />
          </div>
          <div className="col-lg-2 col-md-3 col-sm-3">
            <input
              onChange={(e) =>
                setFormresponse({ ...formresponse, endDate: e.target.value })
              }
              value={formresponse.endDate}
              className="input-cars"
              type="date"
            />
          </div>
        </div>
        <div style={{ height: "40px" }} />
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Claimsstep4;

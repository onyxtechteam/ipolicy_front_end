import React from "react";

export const CarDetailCard = (props) => {
  const { data } = props;
  return (
    <ul className="detailUserPolicy">
      <button className="btn4">View Users DocUments</button>
      <li>
        <span>Customer Name:</span>
        <span>
          {data.firstName} {data.lastName}
        </span>
      </li>
      <li>
        <span>MobileNumber:</span>
        <span>{data.mobileNumber}</span>
      </li>
      <li>
        <span>Email:</span>
        <span>{data.email}</span>
      </li>
      <li>
        <span>Date Of Birth:</span>
        <span>
          {data.dob_day}/{data.dob_month}/{data.dob_year}
        </span>
      </li>
      <li>
        <span>address:</span>
        <span>{data.otherData.address}</span>
      </li>
      <li>
        <span>Provide A No Claims?:</span>
        <span>{data.Provide_No_Claim}</span>
      </li>
      <li>
        <span>Product Catalogue:</span>
        <span>{data.productCatalogue}</span>
      </li>
      <li>
        <span>Car Brand:</span>
        <span>{data.otherData.carBrand}</span>
      </li>
      <li>
        <span>Car Model:</span>
        <span>{data.otherData.carModel}</span>
      </li>
      <li>
        <span>Car Year:</span>
        <span>{data.otherData.carRegisterYear} </span>
      </li>
      <li>
        <span>coverType:</span>
        <span>{data.otherData.coverType} </span>
      </li>
      <li>
        <span>insurance type:</span>
        <span>{data.otherData.insurance_type} </span>
      </li>
      <li>
        <span>Previous Claims:</span>
        <span>{data.otherData.previousClaims}</span>
      </li>
      <li>
        <span>product type:</span>
        <span>{data.otherData.product_type}</span>
      </li>
      <li>
        <span>Insurer:</span>
        <span>{data.insuranceEntity.name}</span>
      </li>
      <li>
        <span>Insurer Phone No:</span>
        <span>{data.insuranceEntity.telephone}</span>
      </li>
      <li>
        <span>Insurer Quote Price(car):</span>
        <span>{data.insuranceEntity.carQuotePrice}</span>
      </li>
    </ul>
  );
};

// three wheeler start
export const ThreeDetailCard = (props) => {
  const { data } = props;
  return (
    <ul className="detailUserPolicy">
      <button className="btn4">View Users DocUments</button>
      <li>
        <span>Customer Name:</span>
        <span>
          {data.firstName} {data.lastName}
        </span>
      </li>
      <li>
        <span>MobileNumber:</span>
        <span>{data.mobileNumber}</span>
      </li>
      <li>
        <span>Email:</span>
        <span>{data.email}</span>
      </li>
      <li>
        <span>Date Of Birth:</span>
        <span>
          {data.dob_day}/{data.dob_month}/{data.dob_year}
        </span>
      </li>
      <li>
        <span>address:</span>
        <span>{data.otherData.address}</span>
      </li>
      <li>
        <span>Provide A No Claims?:</span>
        <span>{data.Provide_No_Claim}</span>
      </li>
      <li>
        <span>Product Catalogue:</span>
        <span>{data.productCatalogue}</span>
      </li>
      <li>
        <span>tricycle Brand:</span>
        <span>{data.otherData.tricycleBrand}</span>
      </li>
      <li>
        <span>tricycle Model:</span>
        <span>{data.otherData.tricycleModel}</span>
      </li>
      <li>
        <span>tricycle Year:</span>
        <span>{data.otherData.tricycleRegisterYear} </span>
      </li>
      <li>
        <span>tricycle Fuel Type:</span>
        <span>{data.otherData.tricycleFuelType} </span>
      </li>
      <li>
        <span>coverType:</span>
        <span>{data.otherData.coverType} </span>
      </li>
      <li>
        <span>insurance type:</span>
        <span>{data.otherData.insurance_type} </span>
      </li>
      <li>
        <span>Previous Claims:</span>
        <span>{data.otherData.previousClaims}</span>
      </li>
      <li>
        <span>product type:</span>
        <span>{data.otherData.product_type}</span>
      </li>
      <li>
        <span>Insurer:</span>
        <span>{data.insuranceEntity.name}</span>
      </li>
      <li>
        <span>Insurer Phone No:</span>
        <span>{data.insuranceEntity.telephone}</span>
      </li>
      <li>
        <span>Insurer Quote Price(car):</span>
        <span>{data.insuranceEntity.thricycleQuotePrice}</span>
      </li>
    </ul>
  );
};
//three wheeler end
export const BikeDetailCard = (props) => {
  const { data } = props;
  return (
    <ul className="detailUserPolicy">
      <button className="btn4">View Users DocUments</button>
      <li>
        <span>Customer Name:</span>
        <span>
          {data.firstName} {data.lastName}
        </span>
      </li>
      <li>
        <span>MobileNumber:</span>
        <span>{data.mobileNumber}</span>
      </li>
      <li>
        <span>Email:</span>
        <span>{data.email}</span>
      </li>
      <li>
        <span>Date Of Birth:</span>
        <span>
          {data.dob_day}/{data.dob_month}/{data.dob_year}
        </span>
      </li>
      <li>
        <span>address:</span>
        <span>{data.otherData.address}</span>
      </li>
      <li>
        <span>Provide A No Claims?:</span>
        <span>{data.Provide_No_Claim}</span>
      </li>
      <li>
        <span>Product Catalogue:</span>
        <span>{data.productCatalogue}</span>
      </li>
      <li>
        <span>bike Brand:</span>
        <span>{data.otherData.bikeBrand}</span>
      </li>
      <li>
        <span>bike Model:</span>
        <span>{data.otherData.bikeModel}</span>
      </li>
      <li>
        <span>bike Year:</span>
        <span>{data.otherData.bikeRegisterYear} </span>
      </li>
      <li>
        <span>coverType:</span>
        <span>{data.otherData.coverType} </span>
      </li>
      <li>
        <span>insurance type:</span>
        <span>{data.otherData.insurance_type} </span>
      </li>
      <li>
        <span>Payment status:</span>
        <span>{data.paymentDetails.status}</span>
      </li>
      <li>
        <span>Previous Claims:</span>
        <span>{data.otherData.previousClaims}</span>
      </li>
      <li>
        <span>product type:</span>
        <span>{data.otherData.product_type}</span>
      </li>
      <li>
        <span>Insurer:</span>
        <span>{data.insuranceEntity.name}</span>
      </li>
      <li>
        <span>Insurer Phone No:</span>
        <span>{data.insuranceEntity.telephone}</span>
      </li>
      <li>
        <span>Insurer Quote Price(bike):</span>
        <span>{data.insuranceEntity.bikeQuotePrice}</span>
      </li>
    </ul>
  );
};

export const LifeDetailCard = (props) => {
  const { data } = props;
  return (
    <ul className="detailUserPolicy">
      <button className="btn4">View Users DocUments</button>
      <li>
        <span>Customer Name:</span>
        <span>
          {data.firstName} {data.lastName}
        </span>
      </li>
      <li>
        <span>MobileNumber:</span>
        <span>{data.mobileNumber}</span>
      </li>
      <li>
        <span>Email:</span>
        <span>{data.email}</span>
      </li>
      <li>
        <span>Date Of Birth:</span>
        <span>
          {data.dob_day}/{data.dob_month}/{data.dob_year}
        </span>
      </li>
      <li>
        <span>address:</span>
        <span>{data.otherData.address}</span>
      </li>
      <li>
        <span>Provide A No Claims?:</span>
        <span>{data.Provide_No_Claim}</span>
      </li>
      <li>
        <span>Product Catalogue:</span>
        <span>{data.productCatalogue}</span>
      </li>

      <li>
        <span>coverType:</span>
        <span>{data.otherData.cover_type} </span>
      </li>
      <li>
        <span>Start Date:</span>
        <span>{data.otherData.startDate} </span>
      </li>
      <li>
        <span>Previous Claims:</span>
        <span>{data.otherData.previousClaims}</span>
      </li>

      <li>
        <span>Insurer:</span>
        <span>{data.insuranceEntity.name}</span>
      </li>
      <li>
        <span>Insurer Phone No:</span>
        <span>{data.insuranceEntity.telephone}</span>
      </li>
      <li>
        <span>Payment status:</span>
        <span>{data.paymentDetails.status}</span>
      </li>
      <li>
        <span>Insurer Quote Price(bike):</span>
        <span> NGN {data.insuranceEntity.lifeQuotePrice}</span>
      </li>
    </ul>
  );
};
export const HomeDetailCard = (props) => {
  const { data } = props;
  return (
    <ul className="detailUserPolicy">
      <button className="btn4">View Users DocUments</button>
      <li>
        <span>Customer Name:</span>
        <span>
          {data.firstName} {data.lastName}
        </span>
      </li>
      <li>
        <span>MobileNumber:</span>
        <span>{data.mobileNumber}</span>
      </li>
      <li>
        <span>Email:</span>
        <span>{data.email}</span>
      </li>
      {/* <li>
     <span>
     Date Of Birth:
     </span>
     <span>{data.dob_day}/{data.dob_month}/{data.dob_year}</span>
    </li> */}
      <li>
        <span>address:</span>
        <span>{data.otherData.address}</span>
      </li>

      {/* <li>
     <span>
     Provide A No Claims?:
     </span>
     <span>{data.Provide_No_Claim}</span>
    </li> */}
      <li>
        <span>Product Catalogue:</span>
        <span>{data.productCatalogue}</span>
      </li>
      <li>
        <span>movable items value:</span>
        <span>{data.otherData.movable_items_value}</span>
      </li>
      <li>
        <span>Non movable items value:</span>
        <span>{data.otherData.non_movable_items_value}</span>
      </li>
      <li>
        <span>Payment status:</span>
        <span>{data.paymentDetails.status}</span>
      </li>
      <li>
        <span>coverType:</span>
        <span>{data.otherData.insurance_type} </span>
      </li>
      <li>
        <span>Insurance Type:</span>
        <span>{data.otherData.insurance_type} </span>
      </li>
      {/* <li>
     <span>
     Start Date:
     </span>
     <span>{data.otherData.startDate}  </span>
    </li>
    <li>
     <span>
     Previous Claims:
     </span>
     <span>{data.otherData.previousClaims}</span>
    </li> */}

      <li>
        <span>Insurer:</span>
        <span>{data.insuranceEntity.name}</span>
      </li>
      <li>
        <span>Insurer Phone No:</span>
        <span>{data.insuranceEntity.telephone}</span>
      </li>
      <li>
        <span>Insurer Quote Price (Home):</span>
        <span> NGN {data.insuranceEntity.HomeQuotePrice}</span>
      </li>
    </ul>
  );
};

export const TravelDetailCard = (props) => {
  const { data } = props;
  return (
    <ul className="detailUserPolicy">
      <button className="btn4">View Users DocUments</button>
      <li>
        <span>Customer Name:</span>
        <span>
          {data.firstName} {data.lastName}
        </span>
      </li>
      <li>
        <span>MobileNumber:</span>
        <span>{data.mobileNumber}</span>
      </li>
      <li>
        <span>Email:</span>
        <span>{data.email}</span>
      </li>
      <li>
        <span>Date Of Birth:</span>
        <span>
          {data.dob_day}/{data.dob_month}/{data.dob_year}
        </span>
      </li>
      <li>
        <span>address:</span>
        <span>{data.otherData.address}</span>
      </li>
      {/* <li>
     <span>
     Provide A No Claims?:
     </span>
     <span>{data.Provide_No_Claim}</span>
    </li> */}
      <li>
        <span>Product Catalogue:</span>
        <span>{data.productCatalogue}</span>
      </li>

      <li>
        <span>Insurance Type:</span>
        <span>{data.otherData.insurance_type} </span>
      </li>
      <li>
        <span>Marital Status:</span>
        <span>{data.otherData.marital_status} </span>
      </li>
      <li>
        <span>Sex:</span>
        <span>{data.otherData.sex} </span>
      </li>
      <li>
        <span>Start Date:</span>
        <span>{data.otherData.startDate} </span>
      </li>
      <li>
        <span>End Date:</span>
        <span>{data.otherData.endDate} </span>
      </li>
      <li>
        <span>Country:</span>
        <span>
          {data.otherData.country1},{data.otherData.country2},
          {data.otherData.country3},{data.otherData.country4}
        </span>
      </li>
      <li>
        <span>Trip Date:</span>
        <span>
          {data.otherData.tripdate_day}/{data.otherData.tripdate_month}/
          {data.otherData.tripdate_year}
        </span>
      </li>
      <li>
        <span>Insurer:</span>
        <span>{data.insuranceEntity.name}</span>
      </li>
      <li>
        <span>Insurer Phone No:</span>
        <span>{data.insuranceEntity.telephone}</span>
      </li>
      <li>
        <span>Payment status:</span>
        <span>{data.paymentDetails.status}</span>
      </li>
      <li>
        <span>Insurer Quote Price (Travel):</span>
        <span> NGN {data.insuranceEntity.travelQuotePrice}</span>
      </li>
    </ul>
  );
};

export const HealthDetailCard = (props) => {
  const { data } = props;
  return (
    <ul className="detailUserPolicy">
      <button className="btn4">View Users DocUments</button>
      <li>
        <span>Customer Name:</span>
        <span>
          {data.firstName} {data.lastName}
        </span>
      </li>
      <li>
        <span>MobileNumber:</span>
        <span>{data.mobileNumber}</span>
      </li>
      <li>
        <span>Email:</span>
        <span>{data.email}</span>
      </li>
      <li>
        <span>Purchase for:</span>
        <span>{data.Purchase_for}</span>
      </li>
      <li>
        <span>Number Of People:</span>
        <span>{data.members_nos}</span>
      </li>
      <li>
        <span>address:</span>
        <span>{data.otherData.address}</span>
      </li>
      {/* <li>
       <span>
       Provide A No Claims?:
       </span>
       <span>{data.Provide_No_Claim}</span>
      </li> */}
      <li>
        <span>Product Catalogue:</span>
        <span>{data.productCatalogue}</span>
      </li>

      <li>
        <span>Insurance Type:</span>
        <span>{data.otherData.insurance_type} </span>
      </li>
      {data.otherData.member1 ? (
        <>
          <li>
            <span>Marital Status (member 1):</span>
            <span>{data.otherData.member1.marital_status} </span>
          </li>
          <li>
            <span>Sex (member 1):</span>
            <span>{data.otherData.member1.sex} </span>
          </li>
          <li>
            <span>Date Of Birth (member 1):</span>
            <span>
              {data.otherData.member1.dob_day}/
              {data.otherData.member1.dob_month}/
              {data.otherData.member1.dob_year}
            </span>
          </li>
          <li>
            <span>Relationship with sponsor:</span>
            <span>{data.otherData.member1.relationship_with_sponsor}</span>
          </li>
        </>
      ) : null}

      {/* <li>
          <span>Start Date:</span>
          <span>{data.otherData.startDate} </span>
        </li>
        <li>
          <span>End Date:</span>
          <span>{data.otherData.endDate} </span>
        </li>
        <li>
          <span>Country:</span>
          <span>
            {data.otherData.country1},{data.otherData.country2},
            {data.otherData.country3},{data.otherData.country4}
          </span>
        </li>
        <li>
          <span>Trip Date:</span>
          <span>
            {data.otherData.tripdate_day}/{data.otherData.tripdate_month}/{data.otherData.tripdate_year}
          </span>
        </li> */}
      <li>
        <span>Insurer:</span>
        <span>{data.insuranceEntity.name}</span>
      </li>
      <li>
        <span>Insurer Phone No:</span>
        <span>{data.insuranceEntity.telephone}</span>
      </li>
      <li>
        <span>Payment status:</span>
        <span>{data.paymentDetails.status}</span>
      </li>
      <li>
        <span>Insurer Quote Price (Health):</span>
        <span> NGN {data.insuranceEntity.healthQuotePrice}</span>
      </li>
    </ul>
  );
};

export const GadgetDetailCard = (props) => {
    const { data } = props;
    return (
      <ul className="detailUserPolicy">
        <button className="btn4">View Users DocUments</button>
        <li>
          <span>Customer Name:</span>
          <span>
            {data.firstName} {data.lastName}
          </span>
        </li>
        <li>
          <span>MobileNumber:</span>
          <span>{data.mobileNumber}</span>
        </li>
        <li>
          <span>Email:</span>
          <span>{data.email}</span>
        </li>
        <li>
          <span>Date Of Birth:</span>
          <span>
            {data.dob_day}/{data.dob_month}/{data.dob_year}
          </span>
        </li>
        <li>
          <span>address:</span>
          <span>{data.otherData.address}</span>
        </li>
        {/* <li>
       <span>
       Provide A No Claims?:
       </span>
       <span>{data.Provide_No_Claim}</span>
      </li> */}
        <li>
          <span>Product Catalogue:</span>
          <span>{data.productCatalogue}</span>
        </li>
  
        <li>
          <span>Gadget:</span>
          <span>{data.otherData.gadGet_type} </span>
        </li>
        <li>
          <span>Gadget Brand:</span>
          <span>{data.otherData.gadGet_brand} </span>
        </li>
        <li>
          <span>Gadget name:</span>
          <span>{data.otherData.Gadget_name} </span>
        </li>
     
     
       
        <li>
          <span>Insurer:</span>
          <span>{data.insuranceEntity.name}</span>
        </li>
        <li>
          <span>Insurer Phone No:</span>
          <span>{data.insuranceEntity.telephone}</span>
        </li>
        <li>
          <span>Payment status:</span>
          <span>{data.paymentDetails.status}</span>
        </li>
        <li>
          <span>Insurer Quote Price (Travel):</span>
          <span> NGN {data.insuranceEntity.gadgetQuotePrice}</span>
        </li>
      </ul>
    );
  };
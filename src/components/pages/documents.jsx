import React from "react";
import DescriptionIcon from "@material-ui/icons/Description";

const Documents = () => {
  return (
    <div className="container-fluid">
      <div className="row page-title">
        <div className="col-md-12">
          <nav aria-label="breadcrumb" className="float-right mt-1">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">IPolicyMart</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/users-document">Document</a>
              </li>
            </ol>
          </nav>
          <h3 className="mb-1 mt-0">
            <b>Document</b>
          </h3>
          <p className="sub-header">
            Here, you can upload document to help you in policy issuance
          </p>
        </div>
      </div>
      <div className="row justify-content-center">
        <div className="col-lg-6">
          <div style={{ minHeight: "300px" }} className="card card-with-shadow">
            <div style={{ textAlign: "center" }} className="card-body ">
              <div style={{ height: "40px" }} />
              <DescriptionIcon
                style={{ fontSize: "80px", color: "dodgerblue" }}
              />
              <div style={{ height: "40px" }} />
              <p className="sub-header">Kepp your Documents organized</p>
              <div style={{ height: "20px" }} />
              <a href="">
                <button className="btn4">+ Add Document</button>
              </a>

              <p className="sub-header"></p>
            </div>{" "}
            {/* end card-body*/}
          </div>{" "}
          {/* end card*/}
        </div>{" "}
      </div>
    </div>
  );
};

export default Documents;

import React from "react";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import TransitionsModal from "./entityModal";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import axios from "axios";
import CircularProgress from "@material-ui/core/CircularProgress";
import MenuEntity from "./menuentity";
import { useSelector } from "react-redux";

const AdminInsuranceEntities = () => {
  const [open, setOpen] = React.useState(false);
  const [fetching, setFetching] = React.useState();
  const { token } = useSelector(({ user }) => user.currentUser);
  const [insuranceEntity, setInsuranceEntity] = React.useState([]);
  const [pagination, setPagination] = React.useState({ total: 0, limit: "" });
  const [openAlert, setOpenAlert] = React.useState({
    status: false,
    message: "",
  });
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  var FetchInsuranceEntity = async () => {
    setFetching(true);
    await axios
      .get(`/api/v1/fetchinsuranceEntity?category=all`)
      .then((response) => {
        setFetching(false);

        console.log(response.data);
        response.data.userData &&
          response.data.userData.length > 0 &&
          setInsuranceEntity(response.data.userData);
        setPagination({
          ...pagination,
          limit: response.data.limit,
          total: response.data.total,
        });
      })
      .catch((err) => {
        setFetching(false);
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };

  var DeleteInsuranceEntity = async (insureId) => {
    setFetching(true);
    await axios
      .post(
        `/api/v1/DeleteInsuranceEntity`,
        { insureId: insureId },
        { headers: { authorization: token } }
      )
      .then((response) => {
        setFetching(false);

        handlesetOpen(true, "deleted");
        response.data.userData &&
          response.data.userData.length > 0 &&
          setInsuranceEntity(response.data.userData);
        setPagination({
          ...pagination,
          limit: response.data.limit,
          total: response.data.total,
        });
      })
      .catch((err) => {
        setFetching(false);
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };
  const mapInsuranceCoys = () => {
    return insuranceEntity.map((xxx) => (
      <tr>
        <td>{xxx.name}</td>
        <td>{xxx.tradeName || "-"}</td>
        {/* <td>Dom</td> */}
        <td>{xxx.carQuotePrice || "-"}</td>
        <td>{xxx.bikeQuotePrice || "-"}</td>
        <td>{xxx.thricycleQuotePrice || "-"}</td>
        <td>{xxx.HomeQuotePrice || "-"}</td>
        <td>{xxx.lifeQuotePrice || "-"}</td>
        <td>{xxx.travelQuotePrice || "-"}</td>
        <td>{xxx.healthQuotePrice || "-"}</td>
        <td>{xxx.gadgetQuotePrice || "-"}</td>
        <td>
          <MenuEntity
            DeleteInsuranceEntity={DeleteInsuranceEntity}
            insureId={xxx._id}
          />
        </td>
      </tr>
    ));
  };
  var handlesetOpen = (status, message) => {
    setOpenAlert({ ...openAlert, status, message });
    FetchInsuranceEntity();
  };
  React.useEffect(() => {
    FetchInsuranceEntity();
  }, []);
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  return (
    <div className="container-fluid">
      <div className="row page-title">
        <div className="col-md-12">
          <nav aria-label="breadcrumb" className="float-right mt-1">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">IPolicyMart</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/users-insuranceEntiries">Admin -Insurers</a>
              </li>
            </ol>
          </nav>
          <h3 className="mb-1 mt-0">
            <b> Manage Insurers </b>
          </h3>
          <span>
            ({insuranceEntity.length}/{pagination.total})
          </span>
        </div>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: "center",
          horizontal: "center",
        }}
        open={openAlert.status}
        autoHideDuration={6000}
        onClose={() => {
          handlesetOpen(false, "");
        }}
      >
        <Alert
          onClose={() => {
            handlesetOpen(false, "");
          }}
          severity="success"
        >
          {openAlert.message}
        </Alert>
      </Snackbar>
      <TransitionsModal
        handlesetOpen={handlesetOpen}
        handleClose={handleClose}
        open={open}
      />
      <div className="row justify-content-end">
        {fetching ? (
          <div className="text-center">
            <CircularProgress
              size={20}
              // color="secondary"
            />
          </div>
        ) : null}
        <div style={{ float: "right" }} className="col-2">
          <button onClick={handleOpen} className="btn1">
            Add <AddCircleOutlineIcon />
          </button>
        </div>
      </div>

      <div style={{ MaxWidth: "100%", overflowX: "scroll" }} className="row">
        <div className="col-12">
          <table class="styled-table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Trade Name</th>

                {/* <th>Developer Portal</th> */}
                <th>Car Quote Price</th>
                <th>Bike Quote Price</th>
                <th>Tricy. Quote Price</th>
                <th>Home Quote Price</th>
                <th>Life Quote Price</th>
                <th>Travel Quote Price</th>

                <th>Health Quote Price</th>
                <th>Gadget Quote Price</th>
                <th>-</th>
              </tr>
            </thead>

            <tbody>{mapInsuranceCoys()}</tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default AdminInsuranceEntities;

import React from "react";

const Payments = () => {
  return (
    <div className="container-fluid">
      <div className="row page-title">
        <div className="col-md-12">
          <nav aria-label="breadcrumb" className="float-right mt-1">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">IPolicyMart</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/users-payment">Payment</a>
              </li>
            </ol>
          </nav>
          <h3 className="mb-1 mt-0">
            <b>Payments</b>
          </h3>
        </div>
      </div>
    </div>
  );
};

export default Payments;

import React from "react";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
// import CheckCircleIcon from "@material-ui/icons/CheckCircle";
// import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
// import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
// import HeadsetMicIcon from "@material-ui/icons/HeadsetMic";
// import DescriptionIcon from "@material-ui/icons/Description";
// import BeenhereIcon from "@material-ui/icons/Beenhere";
// import ListAltIcon from "@material-ui/icons/ListAlt";
// import AllInboxIcon from "@material-ui/icons/AllInbox";
import { useHistory } from "react-router-dom";
import HeaderOne from "../headerone";
import { useDispatch } from "react-redux";
import { SETTRAVELINSURANCEPROCESSINFO } from "../../redux/action";
const TravelInsurance = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <div>
      <HeaderOne />
      <main className="mainBody">
        <div className="Main-section">
          <div className="container">
            <div className="verticalCenterRow">
              <div className="col-lg-5 col-sm-8 Main-content">
                <h3>Compare, Buy, Save</h3>
                <h1 className="Main-content-add">
                  Smart way to buy
                  <br />
                  Insurance
                </h1>
                <p>
                  <p>
                    Compare insurance policies, rate, detail, and <br />
                    buy instantly with IPolicyMart's best <br />
                    insurance companies in less
                    <br /> than five minutes!
                    <br />
                  </p>
                </p>
                <button className="btn1">Compare now</button>
                <div className="watch-video-main">
                  <p className="title4">
                    {" "}
                    <PlayArrowIcon />
                    Watch the intro video Get
                  </p>
                </div>
              </div>
              <div className="col-lg-6 col-sm-4 main-image-div">
                <img className src="/images/man2.png" alt="" />
              </div>
            </div>
            <div className="Main-img" />
          </div>
        </div>
        {/* end main-section */}
        <section className="partners">
          <div className="container">
            <div className="partnersRow verticalCenterRow">
              <div className="">
                <img src="/images/p6.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p1.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p2.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p3.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p4.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p5.png" alt="" />
              </div>
            </div>
          </div>
        </section>
        {/* begin choose us section */}
        <div id="choose-us" className="choose-us">
          <div className="container">
            <div className="verticalCenterRow justify-content-center">
              <div className="choose-us-main col-lg-10 col-sm-9">
                <h2 className="title1">Choose your Travel insurance</h2>
                <p className="sub-title">
                  Keep Your Life,Smile,Safe and Wealthy
                </p>
                <div className="row  choose-us-options justify-content-center">
                  {/* begin card */}

                  {/* end card */}

                  <div
                    onClick={() => {
                      history.push("/travel-insurance-step1");
                      dispatch(
                        SETTRAVELINSURANCEPROCESSINFO({
                          insurance_type: "Single Trip Plan",
                        })
                      );
                    }}
                    className="col-lg-3 col-sm-2"
                  >
                    <div className="card ">
                      <div className="card-body  justify-content-between">
                        <span className="verticalCenterRow ">
                          <h5 style={{ textAlign: "start" }}>
                            Single Trip
                            <br />
                            Plan
                          </h5>
                          <img
                            width="20px"
                            style={{ marginLeft: "40px" }}
                            className
                            src="/images/travel1.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Single-trip plans are designed for travellers who are
                          leaving their homes, visiting another destination,
                          domestic or international, and returning.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div
                    onClick={() => {
                      history.push("/travel-insurance-step1");
                      dispatch(
                        SETTRAVELINSURANCEPROCESSINFO({
                          insurance_type: "Multi Trip Plan",
                        })
                      );
                    }}
                    className="col-lg-3 col-sm-2"
                  >
                    <div className="card">
                      <div className="card-body">
                        <span className="verticalCenterRow">
                          <h5 style={{ textAlign: "start" }}>
                            Multi Trip
                            <br />
                            Plan
                          </h5>
                          <img
                            width="45px"
                            style={{ marginLeft: "30px" }}
                            className
                            src="/images/travel2.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Make your multi-stop international travel less
                          stressful.
                          <br />
                          <br /> <br />
                          <br />
                          <br />
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style={{ height: "60px" }} />
        <div className="get-app">
          <div className="container">
            <div className="verticalCenterRow justify-content-between get-app-container">
              <div style={{ padding: "20px" }} className="col-lg-6 col-sm-7">
                <h4 className="title1">
                  Get the iPolicyMart's App
                  <br />
                </h4>
                <p>Get control of all your insurance needs anywhere,anytime</p>
                <ul>
                  <li>
                    Compare different insurance policies
                    <br />
                  </li>
                  <li>
                    Buy, store and share all your policies online
                    <br />
                  </li>
                  <li>
                    Track your Policy status on the go <br />
                  </li>
                  <li>
                    Download your policy with a single tap <br />
                    savings are
                  </li>
                </ul>
                <div className="verticalCenterRow align-items-center">
                  <button
                    style={{
                      paddingLeft: "30px",
                      paddingRight: "30px",
                      marginRight: "10px",
                      backgroundColor: "#fff",
                      borderRadius: "10px",
                      color: "black",
                    }}
                    className="btn1"
                  >
                    <img
                      style={{ marginRight: "7px" }}
                      width="20px"
                      src="/images/playstore.png"
                      alt=""
                    />
                    Play store
                  </button>
                  <button
                    style={{
                      paddingLeft: "30px",
                      paddingRight: "30px",
                      marginRight: "30px",
                      border: "1px solid #fff",
                      borderRadius: "10px",
                    }}
                    className="btn1"
                  >
                    <img width="20px" src="/images/appstore.png" alt="" />
                    App store
                  </button>
                </div>
              </div>
              <div
                style={{ alignSelf: "flex-end" }}
                className="col-lg-5 col-sm-7 about-us-img"
              >
                <img width="110%" src="/images/mobiles.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </main>
      <footer>
        <div className="footer-section">
          <div className="container">
            <div className="footer-items">
              <div className="col-lg-4">
                <h2 className="title4">iPolicyMart</h2>
                <div>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore
                  </p>
                </div>
                <div className="subscribe-now verticalCenterRow">
                  <input placeholder="subscribe newsletter" />
                  <button>Send</button>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="verticalCenterRow">
                  <ul>
                    <li>Quicklinks</li>
                    <li>Home</li>
                    <li>Abouts</li>
                    <li>Features</li>
                    <li>Accounts</li>
                    <li>Contact</li>
                  </ul>
                  <ul>
                    <li>Company</li>
                    <li>About us</li>
                    <li>Carrer</li>
                    <li>Support</li>
                    <li>Affiliate</li>
                    <li>Program</li>
                  </ul>
                  <ul>
                    <li>Contacts</li>
                    <li>2348000000000</li>
                    <li>help@ipolicymarts.com</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="footer-divider" />
            <p className="foot-text">2020,CopyWrite| All Right Reserved</p>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default TravelInsurance;

import React from "react";
import HeaderTwo from "../headertwo";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Alert from "@material-ui/lab/Alert";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { LOGINSUCCESS } from "../../redux/action";
const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [passwordtype, setpasswordtype] = React.useState("password");
  const [passwordtype2, setpasswordtype2] = React.useState("password");
  const [mounted, setmounted] = React.useState(1);
  const [loading, setLoading] = React.useState(false);
  const [loading2, setLoading2] = React.useState(false);
  const [formResponse1, setFormresponse1] = React.useState({
    firstName: "",
    lastName: "",
    mobileNumber: "",
    email: "",
    password: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: [""],
  });
  const [formResponse2, setFormresponse2] = React.useState({
    firstName: "",
    lastName: "",
    mobileNumber: "",
    email: "",
    password: "",
    password2: "",
  });
  function validateEmail(email) {
    const re =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  const handleLogin = async (e) => {
    // console.log(formResponse1);
    e.preventDefault();
    if (!formResponse1.email) {
      return setErrorAlert({
        status: true,
        message: ["Provide a valid email"],
      });
    }
    if (!validateEmail(formResponse1.email)) {
      return setErrorAlert({
        status: true,
        message: ["Email not valid"],
      });
    }
    if (!formResponse1.password) {
      return setErrorAlert({
        status: true,
        message: ["Provide a password"],
      });
    }
    setLoading2(true);
    await axios
      .post(`/api/v1/login`, {
        Email: formResponse1.email,
        Password: formResponse1.password,
      })
      .then((response) => {
        setLoading2(false);
        // console.log(response.data);
        dispatch(LOGINSUCCESS(response.data.userData));
        history.push("/users-dashboard");
        // response.data.userData &&
        //   response.data.userData.length > 0 &&
      })
      .catch((err) => {
        setLoading2(false);
        if (err.response.data.message) {
          setErrorAlert({
            status: true,
            message: [err.response.data.message],
          });
        } else {
          setErrorAlert({
            status: true,
            message: ["An error occured, make sure you have a working network"],
          });
        }
        console.log(err);
      });
  };
  const handleSignUp = async (e) => {
    e.preventDefault();

    if (!formResponse2.firstName) {
      return setErrorAlert({
        status: true,
        message: ["Provide a first Name"],
      });
    }
    if (!formResponse2.lastName) {
      return setErrorAlert({
        status: true,
        message: ["Provide a last Name"],
      });
    }
    if (!formResponse2.email) {
      return setErrorAlert({
        status: true,
        message: ["Provide a valid email"],
      });
    }
    if (!validateEmail(formResponse2.email)) {
      return setErrorAlert({
        status: true,
        message: ["email not valid"],
      });
    }
    if (!formResponse2.mobileNumber) {
      return setErrorAlert({
        status: true,
        message: ["provide a valid phone number"],
      });
    }
    if (!formResponse2.password) {
      return setErrorAlert({
        status: true,
        message: ["password is required"],
      });
    }
    if (!formResponse2.password2) {
      return setErrorAlert({
        status: true,
        message: ["pls provide password confirmation"],
      });
    }
    if (formResponse2.password !== formResponse2.password2) {
      console.log(formResponse2.password, formResponse2.password2);
      return setErrorAlert({
        status: true,
        message: ["Both Passwords do not match"],
      });
    }
    setErrorAlert({
      status: false,
      message: [""],
    });
    console.log(formResponse2);

    setLoading(true);
    await axios
      .post(`/api/v1/Register`, formResponse2)
      .then((response) => {
        setLoading(false);
        console.log(response.data);
        dispatch(LOGINSUCCESS(response.data.userData));
        history.push("/users-dashboard");
        // response.data.userData &&
        //   response.data.userData.length > 0 &&
      })
      .catch((err) => {
        setLoading(false);
        if (err.response.data) {
          setErrorAlert({
            status: true,
            message: [err.response.data.message],
          });
        }
        console.log(err);
      });
  };

  const SIGNIN = () => {
    return (
      <div className="row">
        <form className="login-form" autoComplete={false}>
          {loading2 ? (
            <div className="text-center">
              <img width="40px" src="/images/spinner2.gif" alt="" />
            </div>
          ) : null}
          <p>Your Email Address</p>

          <input
            value={formResponse1.email}
            onChange={(e) =>
              setFormresponse1({
                ...formResponse1,
                email: e.target.value.toLowerCase(),
              })
            }
            className="input-cars"
            type="text"
          />
          <div style={{ height: "30px" }} />
          <div className="verticalCenterRow justify-content-between">
            <p>Your Password</p>
            <VisibilityIcon
              onClick={() => {
                passwordtype === "password"
                  ? setpasswordtype("text")
                  : setpasswordtype("password");
              }}
              style={{ cursor: "pointer" }}
            />
          </div>
          <div>
            <input
              value={formResponse1.password}
              onChange={(e) =>
                setFormresponse1({
                  ...formResponse1,
                  password: e.target.value,
                })
              }
              autoComplete="false"
              className="input-cars"
              type={passwordtype}
            />
          </div>
          <div style={{ height: "30px" }} />
          <div className="verticalCenterRow justify-content-between">
            <p>I forgot my password</p>
            <p>Remmember me</p>
          </div>
          <div style={{ height: "30px" }} />
          <div className="col-12">
            <button
              onClick={handleLogin}
              style={{ width: "100%" }}
              className="btn1 text-center"
            >
              Sign In
            </button>
            <div style={{ height: "4px" }} />
            <p className="text-center">Or</p>
            <button
              style={{ width: "100%", backgroundColor: "#3C5A96" }}
              className="btn1 text-center"
            >
              Sign in with Facebook
            </button>
          </div>
        </form>
      </div>
    );
  };
  const SIGNUP = () => {
    return (
      <div className="row">
        <form className="login-form" autoComplete="null">
          {loading ? (
            <div className="text-center">
              <img width="40px" src="/images/spinner2.gif" alt="" />
            </div>
          ) : null}
          <div className="row">
            <div className="col-6">
              <p>First Name</p>
              <input
                value={formResponse2.firstName}
                onChange={(e) =>
                  setFormresponse2({
                    ...formResponse2,
                    firstName: e.target.value,
                  })
                }
                className="input-cars"
                type="text"
                autoComplete="null"
              />
            </div>
            <div className="col-6">
              <p>Last Name</p>
              <input
                value={formResponse2.lastName}
                onChange={(e) =>
                  setFormresponse2({
                    ...formResponse2,
                    lastName: e.target.value,
                  })
                }
                type="text"
                className="input-cars"
                autoComplete="null"
              />
            </div>
          </div>
          <div style={{ height: "4px" }} />
          <p>Email Address</p>
          <input
            value={formResponse2.email}
            onChange={(e) =>
              setFormresponse2({
                ...formResponse2,
                email: e.target.value.toLowerCase(),
              })
            }
            type="text"
            className="input-cars"
            autoComplete="null"
          />
          <div style={{ height: "4px" }} />
          <p>Phone Number</p>
          <input
            value={formResponse2.mobileNumber}
            onChange={(e) =>
              setFormresponse2({
                ...formResponse2,
                mobileNumber: e.target.value,
              })
            }
            type="text"
            className="input-cars"
            autoComplete="null"
          />
          <div style={{ height: "4px" }} />
          <div className="verticalCenterRow justify-content-between">
            <p>Your Password</p>
            <VisibilityIcon
              onClick={() => {
                passwordtype2 === "password"
                  ? setpasswordtype2("text")
                  : setpasswordtype2("password");
              }}
              style={{ cursor: "pointer" }}
            />
          </div>
          <input
            value={formResponse2.password}
            onChange={(e) =>
              setFormresponse2({ ...formResponse2, password: e.target.value })
            }
            type={passwordtype2}
            className="input-cars"
            autoComplete="null"
          />
          <div style={{ height: "4px" }} />
          {/* <small>
            Password must be 8-20 characters, including: at least one capital
            letter, at least one small letter, one number and one special
            character - ! @ # $ % ^ & * ( ) _ +
          </small> */}
          <p>Confirm Password</p>
          <div>
            <input
              value={formResponse2.password2}
              onChange={(e) =>
                setFormresponse2({
                  ...formResponse2,
                  password2: e.target.value,
                })
              }
              type={passwordtype2}
              className="input-cars"
              autoComplete="null"
            />
          </div>
          <div style={{ height: "20px" }} />

          <div className="col-12">
            <button
              onClick={handleSignUp}
              style={{ width: "100%" }}
              className="btn1 text-center"
            >
              Sign up
            </button>
            <div style={{ height: "4px" }} />
            <p className="text-center">Or</p>
            <button
              style={{ width: "100%", backgroundColor: "#3C5A96" }}
              className="btn1 text-center"
            >
              Sign up with Facebook
            </button>
          </div>
        </form>
      </div>
    );
  };
  return (
    <div className="login-page">
      <HeaderTwo />
      <div className="container">
        <div className="row">
          <div className="login-left-section">
            <div className="container">
              <div className="row">
                <div className="col-4">
                  <div
                    onClick={() => setmounted(1)}
                    className={`loginradios ${
                      mounted === 1 ? "focused" : null
                    } verticalCenterRow `}
                  >
                    <input
                      checked={mounted === 1}
                      type="radio"
                      className="cars-input"
                    />
                    <span>SIGN IN</span>
                  </div>
                </div>
                <div className="col">
                  <div
                    onClick={() => setmounted(2)}
                    className={`loginradios ${
                      mounted === 2 ? "focused" : null
                    } verticalCenterRow `}
                  >
                    <input
                      checked={mounted === 2}
                      type="radio"
                      className="cars-input"
                    />
                    <span>CREATE ACCOUNT</span>
                  </div>
                </div>
              </div>
              <div className="col">
                <div style={{ height: "10px" }} />
                {errorAlert.status ? (
                  <Alert style={{ marginBottom: "10px" }} severity="error">
                    {errorAlert.message.map((xxx) => (
                      <small className="erroAlert">
                        {xxx}
                        <br />
                      </small>
                    ))}
                  </Alert>
                ) : null}
              </div>

              <div style={{ height: "10px" }} />
              {mounted === 1 ? SIGNIN() : mounted === 2 ? SIGNUP() : null}
            </div>
          </div>
          <div className="login-right-section">
            <div class="container">
              <h3>Compare, Buy, Save</h3>
              <h1 className="">
                Smart way to buy
                <br />
                Insurance
              </h1>
              <p className="login-small-text">
                Compare insurance policies, evaluate plans and <br />
                details and buy instantly from top insurance
                <br /> companies in Nigeria
              </p>
              <div className="row">
                <div className="col-5">
                  <div className="card cardc">
                    <div className="card-body text-center">
                      <div style={{ height: "20px" }} />
                      <img width="50px" src="./images/auto.png" alt="" />
                      <div style={{ height: "10px" }} />
                      <h6>Auto Insurance</h6>
                    </div>
                  </div>
                </div>
                <div className="col-5">
                  <div className="cardc card">
                    <div className="card-body text-center">
                      <div style={{ height: "20px" }} />
                      <img width="50px" src="./images/home.png" alt="" />
                      <div style={{ height: "10px" }} />
                      <h6>Home Insurance</h6>
                    </div>
                  </div>
                </div>
                <div className="col-5">
                  <div className="card cardc">
                    <div className="card-body text-center">
                      <div style={{ height: "20px" }} />
                      <img width="50px" src="./images/medical.png" alt="" />
                      <div style={{ height: "10px" }} />
                      <h6>Medical Insurance</h6>
                    </div>
                  </div>
                </div>
                <div className="col-5">
                  <div className="cardc card">
                    <div className="card-body text-center">
                      <div style={{ height: "20px" }} />
                      <img width="40px" src="./images/travel.png" alt="" />
                      <div style={{ height: "10px" }} />
                      <h6>Travel Insurance</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;

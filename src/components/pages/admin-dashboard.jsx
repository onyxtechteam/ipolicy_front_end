import React from "react";
import { useSelector } from "react-redux";
import ThumbUpAltIcon from "@material-ui/icons/ThumbUpAlt";
import { AdminDashCards } from "./policycards";
import axios from "axios";

const AdminDashboard = () => {
  const currentUser = useSelector(({ user }) => user.currentUser);
  const [loading, setLoading] = React.useState(false);
  const [counts, setCounts] = React.useState({
    totalPolicies: 0,
    TotalUsers: 0,
    totalInsuranceEntitites: 0,
  });
  const { token } = useSelector(({ user }) => user.currentUser);
  var CountsSearchs = async () => {
    setLoading(true);
    await axios
      .get(
        `/api/v1/countUsersAndPolicies`,

        { headers: { authorization: token } }
      )
      .then((response) => {
        setLoading(false);

        console.log(response.data);
        response.data.userData &&
          setCounts({ ...counts, ...response.data.userData });
      })
      .catch((err) => {
        setLoading(false);
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };

  React.useEffect(() => {
    CountsSearchs();
  }, []);
  return (
    // <div className="container dashboard-right-component">
    //   <div className="col-9">
    //     <h5>
    //       Hey, {currentUser.user.firstName}{" "}
    //       <span>
    //         <ThumbUpAltIcon style={{ color: "orange", fontSize: "24px" }} />
    //       </span>
    //     </h5>
    //   </div>
    //   <div style={{ height: "30px" }} />
    //   <div style={{ minHeight: "100px" }} className="row ">
    //     <div className="col-9">
    //       <p>Recent Policies</p>
    //     </div>
    //   </div>
    //   <div style={{ height: "70px" }} />
    //   <div className="row">
    //     <div className="col-9">
    //       <p>Recent Searchs</p>
    //     </div>
    //   </div>
    // </div>

    <div className="">
      {/* Start Content*/}
      <div className="container-fluid">
        <div className="row page-title">
          <div className="col-md-12">
            <nav aria-label="breadcrumb" className="float-right mt-1">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="/">IPolicyMart</a>
                </li>
                <li className="breadcrumb-item">
                  <a href="/users-dashboard">Admin - Dashboard</a>
                </li>
              </ol>
            </nav>
            <h3 className="mb-1 mt-0">
              Hey, {currentUser.user.firstName} (Admin)
              <span>
                <ThumbUpAltIcon style={{ color: "orange", fontSize: "24px" }} />
              </span>
            </h3>
          </div>
        </div>
        <div style={{ height: "30px" }} />

        <div style={{ height: "30px" }} />
        <div className="row">
          {/* end col*/}
          <AdminDashCards
            count={counts.TotalUsers}
            loading={loading}
            title="Registered Users"
          />
          <AdminDashCards
            count={counts.totalPolicies}
            title="Policy Purchased"
            loading={loading}
          />
          <AdminDashCards
            count={counts.totalInsuranceEntitites}
            title="insurers"
            loading={loading}
          />
          {/* end col*/}
        </div>
        {/* end row */}
        <div style={{ height: "30px" }} />

        {/* end row */}

        {/* end row */}
      </div>{" "}
      {/* container-fluid */}
    </div>
  );
};

export default AdminDashboard;

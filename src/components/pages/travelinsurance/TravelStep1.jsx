import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETTRAVELINSURANCEPROCESSINFO } from "../../../redux/action";
const TravelStep1 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const [formresponse, setformresponse] = React.useState({
    country1: "",
    country2: "",
    country3: "",
    country4: "",
  });
  const [formresponse2, setformresponse2] = React.useState({
    startDate: "",
    endDate: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const TravelInsuranceReducer = useSelector(
    ({ TravelInsuranceReducer }) => TravelInsuranceReducer
  );
  // const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    // const { country1, country2, country3, country4 } = formresponse;
    console.log(formresponse);
    let selected = 0;
    Object.values(formresponse).map((xxx) => (!xxx ? selected++ : null));
    if (selected > 3) {
      return setErrorAlert({
        status: true,
        message: "You must select at least one destination country",
      });
    }
    if (!formresponse2.endDate || !formresponse2.startDate) {
      return setErrorAlert({
        status: true,
        message: "You must select start date and end date",
      });
    }
    dispatch(
      SETTRAVELINSURANCEPROCESSINFO({ ...formresponse, ...formresponse2 })
    );
    history.push("/travel-insurance-step2");
  };
  return (
    <div className="container">
      <div className="col-lg-11">
        <h5>Trip Details</h5>
        <div style={{ height: "20px" }} />
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <p className="midtext1">Cover Type:</p>

        <div style={{ height: "2px" }} />
        <div className="row carsModel">
          {/* begin healthd */}
          <div
            // onClick={() =>
            //   setformresponse({ ...formresponse, Purchase_for: "Worker" })
            // }
            className={`cardB ${
              TravelInsuranceReducer.insurance_type === "Single Trip Plan"
                ? "focused"
                : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Single Trip
            </button>
          </div>

          <div
            // onClick={() =>
            //   setformresponse({ ...formresponse, Purchase_for: "Dependent" })
            // }
            className={`cardB ${
              TravelInsuranceReducer.insurance_type === "Multi Trip Plan"
                ? "focused"
                : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Multi Trip
            </button>
          </div>
          {/* end healthd */}
          {/* end healthd */}
        </div>

        <div style={{ height: "20px" }} />
        <p className="midtext1">
          Select The Countries That You Want To Travel To?
        </p>
        <div style={{ height: "11px" }} />
        <div className="col">
          <div className="row carsModel">
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country1: "USA" })
              }
              className={`cardB ${
                formresponse.country1 === "USA" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                USA
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country1: "UAE" })
              }
              className={`cardB ${
                formresponse.country1 === "UAE" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                UAE
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country1: "CANADA" })
              }
              className={`cardB ${
                formresponse.country1 === "CANADA" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                CANADA
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country1: "UK" })
              }
              className={`cardB ${
                formresponse.country1 === "UK" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                UK
              </button>
            </div>
            {/* end card */}
          </div>
        </div>
        <div className="col">
          <div className="row carsModel">
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country2: "Germany" })
              }
              className={`cardB ${
                formresponse.country2 === "Germany" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Germany
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country2: "Meldavis" })
              }
              className={`cardB ${
                formresponse.country2 === "Meldavis" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Meldavis
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country2: "France" })
              }
              className={`cardB ${
                formresponse.country2 === "France" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                France
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country2: "Italy" })
              }
              className={`cardB ${
                formresponse.country2 === "Italy" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Italy
              </button>
            </div>
            {/* end card */}
          </div>
        </div>

        <div className="col">
          <div className="row carsModel">
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country3: "Oman" })
              }
              className={`cardB ${
                formresponse.country3 === "Oman" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Oman
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country3: "Singapur" })
              }
              className={`cardB ${
                formresponse.country3 === "Singapur" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Singapur
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country3: "Switzerland" })
              }
              className={`cardB ${
                formresponse.country3 === "Switzerland" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Switzerland
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country3: "Jordan" })
              }
              className={`cardB ${
                formresponse.country3 === "Jordan" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Jordan
              </button>
            </div>
            {/* end card */}
          </div>
        </div>

        <div className="col">
          <div className="row carsModel">
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country4: "Russia" })
              }
              className={`cardB ${
                formresponse.country4 === "Russia" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Russia
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country4: "Turkey" })
              }
              className={`cardB ${
                formresponse.country4 === "Turkey" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Turkey
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country4: "Thailand" })
              }
              className={`cardB ${
                formresponse.country4 === "Thailand" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Thailand
              </button>
            </div>
            {/* end card */}
            {/* begin card */}
            <div
              onClick={() =>
                setformresponse({ ...formresponse, country4: "Poland" })
              }
              className={`cardB ${
                formresponse.country4 === "Poland" ? "focused" : null
              } col-lg-2 col-md-3 col-sm-3`}
            >
              <button style={{ width: "100%" }} className="btn3">
                Poland
              </button>
            </div>
            {/* end card */}
          </div>
        </div>
        <div style={{ height: "30px" }} />
        <p className="midtext1">Enter The Start Date And End Date:</p>
        <div className="row ">
          <div className="col-lg-2 col-md-3 col-sm-3">
            <input
              onChange={(e) =>
                setformresponse2({
                  ...formresponse2,
                  startDate: e.target.value,
                })
              }
              value={formresponse2.startDate}
              className="input-cars"
              type="date"
            />
          </div>
          <div className="col-lg-2 col-md-3 col-sm-3">
            <input
              onChange={(e) =>
                setformresponse2({ ...formresponse2, endDate: e.target.value })
              }
              value={formresponse2.endDate}
              className="input-cars"
              type="date"
            />
          </div>
        </div>
        <div style={{ height: "40px" }} />

        <div style={{ height: "40px" }} />
        <div className="row carsModel">
          {/* begin healthd */}

          {/* end healthd */}
          {/* end healthd */}
        </div>
        <div style={{ height: "10px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleBack} className="btn2 ">
            back
          </button>
          <button
            style={{ width: "150px" }}
            onClick={handleNext}
            className="btn1 healthNextbtn"
          >
            Next
          </button>
        </div>
        <div style={{ height: "40px" }} />
      </div>
    </div>
  );
};

export default TravelStep1;

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { CircularProgress } from "@material-ui/core";
import axios from "axios";
import { useSelector } from "react-redux";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",

    justifyContent: "center",
  },
  paper: {
    backgroundColor: "#fff",
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function TransitionsModal(props) {
  const classes = useStyles();
  const { open, handleClose, handlesetOpen } = props;
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const [loading, setLoading] = React.useState(false);
  const { token } = useSelector(({ user }) => user.currentUser);
  const [image, setImage] = React.useState();
  const [ImageState, setImageState] = React.useState({ file: null, Uri: null });
  const [formresponse, setFormResponse] = React.useState({
    name: "",
    tradeName: "",
    classification: "",
    registrationYear: "",
    logoImg2: "",
    yearEstablished: "",
    website: "",
    telephone: "",
    creditRating: "",
    developerPoral: "",
    carQuotePrice: "",
    bikeQuotePrice: "",
    thricycleQuotePrice: "",
    HomeQuotePrice: "",
    lifeQuotePrice: "",
    gadgetQuotePrice: "",
    travelQuotePrice: "",
    phoneQuotePrice: "",
    pcQuotePrice: "",
    laptopQuotePrice: "",
    CameratopQuotePrice: "",
    ipodQuotePrice: "",
    tabletQuotePrice: "",
    healthQuotePrice: "",
    registrationNumber: "",
  });

  const handleFormResponse = (e) => {
    setFormResponse({ ...formresponse, [e.target.name]: e.target.value });
  };

  const addInsurer = async () => {
    var formData = new FormData();
    console.log(formresponse.logoImg);

    formData.append("file", formresponse.logoImg2);
    formData.append("formResponse", JSON.stringify(formresponse));
    setLoading(true);
    await axios({
      method: "POST",
      url: `/api/v1/addInsurer`,
      data: formData,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: token,
      },
    })
      .then((response) => {
        setLoading(false);

        console.log(response.data);
        handleClose();
        response.data && handlesetOpen(true, response.data.message);
      })
      .catch((err) => {
        setLoading(false);
        alert("Fail");
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };
  const HandleSubmit = (e) => {
    e.preventDefault();
    if (!formresponse.name) {
      return setErrorAlert({
        status: true,
        message: "insurer name is required,",
      });
    }
    console.log(formresponse.logoImg2);
    if (!formresponse.logoImg2) {
      return setErrorAlert({
        status: true,
        message: "Image thumb nail is required,",
      });
    }
    addInsurer();
    // console.log(formresponse);
  };
  return (
    // <div>
    //   <button type="button" onClick={handleOpen}>
    //     react-transition-group
    //   </button>
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <form
          style={{ height: "80vh", marginTop: "80px", overflowY: "scroll" }}
          className={`${classes.paper} col-10`}
        >
          <h2 style={{ color: "dodgerblue" }}>Add Insurers</h2>
          <p id="transition-modal-description">
            Here You Can Add insurance comapanies to Data base
          </p>
          <div style={{ height: "40px" }} />
          <div className="row  justify-content-center">
            <div className="col-lg-6 ">
              {errorAlert.status ? (
                <Alert style={{ marginBottom: "10px" }} severity="error">
                  {errorAlert.message}
                </Alert>
              ) : null}
            </div>
          </div>

          <div className="row editInsurer justify-content-center">
            <div className="col-lg-6">
              <h6
                className="btn3"
                style={{ backgroundColor: "dodgerblue", color: "#fff" }}
              >
                Details:
              </h6>
              <div style={{ height: "40px" }} />
              <label>Insurer Name:</label>
              <input
                name="name"
                value={formresponse.name}
                onChange={handleFormResponse}
                placeholder="Insurer Name"
                className="input-cars"
              />
              <label>Trade Name:</label>
              <input
                name="tradeName"
                value={formresponse.tradeName}
                onChange={handleFormResponse}
                placeholder="Trade Name"
                className="input-cars"
              />
              <label> Year Established:</label>
              <select
                value={formresponse.yearEstablished}
                onChange={handleFormResponse}
                defaultValue={formresponse.yearEstablished}
                className="input-cars"
                id="yearEstablished"
                name="yearEstablished"
              >
                <option>year</option>
                <option value="2020">2020</option>
                <option value="2019">2019</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
                <option value="1999">1999</option>
                <option value="1998">1998</option>
                <option value="1997">1997</option>
                <option value="1996">1996</option>
                <option value="1995">1995</option>
                <option value="1994">1994</option>
                <option value="1993">1993</option>
                <option value="1992">1992</option>
                <option value="1991">1991</option>
                <option value="1990">1990</option>
                <option value="1989">1989</option>
                <option value="1988">1988</option>
                <option value="1987">1987</option>
                <option value="1986">1986</option>
                <option value="1985">1985</option>
                <option value="1984">1984</option>
                <option value="1983">1983</option>
                <option value="1982">1982</option>
                <option value="1981">1981</option>
                <option value="1980">1980</option>
                <option value="1979">1979</option>
                <option value="1978">1978</option>
                <option value="1977">1977</option>
                <option value="1976">1976</option>
                <option value="1975">1975</option>
                <option value="1974">1974</option>
                <option value="1973">1973</option>
                <option value="1972">1972</option>
                <option value="1971">1971</option>
                <option value="1970">1970</option>
                <option value="1969">1969</option>
                <option value="1968">1968</option>
                <option value="1967">1967</option>
                <option value="1966">1966</option>
                <option value="1965">1965</option>
                <option value="1964">1964</option>
                <option value="1963">1963</option>
                <option value="1962">1962</option>
                <option value="1961">1961</option>
                <option value="1960">1960</option>
                <option value="1959">1959</option>
                <option value="1958">1958</option>
                <option value="1957">1957</option>
                <option value="1956">1956</option>
                <option value="1955">1955</option>
                <option value="1954">1954</option>
                <option value="1953">1953</option>
                <option value="1952">1952</option>
                <option value="1951">1951</option>
                <option value="1950">1950</option>
                <option value="1949">1949</option>
                <option value="1948">1948</option>
                <option value="1947">1947</option>
                <option value="1946">1946</option>
                <option value="1945">1945</option>
                <option value="1944">1944</option>
                <option value="1943">1943</option>
                <option value="1942">1942</option>
                <option value="1941">1941</option>
                <option value="1940">1940</option>
              </select>
              <label>Website Url:</label>
              <input
                name="website"
                value={formresponse.website}
                onChange={handleFormResponse}
                placeholder=""
                className="input-cars"
              />
              <label>telephone:</label>
              <input
                name="telephone"
                value={formresponse.telephone}
                onChange={handleFormResponse}
                placeholder=" telephone number of insurere"
                className="input-cars"
              />{" "}
              <label>Classification:</label>
              <input
                name="classification"
                value={formresponse.classification}
                onChange={handleFormResponse}
                placeholder=""
                className="input-cars"
              />
            </div>
            <div className="col-lg-6">
              <label>Registration Number:</label>
              <input
                name="registrationNumber"
                value={formresponse.registrationNumber}
                onChange={handleFormResponse}
                placeholder=""
                className="input-cars"
              />
              <label>ThumbNail Image:</label>
              <input
                onChange={(e) =>
                  setFormResponse({
                    ...formresponse,
                    logoImg2: e.target.files[0],
                  })
                }
                accept="image/png, image/jpeg"
                style={{ fontSize: "10px" }}
                type="File"
                placeholder=""
                className="input-cars"
              />
              <div style={{ height: "40px" }} />
              <h6
                className="btn3"
                style={{ backgroundColor: "dodgerblue", color: "#fff" }}
              >
                Insurer Quotes:
              </h6>
              <div style={{ height: "40px" }} />
              <em style={{ color: "tomato" }}> Prices In Naira</em>
              <div className="row">
                <div className="col-4">
                  <label>Car Quote Price:</label>
                  <input
                    name="carQuotePrice"
                    value={formresponse.carQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
                <div className="col-4">
                  <label>Health Quote Price:</label>
                  <input
                    name="healthQuotePrice"
                    value={formresponse.healthQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
                <div className="col-4">
                  <label>Travel Quote Price:</label>
                  <input
                    name="travelQuotePrice"
                    value={formresponse.travelQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
                <div className="col-4">
                  <label>Home Quote Price:</label>
                  <input
                    name="HomeQuotePrice"
                    value={formresponse.HomeQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
                <div className="col-4">
                  <label>Bike Quote Price:</label>
                  <input
                    name="bikeQuotePrice"
                    value={formresponse.bikeQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
                <div className="col-4">
                  <label>Pc Quote Price:</label>
                  <input
                    name="pcQuotePrice"
                    value={formresponse.pcQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
                <div className="col-4">
                  <label>Gadget Quote Price:</label>
                  <input
                    name="gadgetQuotePrice"
                    value={formresponse.gadgetQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
                {/* <div className="col-4">
                  <label>Laptop Quote Price:</label>
                  <input
                    name="pcQuotePrice"
                    value={formresponse.pcQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div> */}
                {/* <div className="col-4">
                  <label>Ipod Quote Price:</label>
                  <input
                    name="ipodQuotePrice"
                    value={formresponse.ipodQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div> */}
                {/* <div className="col-4">
                  <label>Tablets Quote Price:</label>
                  <input
                    name="tabletQuotePrice"
                    value={formresponse.tabletQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div> */}
                {/* <div className="col-4">
                  <label>Phone Quote Price:</label>
                  <input
                    name="phoneQuotePrice"
                    value={formresponse.phoneQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div> */}
                {/* <div className="col-4">
                  <label>Digi. Cam. Quote Price:</label>
                  <input
                    name="CameratopQuotePrice"
                    value={formresponse.CameratopQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div> */}
                <div className="col-4">
                  <label>Tricyc. Quote Price:</label>
                  <input
                    name="thricycleQuotePrice"
                    value={formresponse.thricycleQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
                <div className="col-4">
                  <label>Life Quote Price:</label>
                  <input
                    name="lifeQuotePrice"
                    value={formresponse.lifeQuotePrice}
                    onChange={handleFormResponse}
                    type="number"
                    placeholder=""
                    className="input-cars"
                  />
                </div>
              </div>
            </div>
          </div>
          <div style={{ height: "40px" }} />
          <div className="text-center">
            <button type="submit" onClick={HandleSubmit} className="btn1">
              {loading ? (
                <CircularProgress
                  size={20}
                  style={{ color: "#fff", marginRight: "10px" }}
                />
              ) : (
                "Submit"
              )}
            </button>
          </div>
          <div style={{ height: "40px" }} />
        </form>
      </Fade>
    </Modal>
  );
}

import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch ,useSelector} from "react-redux";
import { SETHEALTHSURANCEPROCESSINFO } from "../../../redux/action";
const Healthstep1 = () => {
  const history = useHistory();

  const item = history.location.state;
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const [formresponse, setformresponse] = React.useState({
    members_nos: "",
    Purchase_for: "",
    coverage_details:""
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const InsuranceTypeReducer = useSelector(
    ({ InsuranceTypeReducer }) => InsuranceTypeReducer.insurance_type
  );

  const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    const { Purchase_for, members_nos } = formresponse;
    console.log(formresponse);
    if (!Purchase_for || !members_nos) {
      return setErrorAlert({
        status: true,
        message: "You did not select all fields",
      });
    }
    dispatch(SETHEALTHSURANCEPROCESSINFO(formresponse));
    history.push("/health-insurance-step2");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>
          Insurance Details{" "}
          <span style={{ color: "dodgerblue", textTransform: "capitalize" }}>
            {" "}
            - {item}
          </span>
        </h5>
        <div style={{ height: "20px" }} />
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <p className="midtext1">Who Are You Buying An insurance for ?</p>

        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          {/* begin healthd */}
          <div
            onClick={() =>
              setformresponse({ ...formresponse, Purchase_for: "Worker" })
            }
            className={`cardB ${
              formresponse.Purchase_for === "Worker" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Worker
            </button>
          </div>

          <div
            onClick={() =>
              setformresponse({ ...formresponse, Purchase_for: "Dependent" })
            }
            className={`cardB ${
              formresponse.Purchase_for === "Dependent" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Dependent
            </button>
          </div>
          {/* end healthd */}
          {/* end healthd */}
        </div>

        <div style={{ height: "20px" }} />
        <p className="midtext1">
          How Many Members Do You Want To Apply Insurance For ?
        </p>
        <div style={{ height: "10px" }} />
       
        <div className="col-10">
          <select
            // defaultValue={formresponse.members_nos}
            className="input-cars"
            id="countries-list"
            value={formresponse.members_nos}
            onChange={(e) => {
              e.target.value &&
                setformresponse({
                  ...formresponse,
                  members_nos: parseFloat(e.target.value),
                });
            }}
            style={
              {
                // width: "80%",
                // border: "none",
                // borderRadius: "4px",
                // height: "40px",
              }
            }
          >
            <option value="">Select Person(s)</option>
            <option value={1}>I Person</option>
            <option value={2}>2 Persons</option>
            <option value={3}>3 Persons</option>
            <option value={4}>4 Persons</option>
            <option value={5}>5 Persons</option>
          </select>
        </div>
        <div style={{ height: "40px" }} />
     { InsuranceTypeReducer !== "business-insurance" ? <>
      <p className="midtext1">
      Coverage Details Type ?
        </p>
        <div className="row carsModel">
          {/* begin healthd */}
          <div
            onClick={() =>
              setformresponse({ ...formresponse, coverage_details: "Personal" })
            }
            className={`cardB ${
              formresponse.coverage_details === "Personal" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Personal
            </button>
          </div>

          <div
            onClick={() =>
              setformresponse({ ...formresponse, coverage_details: "Business" })
            }
            className={`cardB ${
              formresponse.coverage_details === "Business" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Business
            </button>
          </div>
          {/* end healthd */}
          {/* end healthd */}
        </div>
       </>:null}
        <div className="row carsModel">
          {/* begin healthd */}

          {/* end healthd */}
          {/* end healthd */}
        </div>
        <div style={{ height: "10px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleBack} className="btn2 ">
            back
          </button>
          <button
            style={{ width: "150px" }}
            onClick={handleNext}
            className="btn1 healthNextbtn"
          >
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Healthstep1;

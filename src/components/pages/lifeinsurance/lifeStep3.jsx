import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETLIFEPROCESS } from "../../../redux/action";
const Lifestep3 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const [formresponse, setformresponse] = React.useState({
    previousClaims: "",
    Provide_No_Claim: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  // const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    const { Provide_No_Claim, previousClaims } = formresponse;
    console.log(formresponse);
    if (!Provide_No_Claim || !previousClaims) {
      return setErrorAlert({
        status: true,
        message: "You did not select all fields",
      });
    }
    dispatch(SETLIFEPROCESS(formresponse));
    history.push("/life-insurance-step4");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Claim Details</h5>
        <div style={{ height: "20px" }} />
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <p className="midtext1">
          Have You Claimed With Your Current Insurance Provider In The Last 12
          Months ?
        </p>

        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          {/* begin card */}
          <div
            onClick={() =>
              setformresponse({ ...formresponse, previousClaims: "yes" })
            }
            className={`cardB ${
              formresponse.previousClaims === "yes" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Yes
            </button>
          </div>

          <div
            onClick={() =>
              setformresponse({ ...formresponse, previousClaims: "no" })
            }
            className={`cardB ${
              formresponse.previousClaims === "no" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              No
            </button>
          </div>
          {/* end card */}
          {/* end card */}
        </div>

        <div style={{ height: "20px" }} />
        <p className="midtext1">
          Can You Provide A No Claim Certificate From Your Current Insurance
          Provider ?
        </p>
        <div style={{ height: "10px" }} />
        <div className="row carsModel">
          <div
            onClick={() =>
              setformresponse({ ...formresponse, Provide_No_Claim: "No" })
            }
            className={`cardB ${
              formresponse.Provide_No_Claim === "No" ? "focused" : null
            } col-lg-4 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              No , I Cannot
            </button>
          </div>

          <div
            onClick={() =>
              setformresponse({
                ...formresponse,
                Provide_No_Claim: "Yes, 1 year",
              })
            }
            className={`cardB ${
              formresponse.Provide_No_Claim === "Yes, 1 year" ? "focused" : null
            } col-lg-4 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Yes, 1 year
            </button>
          </div>
          <div
            onClick={() =>
              setformresponse({
                ...formresponse,
                Provide_No_Claim: "Yes, 2 year",
              })
            }
            className={`cardB ${
              formresponse.Provide_No_Claim === "Yes, 2 year" ? "focused" : null
            } col-lg-4 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Yes, 2 year
            </button>
          </div>
          <div
            onClick={() =>
              setformresponse({
                ...formresponse,
                Provide_No_Claim: "Yes, 3 year",
              })
            }
            className={`cardB ${
              formresponse.Provide_No_Claim === "Yes, 3 year" ? "focused" : null
            } col-lg-4 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Yes, 3 year
            </button>
          </div>
        </div>

        <div style={{ height: "10px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleBack} className="btn2 ">
            back
          </button>
          <button onClick={handleNext} className="btn1 carNextbtn">
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Lifestep3;

import React from "react";
import HeaderTwo from "../headertwo";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";

const SuccessPurchase = () => {
  return (
    <div className="">
      <HeaderTwo />
      <main className="successPurchase">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-10 text-center">
              <CheckCircleOutlineIcon
                style={{ color: "#51AF33", fontSize: "100px" }}
                fontSize="large"
              />
              <div style={{ height: "20px" }} />
              <h5>
                Congratulations.
                <br /> You have successfully purchased a policy.
              </h5>
              <div style={{ height: "30px" }} />
              <small>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
                et arcu nibh.
                <br /> Vestibulum mi dolor, maximus vitae ex a, consequat.
              </small>
              <div style={{ height: "40px" }} />
              <button className="btn1">Go to Dashboard</button>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default SuccessPurchase;

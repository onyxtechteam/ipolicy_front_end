import React from "react";
import { useHistory, useLocation } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETCARPROCESSINFO } from "../../../redux/action";
import axios from "axios";
const Carstep2 = () => {
  const history = useHistory();
  const location = useLocation();
  const { state } = location;
  const dispatch = useDispatch();
  const [carBrandName, setcarBrandNamer] = React.useState("");
  const [searchResults, setsearchResult] = React.useState([]);
  const [carmodels, setCarmodels] = React.useState([]);
  const [focused, setFocused] = React.useState(null);
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    if (!focused) {
      return setErrorAlert({
        status: true,
        message: "You have not made a selection",
      });
    }
    dispatch(SETCARPROCESSINFO({ carModel: focused }));
    history.push("/car-insurance-step4");
  };
  const FetchCrasBrands = async () => {
    await axios
      .get(`/api/v1/FindCarModels?CarBrands=${state}`)
      .then((response) => {
        console.log(response.data);
        response.data.userData &&
          response.data.userData.models.length > 0 &&
          setCarmodels(response.data.userData.models);
      })
      .catch((err) => {
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };
  const MapCarModels = () => {
    return carmodels.slice(0,15).map((xxx, index) => (
      <div
        key={index}
        onClick={() => setFocused(xxx.name)}
        className={`cardB ${
          focused === xxx.name ? "focused" : null
        } col-lg-3 col-md-3 col-sm-3`}
      >
        <button style={{ width: "100%" }} className="btn3">
          {xxx.name}
        </button>
      </div>
    ));
  };
  const handleSearch = (value) => {
    setcarBrandNamer(value);
    !value && setsearchResult([]);
    if (!value) {
      return null;
    }

    // value && alert(value);
  };
  React.useEffect(() => {
    state && FetchCrasBrands();
  }, []);
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>
          Car Details - <span style={{ color: "dodgerblue" ,textTransform:"capitalize"}}>{state}</span>
        </h5>

        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Car Model:</p>
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <input value={carBrandName} onChange={(e)=>handleSearch(e.target.value)} className="input-cars" placeholder="Model" />
        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          {/* begin card */}
          {MapCarModels()}
          <div
            onClick={() => setFocused("others")}
            className={`cardB ${
              focused === "others" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Others
            </button>
          </div>
          {/* end card */}
          {/* end card */}
        </div>
        <div style={{ height: "40px" }} />
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Carstep2;

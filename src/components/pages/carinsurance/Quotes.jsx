import React from "react";
// import PlayArrowIcon from "@material-ui/icons/PlayArrow";
// import CheckCircleIcon from "@material-ui/icons/CheckCircle";
// import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
// import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
// import HeadsetMicIcon from "@material-ui/icons/HeadsetMic";
// import DescriptionIcon from "@material-ui/icons/Description";
// import BeenhereIcon from "@material-ui/icons/Beenhere";
// import ListAltIcon from "@material-ui/icons/ListAlt";
// import AllInboxIcon from "@material-ui/icons/AllInbox";
import HeaderTwo from "../../headertwo";
import { useHistory, useLocation } from "react-router-dom";
import axios from "axios";
import LoadingQuotes from "../../LoadingQuotes";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const QuotesCard = (props) => {
  const [menuexpandMore, setMenuexpandMore] = React.useState(false);
  const [activFeatures, setActiveFeatures] = React.useState("");
  const handlemenuexpandMore = () => setMenuexpandMore(!menuexpandMore);
  const { info } = props;
  function makeid(length) {
    var result = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  return (
    <div className="quotesCard col-10">
      <div className="card" style={{ height: "100%", border: "none" }}>
        <div className="card-body verticalCenterRow justify-content-between">
          <img width="100px" src={info.logoImg} alt="" />
          <ul className="quotesListings">
            <li>{info.name}</li>
            <li>
              Installment available as low as
              <br /> <span>NGN {info.carQuotePrice} </span>
            </li>
            <li>
              Total <br />
              <span>NGN {info.carQuotePrice} </span>
            </li>
          </ul>
          <Link
            to={{
              pathname: "/car-insurance-checkout",
              state: { insuranceEntity: info, quoteRamdom: makeid(6) },
            }}
          >
            <button className="btn4">Buy now</button>
          </Link>
        </div>
        <div className={`expandMore row ${menuexpandMore ? "active" : null}`}>
          <ul className="expandMoreListing">
            <li onClick={() => setActiveFeatures("include Covers")}>
              include Covers <ArrowRightIcon />
            </li>
            <li onClick={() => setActiveFeatures("Exclude Covers")}>
              Exclude Covers <ArrowRightIcon />
            </li>
            <li onClick={() => setActiveFeatures("Features")}>
              Features <ArrowRightIcon />
            </li>
            <li onClick={() => setActiveFeatures("Optinal")}>
              Optinal <ArrowRightIcon />
            </li>
            <li onClick={() => setActiveFeatures("Reviews")}>
              Reviews <ArrowRightIcon />
            </li>
          </ul>
          <div className="expandMoreContent">{activFeatures}</div>
        </div>
        <div className="morebtn">
          {" "}
          <button
            onClick={handlemenuexpandMore}
            style={{ fontSize: "13px" }}
            className="btn2 "
          >
            More Information{" "}
            {!menuexpandMore ? <ExpandMoreIcon /> : <KeyboardArrowUpIcon />}
          </button>
        </div>
      </div>
    </div>
  );
};
const Quotes = (props) => {
  const { location } = useHistory();
  const history = useHistory();

  const [insuranceEntity, setInsuranceEntity] = React.useState([]);
  const [fetching, setFetching] = React.useState(false);
  const CarProcessReducer = useSelector(
    ({ CarProcessReducer }) => CarProcessReducer
  );

  const ValidateProcessData = () => {
    const { email, firstName, lastName } = CarProcessReducer;
    if (!email || !firstName || !lastName) {
      history.push("/");
      return false;
    } else return true;
  };
  var FetchInsuranceEntity = async () => {
    setFetching(true);
    await axios
      .get(`/api/v1/fetchinsuranceEntity?category=car`)
      .then((response) => {
        setTimeout(() => {
          setFetching(false);
        }, 4000);
        console.log(response.data);
        response.data.userData &&
          response.data.userData.length > 0 &&
          setInsuranceEntity(response.data.userData);
      })
      .catch((err) => {
        setFetching(false);
        if (err.message) {
          console.log(err.message);
        }
        console.log(err);
      });
  };

  const MapInsuranceEntity = () => {
    return insuranceEntity.map((xxx, index) => (
      // <InsuranceCoysCard info={xxx} key={index} />
      <QuotesCard info={xxx} img_src={xxx.logoImg} />
    ));
  };
  React.useEffect(() => {
    ValidateProcessData() && FetchInsuranceEntity();
    console.log(CarProcessReducer);
  }, []);

  return (
    <div>
      <HeaderTwo />
      {fetching ? (
        <LoadingQuotes />
      ) : (
        <main>
          <div className="Qoutes-header-Count-first verticalCenterRow justify-content-between">
            <h4>Car insurance</h4>
            <a href="/">
              {" "}
              <button className="btn2">Back to home</button>
            </a>
          </div>
          <div className="Qoutes-header-Count">
            {insuranceEntity.length > 0 ? (
              <p>
                {insuranceEntity.length} Quotes for your{" "}
                {CarProcessReducer.carBrand} {CarProcessReducer.carModel}
              </p>
            ) : (
              <p>No results</p>
            )}
          </div>
          <section className="Car-section row">
            <div className="left-content quotesList">
              <ul>
                <li>
                  <h4>Filter</h4>
                </li>
                <li>
                  <p>Select Date</p>
                </li>
                <li>
                  <input className="input-cars" type="date" />
                </li>
                <li>
                  <p>Price Range</p>
                </li>
                <li>
                  <button className="btn1">Apply Filter</button>
                </li>
              </ul>
            </div>
            <div className="right-content QuotesSection">
              {/* qoutes here */}

              {/* <p>Quotes here</p> */}
              {/* <QuotesCard img_src="/images/p1.png" />
              <QuotesCard img_src="/images/p2.png" />
              <QuotesCard img_src="/images/p3.png" /> */}
              {insuranceEntity.length > 0 ? MapInsuranceEntity() : null}

              {/* qoutes here */}
            </div>
          </section>
        </main>
      )}
    </div>
  );
};

export default Quotes;

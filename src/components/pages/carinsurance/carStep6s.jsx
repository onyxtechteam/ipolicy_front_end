import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETCARPROCESSINFO } from "../../../redux/action";
const Carstep6s = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const [formresponse, setformresponse] = React.useState({
 
    coverType: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    const {  coverType} = formresponse;
    console.log(formresponse);
    if ( !coverType) {
      return setErrorAlert({
        status: true,
        message: "You did not select all fields",
      });
    }
    dispatch(SETCARPROCESSINFO(formresponse));
    history.push("/car-insurance-step7");
  };
  return (
    <div className="container">
      <div className="col-lg-11">
        <h5>Insurance Details</h5>
        <div style={{ height: "20px" }} />
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
       

      

    
       
        <p className="midtext1">Select One</p>
        <div className="row carsModel">
          {/* begin card */}
          <div
            onClick={() =>
              setformresponse({
                ...formresponse,
                coverType: "Third Party",
              })
            }
            className={`cardB ${
              formresponse.coverType === "Third Party" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Third Party
            </button>
          </div>

          <div
            onClick={() =>
              setformresponse({
                ...formresponse,
                coverType: "Third part & Fire Theft",
              })
            }
            className={`cardB ${
              formresponse.coverType === "Third part & Fire Theft" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Third party Fire & Theft
            </button>
          </div>
          <div
            onClick={() =>
              setformresponse({ ...formresponse, coverType: "Comprehensive" })
            }
            className={`cardB ${
              formresponse.coverType === "Comprehensive" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Comprehensive
            </button>
          </div>
          {/* end card */}
          {/* end card */}
        </div>
        <div style={{ height: "10px" }} />
        <div className="verticalCenterRow justify-content-around">
          <button onClick={handleBack} className="btn2 ">
            back
          </button>
          <button onClick={handleNext} className="btn1 carNextbtn">
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Carstep6s;

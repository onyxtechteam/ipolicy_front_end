import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETCARPROCESSINFO } from "../../../redux/action";
import axios from "axios";
import styled from "styled-components";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";

const SearchResultContainer = styled.div`
  min-height: 40px;
  background-color: rgb(255, 255, 255, 0.5);
  position: absolute;
  width: 100%;
  z-index: 2;
  padding: 10px;
  box-sizing: border-box;
`;
const CarBrandContainer = styled.div`
  background-color: #fff;
  min-height: 18px;
  margin: 5px;
  padding: 4px;
  cursor: pointer;
  justify-content: space-between;
  box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px;
`;
const CARMODELSCARDS = (props) => {
  // "/images/mercedes.png"
  const history = useHistory();
  const dispatch = useDispatch();
  const handleNext = (data) => {
    dispatch(SETCARPROCESSINFO({ carBrand: data }));
    history.push({ pathname: "/car-insurance-step2", state: data });
  };
  return (
    <CarBrandContainer
      onClick={handleNext.bind(this, props.name)}
      className="verticalCenterRow col-md-5"
    >
      <p style={{ textTransform: "uppercase" }}>{props.name}</p>
      <ArrowForwardIcon fontSize="medium" />
    </CarBrandContainer>
  );
};

const Carstep1 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [carBrandName, setcarBrandNamer] = useState("");
  const [searchResults, setsearchResult] = useState([]);
  const [focused, setFocused] = React.useState(null);
  const handlefocused = (value) => setFocused(value);
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handleNext = (data) => {
    if (!focused) {
      return setErrorAlert({
        status: true,
        message: "No selection made",
      });
    }
    dispatch(SETCARPROCESSINFO({ carBrand: focused }));
    history.push({ pathname: "/car-insurance-step2", state: focused });
  };

  // const FetchCrasBrands = async () => {
  //   await axios
  //     .get("/api/v1/listAllCars")
  //     .then((response) => {
  //       // console.log(response.data);
  //     })
  //     .catch((err) => {
  //       if (err.message) {
  //         // console.log(err.message);
  //       }
  //       // console.log(err);
  //     });
  // };
  const MaphResults = () => {
    return searchResults.map((xxx) => (
      <CARMODELSCARDS name={xxx.name} key={xxx._id} />
    ));
  };
  const FindCarModelsBySearch = async (query) => {
    await axios
      .get(`/api/v1/FindCarModelsBySearch?search=${query}`)
      .then((response) => {
        console.log(response.data);
        response.data &&
          response.data.userData.length > 0 &&
          setsearchResult(response.data.userData);
      })
      .catch((err) => {
        if (err.message) {
          // console.log(err.message);
        }
        // console.log(err);
      });
  };
  const handleBack = () => history.goBack();
  const handleSearch = (value) => {
    setcarBrandNamer(value);
    !value && setsearchResult([]);
    if (!value) {
      return null;
    }

    value && FindCarModelsBySearch(value);
  };
  // useEffect(() => {
  //   FetchCrasBrands();
  // }, []);
  return (
    <div className="container">
      <div className="col-lg-9 col-sm-12">
        <h5>Car Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Car Brand:</p>
        <div className="col-lg-4">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>
        <div style={{ position: "relative" }}>
          <input
            value={carBrandName}
            onChange={(e) => handleSearch(e.target.value)}
            className="input-cars"
            placeholder="Search cars"
          />
          {searchResults.length > 0 ? (
            <SearchResultContainer>{MaphResults()}</SearchResultContainer>
          ) : null}
        </div>
        <div style={{ height: "20px" }} />
        <div className="row carsList">
          {/* begin card */}
          <div className=" car-card wrapper  col-lg-3 ">
            <div
              onClick={handlefocused.bind(this, "Mercedes-Benz")}
              className={`card ${focused === "Mercedes-Benz" ? "focused" : null} `}
            >
              <div className="card-body">
                <img src="/images/carslogo/mercedes.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className=" car-card  col-lg-3 wrapper ">
            <div
              className={`card ${focused === "hyundai" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "hyundai")}
            >
              <div className="card-body">
                <img src="/images/carslogo/car2.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className=" car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "honda" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "honda")}
            >
              <div className="card-body">
                <img src="/images/carslogo/car3.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className=" car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "chevrolet" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "chevrolet")}
            >
              <div className="card-body">
                <img src="/images/carslogo/car4.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className="car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "Land Rover" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "Land Rover")}
            >
              <div className="card-body">
                <img src="/images/carslogo/landrover.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className="car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "ford" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "ford")}
            >
              <div className="card-body">
                {/* <img src="/images/carslogo/car6.png" alt="" /> */}
                <img src="/images/carslogo/car13.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className="car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "kia" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "kia")}
            >
              <div className="card-body">
                <img src="/images/carslogo/kia.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className="car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "lexus" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "lexus")}
            >
              <div className="card-body">
                <img src="/images/carslogo/lexus.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className="car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "nissan" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "nissan")}
            >
              <div className="card-body">
                <img src="/images/carslogo/nissan.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className="car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "mazda" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "mazda")}
            >
              <div className="card-body">
                <img src="/images/carslogo/mazda.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className="car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "toyota" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "toyota")}
            >
              <div className="card-body">
                <img src="/images/carslogo/car11.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
          {/* begin card */}
          <div className="car-card wrapper col-lg-3">
            <div
              className={`card ${focused === "volkswagen" ? "focused" : null} `}
              onClick={handlefocused.bind(this, "volkswagen")}
            >
              <div className="card-body">
                <img src="/images/carslogo/car12.png" alt="" />
              </div>
            </div>
          </div>
          {/* end card */}
        </div>{" "}
        <div style={{ height: "40px" }} />
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
        <div style={{ height: "40px" }} />
      </div>
    </div>
  );
};

export default Carstep1;

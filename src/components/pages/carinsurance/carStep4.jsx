import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETCARPROCESSINFO } from "../../../redux/action";
const carRegistrationYear = [
  { name: "brand new car" },
  { name: "2020" },
  { name: "2019" },
  { name: "2018" },
  { name: "2017" },
  { name: "2016" },
  { name: "2015" },
  { name: "2014" },
  { name: "2013" },
  { name: "2012" },
  { name: "2011" },
  { name: "2010" },
  { name: "2009" },
  { name: "2008" },
  { name: "2007" },
  { name: "2006" },
  { name: "2005" },
  { name: "2004" },
  { name: "2003" },
  { name: "2002" },
  { name: "2001" },
  { name: "2000" },
  { name: "1999" },
  { name: "1998" },
  { name: "1997" },
  { name: "1996" },
  { name: "1995" },
];
const Carstep4 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const handleBack = () => history.goBack();
  const [formresponse, setFormresponse] = React.useState({
    carRegisterYear: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });

  const handleNext = () => {
    if (!formresponse.carRegisterYear) {
      return setErrorAlert({
        status: true,
        message: "You have not made a selection",
      });
    }
    dispatch(SETCARPROCESSINFO(formresponse));
    history.push("/car-insurance-step6");
  };
  const mapCarYears = () => {
    return carRegistrationYear.map((xxx) => (
      <div
        onClick={() => setFormresponse({ carRegisterYear: xxx.name })}
        className={`cardB ${
          formresponse.carRegisterYear === xxx.name ? "focused" : null
        } col-lg-3 col-md-3 col-sm-3`}
      >
        <button style={{ width: "100%" }} className="btn3">
          {xxx.name}
        </button>
      </div>
    ));
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Car Details</h5>
        <div style={{ height: "20px" }} />
        <p className="midtext1">Select The Year:</p>
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>

        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          {/* begin card */}

          {mapCarYears()}
          {/* end card */}
          {/* end card */}
        </div>

        <div style={{ height: "20px" }} />
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Carstep4;

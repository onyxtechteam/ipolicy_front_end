import React from "react";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { SETCARPROCESSINFO } from "../../../redux/action";
const Carstep3 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [focused, setFocused] = React.useState(null);
  const [formresponse, setformresponse] = React.useState({
    carFuelType: "",

    carVarient: "",
  });
  const [errorAlert, setErrorAlert] = React.useState({
    status: false,
    message: "",
  });
  const handlefocused = (value) => setFocused(value);
  const handleBack = () => history.goBack();
  const handleNext = () => {
    // console.log(formresponse);
    // if (!formresponse.carFuelType || !formresponse.carVarient) {
    //   return setErrorAlert({
    //     status: true,
    //     message: "You did not select all fields",
    //   });
    // }
    dispatch(SETCARPROCESSINFO(formresponse));
    history.push("/car-insurance-step4");
  };
  return (
    <div className="container">
      <div className="col-lg-9">
        <h5>Car Details</h5>
        <div style={{ height: "20px" }} />
        {/* <p className="midtext1">Select The Fuel Type:</p> */}
        <div className="col-lg-5">
          {errorAlert.status ? (
            <Alert style={{ marginBottom: "10px" }} severity="error">
              {errorAlert.message}
            </Alert>
          ) : null}
        </div>

        <div style={{ height: "20px" }} />
        <div className="row carsModel">
          {/* begin card */}
          {/* <div
            onClick={() =>
              setformresponse({ ...formresponse, carFuelType: "Petrol" })
            }
            className={`cardB ${
              formresponse.carFuelType === "Petrol" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Petrol
            </button>
          </div> */}

          {/* <div
            onClick={() =>
              setformresponse({ ...formresponse, carFuelType: "diesel" })
            }
            className={`cardB ${
              formresponse.carFuelType === "diesel" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Diesel
            </button>
          </div> */}
          {/* end card */}
          {/* end card */}
        </div>

        <div style={{ height: "20px" }} />
        {/* <p className="midtext1">Select The Variant Type:</p> */}
        <div style={{ height: "10px" }} />
        <div className="row carsModel">
          {/* begin card */}
          {/* <div
            onClick={() =>
              setformresponse({ ...formresponse, carVarient: "automatic" })
            }
            className={`cardB ${
              formresponse.carVarient === "automatic" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Automatic
            </button>
          </div> */}

          {/* <div
            onClick={() =>
              setformresponse({ ...formresponse, carVarient: "manual" })
            }
            className={`cardB ${
              formresponse.carVarient === "manual" ? "focused" : null
            } col-lg-3 col-md-3 col-sm-3`}
          >
            <button style={{ width: "100%" }} className="btn3">
              Manual
            </button>
          </div> */}
          {/* end card */}
          {/* end card */}
        </div>
        <button onClick={handleBack} className="btn2 ">
          back
        </button>
        <button onClick={handleNext} className="btn1 carNextbtn">
          Next
        </button>
      </div>
    </div>
  );
};

export default Carstep3;

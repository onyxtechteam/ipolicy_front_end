import React from "react";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
import HeadsetMicIcon from "@material-ui/icons/HeadsetMic";
import DescriptionIcon from "@material-ui/icons/Description";
import BeenhereIcon from "@material-ui/icons/Beenhere";
import ListAltIcon from "@material-ui/icons/ListAlt";
import AllInboxIcon from "@material-ui/icons/AllInbox";
import HeaderOne from "../headerone";
import { Link, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { CLEARLOCALSTORAGEPROCESS, SETINSURANCETYPE } from "../../redux/action";
import Footer from "./Footer";
const Homepage = () => {
  const dispatch = useDispatch({});
  const history = useHistory();
  React.useEffect(() => {
    dispatch(CLEARLOCALSTORAGEPROCESS());
  }, []);
  return (
    <div>
      <HeaderOne />
      <main id='mainbody' className="mainBody">
        <div className="Main-section">
          <div className="container">
            <div className="verticalCenterRow">
              <div className="col-lg-5 col-sm-8 Main-content">
                <h3>Compare, Buy, Save</h3>
                <h1 className="Main-content-add">
                  Smart way to buy
                  <br />
                  Insurance
                </h1>
                <p>
                  Compare insurance policies, rate, detail, and <br />
                  buy instantly with IPolicyMart's best <br />
                  insurance companies in less
                  <br /> than five minutes!
                  <br />
                </p>
                <button className="btn1">Compare now</button>
                <div className="watch-video-main">
                  <p className="title4">
                    {" "}
                    <PlayArrowIcon />
                    Watch the intro video Get
                  </p>
                </div>
              </div>
              <div className="col-lg-6 col-sm-4 main-image-div">
                <img className src="/images/mansmile.png" alt="" />
              </div>
            </div>
            <div className="Main-img" />
          </div>
        </div>
        <section className="partners">
          <div className="container">
            <div className="partnersRow verticalCenterRow">
              <div className="">
                <img src="/images/p6.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p1.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p2.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p3.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p4.png" alt="" />
              </div>
              <div className="col-lg-2 col-sm-1">
                <img src="/images/p5.png" alt="" />
              </div>
            </div>
          </div>
        </section>
        {/* end main-section */}
        {/* begin choose us section */}
        <div id="choose-us" className="choose-us">
          <div className="container">
            <div className="verticalCenterRow justify-content-center">
              <div className="choose-us-main col-lg-8 col-sm-9">
                <h2 className="title1">Choose your insurance</h2>
                <p className="sub-title">
                  Keep Your Life Simple, Safe and Wealthy
                </p>
                <div className="row  choose-us-options justify-content-center">
                  <div
                    onClick={() => {
                      history.push("/personal-insurance");
                      dispatch(SETINSURANCETYPE("personal-insurance"));
                    }}
                    className="col-lg-4 col-sm-2"
                  >
                    <div className="card ">
                      <div className="card-body  justify-content-between">
                        <span className="verticalCenterRow ">
                          <h5>
                            Personal
                            <br />
                            Insurance
                          </h5>
                          <img
                            width="50px"
                            style={{ marginLeft: "40px" }}
                            src="/images/hearticon.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Choose from Personal Insurances and help to cover the
                          costs of any unavoidable harm to your home, car or
                          your health.
                        </p>
                        <button className="btn1">Choose</button>
                      </div>
                    </div>
                  </div>
                  <div
                    onClick={() => {
                      history.push("/business-insurance");
                      dispatch(SETINSURANCETYPE("business-insurance"));
                    }}
                    className="col-lg-4 col-sm-2"
                  >
                    <div className="card">
                      <div className="card-body">
                        <span className="verticalCenterRow">
                          <h5>
                            Business
                            <br />
                            Insurance
                          </h5>
                          <img
                            width="45px"
                            style={{ marginLeft: "40px" }}
                            className
                            src="/images/bagicon.png"
                            alt=""
                          />
                        </span>
                        <p>
                          Protect yourself, your employees, your customers, your
                          assets and the future of your company.
                          <br />
                        </p>
                        <button className="btn1">Choose</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="about-us" className="about-us">
          <div className="container">
            <h6 className="title4">ABOUT US</h6>
            <div className="verticalCenterRow justify-content-between">
              <div style={{ paddingLeft: 0 }} className="col-lg-6 col-sm-7">
                <h2 className="title1">
                  iPolicyMart's Best Insurance
                  <br />
                  Comparison Websites
                </h2>
                <p>
                  The Myspace page defines the individual, his or her <br />
                  character traits, personal qualities, and the
                  <br /> person's overall personality this is why
                  <br /> iPolicyMart is so accurate!
                </p>
                <ul>
                  {/* <li>
                    <CheckCircleIcon className="about-us-icon" /> Never worry
                    about overpaying for your energy
                    <br />
                    again
                  </li> */}
                  <li>
                    <CheckCircleIcon className="about-us-icon" />
                    Never worry about paying too much for your insurance
                    <br /> again - we only connect you with the
                    <br /> successful companies we trust <br />
                    and that will treat you well.
                  </li>
                  <li>
                    <CheckCircleIcon className="about-us-icon" />
                    We check the market daily and know where the savings are.
                  </li>
                </ul>
                <div className="verticalCenterRow align-items-center read-more">
                  <button
                    style={{
                      paddingLeft: "30px",
                      paddingRight: "30px",
                      marginRight: "30px",
                    }}
                    className="btn1"
                  >
                    Read More...
                  </button>
                  <p className="WatchVideo title4">
                    {" "}
                    <PlayArrowIcon /> Watch The Video
                  </p>
                </div>
              </div>
              <div className="col-lg-5 col-sm-7 about-us-img">
                <img style={{ width: "120%" }} src="/images/woman.png" alt="" />
              </div>
              {/* <div class="col-lg-5 col-sm-4 about-us-img">
              <img class="" src="/images/woman.png" alt="" />
            </div> */}
            </div>
          </div>
        </div>
        <div className="Loves-us-section">
          <div className="container">
            <h2 className="title1">10,000+ people believed in us</h2>
            <p className="sub-title">
              These companies always implement their own version of the
              <br /> operating system with minimal changes
            </p>
            <div className="row testimonies ">
              {/* begin card */}
              <div className="col-lg-4 col-sm-7">
                <div className="card loves-us-card">
                  <div className="card-body">
                    <img
                      width="50px"
                      height="50px"
                      src="/images/man11.png"
                      alt=""
                    />
                    <p>
                      its not only about what your write but how you present
                      <br />
                      only those who risk going far can actually find out how
                      far one can go{" "}
                    </p>
                    <p className="light-text">
                      Arthurm shazonov
                      <br />
                      Project manager
                    </p>
                  </div>
                </div>
              </div>
              {/* end card */}
              {/* begin card */}
              <div className="col-lg-4 col-sm-7">
                <div className="card loves-us-card">
                  <div className="card-body">
                    <img
                      width="50px"
                      height="50px"
                      src="/images/man31.png"
                      alt=""
                    />
                    <p>
                      its not only about what your write but how you present
                      <br />
                      only those who risk going far can actually find out how
                      far one can go{" "}
                    </p>
                    <p className="light-text">
                      Arthurm shazonov
                      <br />
                      Project manager
                    </p>
                  </div>
                </div>
              </div>
              {/* end card */}
              {/* begin card */}
              <div className="col-lg-4 col-sm-7">
                <div className="card loves-us-card">
                  <div className="card-body">
                    <img
                      width="50px"
                      height="50px"
                      src="/images/man41.png"
                      alt=""
                    />
                    <p>
                      its not only about what your write but how you present
                      <br />
                      only those who risk going far can actually find out how
                      far one can go
                    </p>
                    <p className="light-text">
                      Arthurm shazonov
                      <br />
                      Project manager
                    </p>
                  </div>
                </div>
              </div>
              {/* end card */}
            </div>
          </div>
          <div className="verticalCenterRow justify-content-center">
            <div className="col-lg-7 readerCoins">
              <div
                className="verticalCenterRow justify-content-around"
                style={{ backgroundColor: "#fff" }}
              >
                <div className="col-lg-3 text-center">
                  <div className="card">
                    <div className="card-body">
                      <h3>3258+</h3>
                      <p>happy customers</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 text-center">
                  <div className="card">
                    <div className="card-body">
                      <h3>12,000</h3>
                      <p>Companions made</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 text-center">
                  <div className="card">
                    <div className="card-body">
                      <h3>$5500k</h3>
                      <p>Insurernce sold</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 text-center">
                  <div className="card">
                    <div className="card-body">
                      <h3>35</h3>
                      <p>Award Winner</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="why-choose-us">
          <div className="container">
            <h2 className="title1">
              Why you should choose
              <br />
              Insurance <b className="title4">Aggregator</b>
            </h2>
            <p className="sub-title2">
              Compare life insurance, health insurance, car insurance and travel
              <br />
              find the best plan for you at the best price.
            </p>
            <div className="row justify-content-center">
              <div className="col-lg-9 why-choose-us-container">
                <div className="row">
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body why-cards">
                        <div
                          style={{ backgroundColor: "rgb(234,227,243)" }}
                          className="coins"
                        >
                          <LibraryAddCheckIcon
                            style={{ color: "rgb(119,81,190)" }}
                          />
                        </div>
                        <h6>Free comparison</h6>
                        <p>
                          Compare insurance plans without obligation,
                          <br />
                          compare as much as you wan completely free!
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body why-cards">
                        <div
                          style={{ backgroundColor: "rgb(254,242,216)" }}
                          className="coins"
                        >
                          <AllInboxIcon style={{ color: "rgb(251,176,1) " }} />
                        </div>
                        <h6>Insurance Vault</h6>
                        <p>
                          We provide access to reliable insurance <br />
                          companies that will listen to your needs.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body why-cards">
                        <div
                          style={{ backgroundColor: "rgb(253,229,229)" }}
                          className="coins"
                        >
                          <DescriptionIcon
                            style={{ color: "rgb(237,62,59)" }}
                          />
                        </div>
                        <h6>Quotes from multiple insurers</h6>
                        <p>
                          Compare life insurance,health insurance,
                          <br />
                          motor insurance and travel insurance
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body why-cards">
                        <div
                          style={{ backgroundColor: "rgb(229,249,240)" }}
                          className="coins"
                        >
                          <BeenhereIcon style={{ color: "rgb(59,213,151)" }} />
                        </div>
                        <h6>Zero Hassle</h6>
                        <p>Choose insurance without any interferences</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body why-cards">
                        <div
                          style={{ backgroundColor: "rgb(230,232,253)" }}
                          className="coins"
                        >
                          <HeadsetMicIcon style={{ color: "rgb(65,82,248)" }} />
                        </div>
                        <h6>Claims Assistance</h6>
                        <p>Our company are always ready to help you.</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body why-cards">
                        <div
                          style={{ backgroundColor: "rgb(234,227,243)" }}
                          className="coins"
                        >
                          <AccountBalanceWalletIcon
                            style={{ color: "rgb(119,81,190)" }}
                          />
                        </div>
                        <h6>Pay Online</h6>

                        <p>No inconvenience! Pay online and save your time.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="how-it-works" id="how-it-works">
          <div className="container">
            <h2 className="title1">How does it work?</h2>
            <p className="sub-title2">
              Complete 3 simple steps to find the best insurance plan for your
              or your family!
            </p>
            <div className="verticalCenterRow justify-content-center">
              <div className="col-lg-10 how-it-works-container">
                <div className="row">
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body choose-cards">
                        <div
                          style={{ backgroundColor: "rgb(230,242,255)" }}
                          className="coins2"
                        >
                          <ListAltIcon
                            style={{
                              color: "rgb(29,143,255)",
                              fontSize: "30px",
                            }}
                          />
                        </div>
                        <h6>1-Enter details</h6>
                        <p>
                          Answer a few simple questions so the <br />
                          program could generate a custom quote for you!
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body choose-cards">
                        <div
                          style={{ backgroundColor: "rgb(230,242,255)" }}
                          className="coins2"
                        >
                          <LibraryAddCheckIcon
                            style={{
                              color: "rgb(29,143,255)",
                              fontSize: "30px",
                            }}
                          />
                        </div>

                        <h6>2-Compare plans</h6>
                        <p>
                          View the quotes, compare them, and choose <br />
                          the best deal.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body choose-cards">
                        <div
                          style={{ backgroundColor: "rgb(230,242,255)" }}
                          className="coins2"
                        >
                          <AccountBalanceWalletIcon
                            style={{
                              color: "rgb(29,143,255)",
                              fontSize: "30px",
                            }}
                          />
                        </div>
                        <h6>3-Buy Online</h6>
                        <p>
                          Get instant insurance from the company of your <br />
                          choice through us.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="get-app">
          <div className="container">
            <div className="verticalCenterRow justify-content-between get-app-container">
              <div style={{ padding: "20px" }} className="col-lg-6 col-sm-7">
                <h4 className="title1">
                  Get iPolicyMart's app.
                  <br />
                </h4>
                <p>
                  Stay in control of all your insurance needs anywhere, anytime
                </p>
                <ul>
                  <li>
                    Compare different insurance policies
                    <br />
                  </li>
                  <li>
                    Buy, store and share all your policies online
                    <br />
                  </li>
                  <li>
                    Track the status of your policy on the go
                    <br />
                  </li>
                  <li>
                    Download your policy with a single tap <br />
                  </li>
                </ul>
                <div className="verticalCenterRow align-items-center">
                  <button
                    style={{
                      paddingLeft: "30px",
                      paddingRight: "30px",
                      marginRight: "10px",
                      backgroundColor: "#fff",
                      borderRadius: "10px",
                      color: "black",
                    }}
                    className="btn1"
                  >
                    <img
                      style={{ marginRight: "7px" }}
                      width="20px"
                      src="/images/playstore.png"
                      alt=""
                    />
                    Play store
                  </button>
                  <button
                    style={{
                      paddingLeft: "30px",
                      paddingRight: "30px",
                      marginRight: "30px",
                      border: "1px solid #fff",
                      borderRadius: "10px",
                    }}
                    className="btn1"
                  >
                    <img width="20px" src="/images/appstore.png" alt="" />
                    App store
                  </button>
                </div>
              </div>
              <div
                style={{ alignSelf: "flex-end" }}
                className="col-lg-5 col-sm-7 about-us-img"
              >
                <img width="110%" src="/images/mobiles.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </main>
   <Footer/>
    </div>
  );
};

export default Homepage;

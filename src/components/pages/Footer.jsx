import React from 'react';

const Footer = () => {
    return (    <footer>
        <div className="footer-section">
          <div className="container">
            <div className="footer-items">
              <div className="col-lg-4">
                <h2 className="title4">iPolicyMart</h2>
                <div>
                  <p>
                    If you have any questions please reach out! We will be more
                    than happy to assist you.
                  </p>
                </div>
                <div className="subscribe-now verticalCenterRow">
                  <input placeholder="Subscribe To Our Newsletter" />
                  <button>Send</button>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="verticalCenterRow">
                  <ul>
                    <li>Quicklinks</li>
                    <li>
                      <a href='/#mainbody'>Home</a></li>
                    <li>Accounts</li>
                    <li>
                      <a href="/#how-it-works">Features </a>
                    </li>
                    <li>Contact</li>
                    <li>
                      <a href="/insurance-claims"> Claims</a>
                    </li>
                  </ul>
                  <ul>
                    <li>Company</li>
                    <li><a href='/#about-us'>About us</a></li>
                    <li>Career</li>
                    <li>Support</li>
                    <li>Affiliate</li>
                    <li>Program</li>
                  </ul>
                  <ul>
                    <li>Contacts</li>
                    <li>2348000000000</li>
                    <li>help@ipolicymart.com</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="footer-divider" />
            <p className="foot-text">
              {new Date().getFullYear()}, CopyWrite| All Right Reserved
            </p>
          </div>
        </div>
      </footer>
    );
}
 
export default Footer;
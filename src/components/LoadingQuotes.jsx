import React from "react";

const LoadingQuotes = () => {
  return (
    <div className="LoadingQuotes">
      <div col-4>
        <img width="80px" src="/images/spinner.gif" alt="" />
        <h4>Preparing Your Quotes</h4>
      </div>
    </div>
  );
};

export default LoadingQuotes;

import React from "react";
import { Link } from "react-router-dom";
import ClearIcon from "@material-ui/icons/Clear";
import MenuIcon from "@material-ui/icons/Menu";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
const SideNav = styled.div`
  z-index: 3;
  width: 300px;
  height: 100vh;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: 20px;
  padding-top: 3px;
  position: absolute;
  background-color: white;
  right: 0;
  top: 0;
  animation: apper 1.2s ease;
  animation-iteration-count: 1;

  @keyframes apper {
    from {
      opacity: 0;
    }
    to {
      top: 1;
    }
  }
`;
const Listing = styled.ul`
  display: flex;
  margin: 0;
  list-style-type: none;
  padding: 0;

  flex-direction: column;
  align-items: flex-start;
  width: 98%;
  li {
    display: flex;
    font-size: 16px;
    border-bottom: 0.1px solid silver;
    width: 100%;
    color: black;
    padding: 10px;
    // &:nth-child(1) {
    //   font-size: 20px;
    //   color: grey;
    // }
    // &:nth-child(2) {
    //   font-size: 30px;
    //   color: black;
    //   font-weight: 600;
    //   @media (max-width: 1100px) {
    //     font-size: 30px;
    //   }
    // }
    // &:nth-child(3) {
    //   font-size: 20px;
    //   color: grey;
    //   font-weight: 500;
    //   @media (max-width: 1100px) {
    //     font-size: 20px;
    //   }
    // }
  }
`;
const MenuIconDiv = styled.div`
  // margin-right: 20px;
  cursor: pointer;
  @media (min-width: 930px) {
    display: none;
  }
`;
const HideMobile = styled.span`
  @media (max-width: 930px) {
    display: none;
  }
`;
const Filter = styled.div`
  width: 100%;
  height: 100vh;
  background-color: grey;
  position: absolute;
  z-index: 1;
  opacity: 0.6;
  top: 0;
  left: 0;
`;
const HeaderTwo = () => {
  const history = useHistory();
  const [sidebarVisible, setSidebarVisible] = React.useState(false);
  const currentUser = useSelector(({ user }) => user.currentUser);
  const handleOpenSideBar = () => {
    setSidebarVisible(!sidebarVisible);
  };
  return (
    <header style={{ backgroundColor: "#fff" }}>
      {/* how-it-works */}
      <nav className="nav-bar align-items-center">
        {sidebarVisible ? <Filter onClick={handleOpenSideBar} /> : null}
        <Link to="/">
          <h4 className="navTitle">iPolicyMart</h4>
        </Link>
        <div className="option-section align-items-center">
          <div className="nav-options">
            <ul>
              <a href="/#choose-us">
                <li>Compare insurance quotes</li>
              </a>
              <a href="/#about-us">
                <li>About us</li>
              </a>
              <li>Contact us</li>
            </ul>
          </div>

          <div>
            <HideMobile>
              {currentUser ? (
                <Link to="/users-dashboard">
                  <small style={{ textTransform: "capitalize" }}>
                    hi ,{currentUser.user.firstName}
                  </small>
                </Link>
              ) : (
                <Link to="/login">
                  <button className="btn2">Login</button>
                </Link>
              )}
            </HideMobile>
          </div>
        </div>
        <MenuIconDiv
          onClick={() => {
            handleOpenSideBar();
          }}
        >
          <MenuIcon size="large" style={{ fontSize: "40px" }} />
        </MenuIconDiv>
        {sidebarVisible ? (
          <SideNav>
            <ClearIcon
              onClick={() => {
                handleOpenSideBar();
              }}
              style={{
                alignSelf: "flex-end",
                margin: "10px",
                fontSize: "40px",
                cursor: "pointer",
              }}
              size="large"
            />

            <ul
              onClick={() => {
                handleOpenSideBar();
              }}
              className="Nav-Colapse-listings"
            >
              <a href="/#choose-us">
                <li>Compare insurance quotes</li>
              </a>

              <li>About us</li>
              <li>Contact us</li>
            </ul>

            <br />
            <br />
            <br />
            {/* <br /> */}

            {currentUser ? (
              <Link
                onClick={() => {
                  handleOpenSideBar();
                }}
                to="/users-dashboard"
              >
                <p style={{ textTransform: "capitalize" }}>
                  hi ,{currentUser.user.firstName}
                </p>
              </Link>
            ) : (
              <button
                onClick={() => {
                  // handleOpenSideBar();
                  history.push("/login");
                }}
                className="btn2"
              >
                Login
              </button>
            )}
          </SideNav>
        ) : null}
      </nav>
    </header>
  );
};

export default HeaderTwo;

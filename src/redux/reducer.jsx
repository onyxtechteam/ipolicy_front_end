import * as Action_types from "./actiontypes";

const init_state = { currentUser: null };
export const UserReducers = (state = init_state, action) => {
  if (action.type === Action_types.LOGINSUCCESS) {
    return { ...state, currentUser: action.payload };
  } else {
    if (action.type === Action_types.LOGINOUTUSER) {
      return { ...state, currentUser: null };
    }
    if (action.type === Action_types.SYNCUSERDATA) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          user: action.payload.user,
        },
      };
    } else {
      return state;
    }
  }
};

const Carstep_initstate = {
  carNumber: "",
  coverType: "",
  carBrand: "",
  carModel: "",
  carFuelType: "",
  carVarient: "",
  previousClaims: "",
  Provide_No_Claim: "",
  carManufactYear: "",
  carRegisterYear: "",
  firstName: "",
  lastName: "",
  mobileNumber: "",
  email: "",
  dob_day: "",
  dob_month: "",
  dob_year: "",
  Nationality: "",
  house_address: "",
  product_type: "",
  insurance_type: "",
};
export const CarProcessReducer = (state = Carstep_initstate, action) => {
  if (action.type === Action_types.SETCARPROCESSINFO) {
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Car insurance",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else {
    return state;
  }
};
const Bikestep_initstate = {
  bikeNumber: "",
  coverType: "",
  bikeBrand: "",
  bikeModel: "",
  bikeFuelType: "",
  bikeVarient: "",
  previousClaims: "",
  Provide_No_Claim: "",
  bikeManufactYear: "",
  bikeRegisterYear: "",
  firstName: "",
  lastName: "",
  mobileNumber: "",
  email: "",
  dob_day: "",
  dob_month: "",
  dob_year: "",
  Nationality: "",
  house_address: "",
  product_type: "",
  insurance_type: "",
};
export const BikeProcessReducer = (state = Bikestep_initstate, action) => {
  if (action.type === Action_types.SETBIKEPROCESSINFO) {
    console.log(action.payload);
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Bike insurance",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else {
    return state;
  }
};
const Tricyclestep_initstate = {
  tricycleNumber: "",
  coverType: "",
  tricycleBrand: "",
  tricycleModel: "",
  tricycleFuelType: "",
  tricycleVarient: "",
  previousClaims: "",
  Provide_No_Claim: "",
  tricycleManufactYear: "",
  tricycleRegisterYear: "",
  firstName: "",
  lastName: "",
  mobileNumber: "",
  email: "",
  dob_day: "",
  dob_month: "",
  dob_year: "",
  Nationality: "",
  house_address: "",
  product_type: "",
  insurance_type: "",
};
export const TricycleProcessReducer = (
  state = Tricyclestep_initstate,
  action
) => {
  if (action.type === Action_types.SETTRICYCLEPROCESSINFO) {
    console.log(action.payload);
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Tricycle insurance",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else {
    return state;
  }
};
const LifeInsurancestep_initstate = {
  productCatalogue: "life insurance",
};
export const LifeInsuranceProcessReducer = (
  state = LifeInsurancestep_initstate,
  action
) => {
  if (action.type === Action_types.SETLIFEINSURANCEPROCESSINFO) {
    console.log(action.payload);
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Life insurance",
    };
  } else {
    return state;
  }
};
const HomeInsurancestep_initstate = {
  productCatalogue: "Home insurance",
  title: "",
  firstName: "",
  lastName: "",
  sex: "",
  house_address: "",
  house_name: "",
  homeOwnership: "",
  cover_type: "",
  otherElectronics: "",
  otherPersonalItems: "",
  home_cost_estimation: "",
  dob: "",
  occupation: "",
  mobileNumber: "",
  email: "",
  insurance_type: "",
  non_movable_items_value: "",
  movable_items_value: "",
};
export const HomeInsuranceProcessReducer = (
  state = HomeInsurancestep_initstate,
  action
) => {
  if (action.type === Action_types.SETHOMEINSURANCEPROCESSINFO) {
    console.log(action.payload);
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Home insurance",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else {
    return state;
  }
};
const Gadgetinsurancestep_initstate = {
  productCatalogue: "Gadget insurance",
  title: "",
  firstName: "",
  lastName: "",
  sex: "",
  gadget_quantity: "",
  gadget_name: "",
  gadget_make: "",
  gadget_model: "",
  gadget_serial: "",
  cover_type: "",
  gadget_cost_estimation: "",
  dob: "",
  occupation: "",
  gadGet_type: "",
  mobileNumber: "",
  email: "",
  insurance_type: "",
};
export const GadgetInsuranceProcessReducer = (
  state = Gadgetinsurancestep_initstate,
  action
) => {
  if (action.type === Action_types.SETGADGETINSURANCEPROCESSINFO) {
    console.log(action.payload);
    return {
      ...state,
      ...action.payload,
    };
  } else {
    return state;
  }
};

const insuranceType_Initstate = { insurance_type: "" };

export const InsuranceTypeReducer = (
  state = insuranceType_Initstate,
  action
) => {
  if (action.type === Action_types.SETINSURANCETYPE) {
    return { insurance_type: action.payload };
  } else return state;
};

const health_initstate = {
  members_nos: "",
  Purchase_for: "",
  firstName: "",
  lastName: "",
  mobileNumber: "",
  email: "",
  member1: "",
  insurance_type: "",
  productCatalogue: "Health insurance",
};

export const HealthInsuranceReducer = (state = health_initstate, action) => {
  if (action.type === Action_types.SETHEALTHINSURANCEPROCESSINFO) {
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Health insurance",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else return state;
};
const TRAVEL_initstate = {
  members_nos: "",
  Purchase_for: "",
  firstName: "",
  lastName: "",
  dob_day: "",
  dob_month: "",
  dob_year: "",
  mobileNumber: "",
  productCatalogue: "Travel insurance",
  email: "",
  member1: "",
  insurance_type: "",
  country1: "",
  country2: "",
  country3: "",
  country4: "",
  tripdate_day: "",
  tripdate_month: "",
  tripdate_year: "",
  age: "",
  sex: "",
  marital_status: "",
  startDate: "",
  endDate: "",
};

export const TravelInsuranceReducer = (state = TRAVEL_initstate, action) => {
  if (action.type === Action_types.SETTRAVELINSURANCEPROCESSINFO) {
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Travel insurance",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else return state;
};
const Gadget_initstate = {
  gadGet_brand: "",
  gadget_price_value: "",
  Gadget_name: "",
  firstName: "",
  lastName: "",
  mobileNumber: "",
  email: "",
};

export const GadgetInsuranceReducer = (state = Gadget_initstate, action) => {
  if (action.type === Action_types.SETGADGETPROCESS) {
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Gadget insurance",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else return state;
};
const life_initstate = {
  firstName: "",
  lastName: "",
  mobileNumber: "",
  email: "",
  cover_type: "",
  previousClaims: "",
  Provide_No_Claim: "",
  dob_day: "",
  dob_month: "",
  dob_year: "",
  Nationality: "",
};

export const LifeInsuranceReducer = (state = life_initstate, action) => {
  if (action.type === Action_types.SETLIFEPROCESS) {
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Life insurance",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else return state;
};
const Claim_initstate = {
  firstName: "",
  lastName: "",
  mobileNumber: "",
  email: "",
  cover_type: "",
  previousClaims: "",
  Provide_No_Claim: "",
  dob_day: "",
  dob_month: "",
  dob_year: "",
  Nationality: "",
  policyNumber: "",
  policyType: "",
  startDate: "",
  endDate: "",
};

export const ClaimReducer = (state = Claim_initstate, action) => {
  if (action.type === Action_types.SETCLAIMPROCESS) {
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Claims",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else return state;
};
const Rewew_initstate = {
  policyNumber: "",
  policyType: "",
  startDate: "",
  endDate: "",
  dob_day: "",
  dob_month: "",
  dob_year: "",
  Nationality: "",
};

export const RenewPolicyReducer = (state = Rewew_initstate, action) => {
  if (action.type === Action_types.SETRENEWPROCESS) {
    return {
      ...state,
      ...action.payload,
      productCatalogue: "Renew Policy",
    };
  }
  if (action.type === Action_types.CLEARLOCALSTORAGEPROCESS) {
    return {};
  } else return state;
};

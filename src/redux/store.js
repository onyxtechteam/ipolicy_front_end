import { createStore, combineReducers } from "redux";
import {
  UserReducers,
  CarProcessReducer,
  BikeProcessReducer,
  TricycleProcessReducer,
  LifeInsuranceProcessReducer,
  HomeInsuranceProcessReducer,
  GadgetInsuranceProcessReducer,
  InsuranceTypeReducer,
  HealthInsuranceReducer,
  TravelInsuranceReducer,
  GadgetInsuranceReducer,
  LifeInsuranceReducer,
  ClaimReducer,
  RenewPolicyReducer,
} from "./reducer";
import { persistStore, persistReducer } from "redux-persist";

import storage from "redux-persist/lib/storage"; // defaults to localStorage for our  web app

const rootReducer = combineReducers({
  user: UserReducers,
  CarProcessReducer: CarProcessReducer,
  BikeProcessReducer: BikeProcessReducer,
  TricycleProcessReducer: TricycleProcessReducer,
  LifeInsuranceProcessReducer: LifeInsuranceProcessReducer,
  HomeInsuranceProcessReducer: HomeInsuranceProcessReducer,
  GadgetInsuranceProcessReducer: GadgetInsuranceProcessReducer,
  InsuranceTypeReducer: InsuranceTypeReducer,
  HealthInsuranceReducer: HealthInsuranceReducer,
  TravelInsuranceReducer: TravelInsuranceReducer,
  GadgetInsuranceReducer: GadgetInsuranceReducer,
  LifeInsuranceReducer: LifeInsuranceReducer,
  ClaimReducer: ClaimReducer,
  RenewPolicyReducer: RenewPolicyReducer,
});
const authPersistConfig = {
  key: "rootIpolicy",
  storage: storage,
  whitelist: [
    "user",
    "HealthInsuranceReducer",
    "CarProcessReducer",
    "TravelInsuranceReducer",
    "HomeInsuranceProcessReducer",
    "GadgetInsuranceReducer",
    "LifeInsuranceReducer",
    "BikeProcessReducer",
    "TricycleProcessReducer",
    "ClaimReducer",
    "RenewPolicyReducer",
    "InsuranceTypeReducer"
  ],
};
const persistedReducer = persistReducer(authPersistConfig, rootReducer);

export const store = createStore(persistedReducer);
export const persistor = persistStore(store);

import * as acttiontypes from "./actiontypes";
export const LOGINSUCCESS = (response) => {
  return {
    type: acttiontypes.LOGINSUCCESS,
    payload: response,
  };
};

export const LOGINOUTUSER = () => {
  return {
    type: acttiontypes.LOGINOUTUSER,
  };
};
export const SYNCUSERDATA = (userData) => {
  return {
    type: acttiontypes.SYNCUSERDATA,
    payload: userData,
  };
};

export const SETCARPROCESSINFO = (data) => {
  return {
    type: acttiontypes.SETCARPROCESSINFO,
    payload: data,
  };
};
export const SETBIKEPROCESSINFO = (data) => {
  return {
    type: acttiontypes.SETBIKEPROCESSINFO,
    payload: data,
  };
};
export const SETTRICYCLEPROCESSINFO = (data) => {
  return {
    type: acttiontypes.SETTRICYCLEPROCESSINFO,
    payload: data,
  };
};
export const SETLIFEINSURANCEPROCESSINFO = (data) => {
  return {
    type: acttiontypes.SETLIFEINSURANCEPROCESSINFO,
    payload: data,
  };
};
export const SETHOMEINSURANCEPROCESSINFO = (data) => {
  return {
    type: acttiontypes.SETHOMEINSURANCEPROCESSINFO,
    payload: data,
  };
};
export const SETGADGETINSURANCEPROCESSINFO = (data) => {
  return {
    type: acttiontypes.SETGADGETINSURANCEPROCESSINFO,
    payload: data,
  };
};
export const SETINSURANCETYPE = (data) => {
  return {
    type: acttiontypes.SETINSURANCETYPE,
    payload: data,
  };
};
export const SETHEALTHSURANCEPROCESSINFO = (data) => {
  return {
    type: acttiontypes.SETHEALTHINSURANCEPROCESSINFO,
    payload: data,
  };
};
export const SETTRAVELINSURANCEPROCESSINFO = (data) => {
  return {
    type: acttiontypes.SETTRAVELINSURANCEPROCESSINFO,
    payload: data,
  };
};
export const CLEARLOCALSTORAGEPROCESS = () => {
  return {
    type: acttiontypes.CLEARLOCALSTORAGEPROCESS,
    payload: {},
  };
};
export const SETGADGETPROCESS = (data) => {
  return {
    type: acttiontypes.SETGADGETPROCESS,
    payload: data,
  };
};
export const SETLIFEPROCESS = (data) => {
  return {
    type: acttiontypes.SETLIFEPROCESS,
    payload: data,
  };
};
export const SETCLAIMPROCESS = (data) => {
  return {
    type: acttiontypes.SETCLAIMPROCESS,
    payload: data,
  };
};
export const SETRENEWPROCESS = (data) => {
  return {
    type: acttiontypes.SETRENEWPROCESS,
    payload: data,
  };
};

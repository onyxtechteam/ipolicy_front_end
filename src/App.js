import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Homepage from "./components/pages/homepage";
import BusinessHome from "./components/pages/businessHome";
import PersonalHome from "./components/pages/personalHome";
import AutoHome from "./components/pages/AutoHome";
import SuccessPurchase from "./components/pages/successPurchase";
import Carstepwrapper from "./components/pages/carinsurance/Carstepwrapper ";
import Carstep1 from "./components/pages/carinsurance/carStep1";
import Carstep2 from "./components/pages/carinsurance/carStep2";
import Carstep3 from "./components/pages/carinsurance/carStep3";
import Carstep4 from "./components/pages/carinsurance/carStep4";
import Carstep5 from "./components/pages/carinsurance/carStep5";
import Carstep6 from "./components/pages/carinsurance/carStep6";
import Carstep7 from "./components/pages/carinsurance/carStep7";
import Quotes from "./components/pages/carinsurance/Quotes";
import CheckoutCard from "./components/pages/carinsurance/checkoutCar";
import ScrollToTop from "./ScrollToTop";
import { useSelector } from "react-redux";
import GadgetHome from "./components/pages/gadgetHome";
import HealthInsurance from "./components/pages/healthInsurance";
import HomeInsurance from "./components/pages/HomeHome";
import LifeInsurance from "./components/pages/LifeHome";
import TravelInsurance from "./components/pages/travelhome";
import Healthwrapper from "./components/pages/HealthInsurance/Health_wrapper";
import Healthstep1 from "./components/pages/HealthInsurance/HealthStep1";
import Healthstep2 from "./components/pages/HealthInsurance/HealthStep2";
import Healthstep3 from "./components/pages/HealthInsurance/HealthStep3";
import HealthQuotes from "./components/pages/HealthInsurance/Health_Quotes";
import CheckoutHealth from "./components/pages/HealthInsurance/checkoutHealth";
import Travelwrapper from "./components/pages/travelinsurance/Travel_wrapper";
import TravelStep1 from "./components/pages/travelinsurance/TravelStep1";
import TravelStep2 from "./components/pages/travelinsurance/TravelStep2";
import TravelQuotes from "./components/pages/travelinsurance/Travel_Quotes";
import CheckoutTravel from "./components/pages/travelinsurance/checkoutTravel";
import Homewrapper from "./components/pages/homeinsurance/home_wrapper";
import Homestep1 from "./components/pages/homeinsurance/HomeStep1";
import HomeStep2 from "./components/pages/homeinsurance/HomeStep2";
import HomeQuotes from "./components/pages/homeinsurance/Home_Quotes";
import CheckoutHome from "./components/pages/homeinsurance/checkoutHome";
import Login from "./components/pages/login";
import Dashboardwrapper from "./components/pages/dashboard_wrapper";
import Dashboard from "./components/pages/dashboard";
import Claims from "./components/pages/claims";
import Policies from "./components/pages/policies";
import ClaimWrapper from "./components/pages/claim/claim_wrapper";
import InsuranceClaimsdetails from "./components/pages/claim/claim_step1";
import Claimsstep2 from "./components/pages/claim/Claim_Step2";
import Claimsstep3 from "./components/pages/claim/claim_step3";
import Claimsstep4 from "./components/pages/claim/claim_step4";
import Claimsstep5 from "./components/pages/claim/claim_step5";
import Gadgetwrapper from "./components/pages/gadgetinsurance/gadget_wrapper";
import Gadgetstep1 from "./components/pages/gadgetinsurance/gadgetstep1";
import Gadgetstep2 from "./components/pages/gadgetinsurance/gadgetstep2";
import Gadgetstep3 from "./components/pages/gadgetinsurance/gadgetstep3";
import GadgetQuotes from "./components/pages/gadgetinsurance/Gadget_Quotes";
import CheckoutGadget from "./components/pages/gadgetinsurance/checkoutGadget";
import Lifewrapper from "./components/pages/lifeinsurance/life_wrapper";
import Lifestep1 from "./components/pages/lifeinsurance/lifestep1";
import Lifestep2 from "./components/pages/lifeinsurance/lifestep2";
import Lifestep3 from "./components/pages/lifeinsurance/lifeStep3";
import Lifestep4 from "./components/pages/lifeinsurance/lifestep4";
import LifeQuotes from "./components/pages/lifeinsurance/life_Quotes";
import CheckoutLife from "./components/pages/lifeinsurance/checkoutLife";
import Bikewrapper from "./components/pages/bikeinsurance/Bikewrapper ";
import Bikestep1 from "./components/pages/bikeinsurance/bikeStep1";
import Bikestep2 from "./components/pages/bikeinsurance/bikeStep2";
import Biketep3 from "./components/pages/bikeinsurance/bikeStep3";
import Bikestep4 from "./components/pages/bikeinsurance/bikeStep4";
import Bikestep5 from "./components/pages/bikeinsurance/bikeStep5";
import Bikstep6 from "./components/pages/bikeinsurance/bikeStep6";
import BikeQuotes from "./components/pages/bikeinsurance/bikeQuotes";
import CheckoutBike from "./components/pages/bikeinsurance/checkoutBike";
import Bikestep0 from "./components/pages/bikeinsurance/bikeStep0";
import Threewrapper from "./components/pages/threewheel/threewrapper ";
import Threestep0 from "./components/pages/threewheel/threeStep0";
import Threestep1 from "./components/pages/threewheel/threeStep1";
import Threestep2 from "./components/pages/threewheel/threeStep2.jsx";
import Threetep3 from "./components/pages/threewheel/threeStep3";
import Threestep4 from "./components/pages/threewheel/ThreeStep4";
import Threestep5 from "./components/pages/threewheel/threeStep5";
import Threestep6 from "./components/pages/threewheel/ThreeStep6";
import ThreeQuotes from "./components/pages/threewheel/threeQuotes";
import CheckoutThree from "./components/pages/threewheel/checkoutThree";
import RenewWrapper from "./components/pages/renewPolicy/renew_wrapper";
import Renewstep1 from "./components/pages/renewPolicy/renewStep1";
import RenewStep2 from "./components/pages/renewPolicy/renewStep2";
import RenewStep3 from "./components/pages/renewPolicy/renewStep3";
import Renewstep4 from "./components/pages/renewPolicy/renewstep4";
import Documents from "./components/pages/documents";
import Profile from "./components/pages/Profile";
import Payments from "./components/pages/payments";
import AdminPolicies from "./components/pages/admin-policies";
import AdminClaims from "./components/pages/admin-claims";
import AdminDashboard from "./components/pages/admin-dashboard";
import AdminInsuranceEntities from "./components/pages/insuranceEntiries";
import SuccessClaim from "./components/pages/successClaim";
import Carstep6s from "./components/pages/carinsurance/carStep6s";
import Bikestep5s from "./components/pages/bikeinsurance/bikeStep5s";
import Threestep5s from "./components/pages/threewheel/threeStep5s";
import Lifestep0 from "./components/pages/lifeinsurance/lifestep0";
function App() {
  const currentUser = useSelector(({ user }) => user.currentUser);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Homepage />
        </Route>
        <Route exact path="/login">
          {currentUser ? <Dashboardwrapper /> : <Login />}
        </Route>
        <Route exact path="/personal-insurance">
          <PersonalHome />
        </Route>
        <Route path="/business-insurance">
          <BusinessHome />
        </Route>
        <Route path="/auto-insurance">
          <AutoHome />
        </Route>
        <Route path="/Gadget-insurance">
          <GadgetHome />
        </Route>
        <Route path="/Home-insurance">
          <HomeInsurance />
        </Route>
        <Route path="/life-insurance">
          <LifeInsurance />
        </Route>
        <Route path="/health-insurance">
          <HealthInsurance />
        </Route>
        <Route path="/Travel-insurance">
          <TravelInsurance />
        </Route>
        <Route path="/Health-insurance-step1">
          <Healthwrapper>
            <Healthstep1 />
          </Healthwrapper>
        </Route>
        <Route path="/Health-insurance-step2">
          <Healthwrapper>
            <Healthstep2 />
          </Healthwrapper>
        </Route>
        <Route path="/Health-insurance-step3">
          <Healthwrapper>
            <Healthstep3 />
          </Healthwrapper>
        </Route>
        <Route path="/Health-insurance-step4">
          <HealthQuotes />
        </Route>
        <Route path="/Health-insurance-checkout">
          <CheckoutHealth />
        </Route>
        <Route path="/home-insurance-step1">
          <Homewrapper>
            <Homestep1 />
          </Homewrapper>
        </Route>
        <Route path="/home-insurance-step2">
          <Homewrapper>
            <HomeStep2 />
          </Homewrapper>
        </Route>
        <Route path="/home-insurance-step3">
          <HomeQuotes />
        </Route>
        <Route path="/home-insurance-checkout">
          <CheckoutHome />
        </Route>
        <Route path="/life-insurance-step0">
          <Lifewrapper>
            <Lifestep0 />
          </Lifewrapper>
        </Route>
        <Route path="/life-insurance-step1">
          <Lifewrapper>
            <Lifestep1 />
          </Lifewrapper>
        </Route>
        <Route path="/life-insurance-step2">
          <Lifewrapper>
            <Lifestep2 />
          </Lifewrapper>
        </Route>
        <Route path="/life-insurance-step3">
          <Lifewrapper>
            <Lifestep3 />
          </Lifewrapper>
        </Route>
        <Route path="/life-insurance-step4">
          <Lifewrapper>
            <Lifestep4 />
          </Lifewrapper>
        </Route>
        <Route path="/life-insurance-Quotes">
          <LifeQuotes />
        </Route>
        <Route path="/life-insurance-checkout">
          <CheckoutLife />
        </Route>
        <Route path="/travel-insurance-step1">
          <Travelwrapper>
            <TravelStep1 />
          </Travelwrapper>
        </Route>
        <Route path="/travel-insurance-step2">
          <Travelwrapper>
            <TravelStep2 />
          </Travelwrapper>
        </Route>
        <Route path="/travel-insurance-step3">
          <TravelQuotes />
        </Route>
        <Route path="/travel-insurance-checkout">
          <CheckoutTravel />
        </Route>
        <Route path="/car-insurance-step1">
          <Carstepwrapper>
            <Carstep1 />
          </Carstepwrapper>
        </Route>
        <Route path="/car-insurance-step2">
          <Carstepwrapper>
            <Carstep2 />
          </Carstepwrapper>
          ;
        </Route>
        <Route path="/car-insurance-step3">
          <Carstepwrapper>
            <Carstep3 />
          </Carstepwrapper>
          ;
        </Route>
        <Route path="/car-insurance-step4">
          <Carstepwrapper>
            <Carstep4 />
          </Carstepwrapper>
          ;
        </Route>
        <Route path="/car-insurance-step5">
          <Carstepwrapper>
            <Carstep5 />
          </Carstepwrapper>
        </Route>
        <Route path="/car-insurance-step6">
          <Carstepwrapper>
            <Carstep6 />
          </Carstepwrapper>
        </Route>
        <Route path="/car-insurance-step6s">
          <Carstepwrapper>
            <Carstep6s />
          </Carstepwrapper>
        </Route>
        <Route path="/car-insurance-step7">
          <Carstepwrapper>
            <Carstep7 />
          </Carstepwrapper>
        </Route>
        <Route path="/car-insurance-step8">
          <Quotes />
        </Route>
        <Route path="/car-insurance-checkout">
          <CheckoutCard />
        </Route>
        <Route path="/three-insurance-step0">
          <Threewrapper>
            <Threestep0 />
          </Threewrapper>
        </Route>
        <Route path="/three-insurance-step1">
          <Threewrapper>
            <Threestep1 />
          </Threewrapper>
        </Route>
        <Route path="/three-insurance-step2">
          <Threewrapper>
            <Threestep2 />
          </Threewrapper>
        </Route>
        <Route path="/three-insurance-step3">
          <Threewrapper>
            <Threetep3 />
          </Threewrapper>
        </Route>
        <Route path="/three-insurance-step4">
          <Threewrapper>
            <Threestep4 />
          </Threewrapper>
        </Route>
        <Route path="/three-insurance-step5">
          <Threewrapper>
            <Threestep5 />
          </Threewrapper>
        </Route>
        <Route path="/three-insurance-step5s">
          <Threewrapper>
            <Threestep5s />
          </Threewrapper>
        </Route>
        <Route path="/three-insurance-step6">
          <Threewrapper>
            <Threestep6 />
          </Threewrapper>
        </Route>
        <Route path="/three-insurance-quotes">
          <ThreeQuotes />
        </Route>
        <Route path="/three-insurance-checkout">
          <CheckoutThree />
        </Route>
        <Route path="/bike-insurance-step0">
          <Bikewrapper>
            <Bikestep0 />
          </Bikewrapper>
        </Route>
        <Route path="/renew-policy-step1">
          <RenewWrapper>
            <Renewstep1 />
          </RenewWrapper>
        </Route>
        <Route path="/renew-policy-step2">
          <RenewWrapper>
            <RenewStep2 />
          </RenewWrapper>
        </Route>
        <Route path="/renew-policy-step3">
          <RenewWrapper>
            <RenewStep3 />
          </RenewWrapper>
        </Route>
        <Route path="/renew-policy-step4">
          <RenewWrapper>
            <Renewstep4 />
          </RenewWrapper>
        </Route>
        <Route path="/bike-insurance-step1">
          <Bikewrapper>
            <Bikestep1 />
          </Bikewrapper>
        </Route>
        <Route path="/bike-insurance-step2">
          <Bikewrapper>
            <Bikestep2 />
          </Bikewrapper>
        </Route>
        <Route path="/bike-insurance-step3">
          <Bikewrapper>
            <Biketep3 />
          </Bikewrapper>
        </Route>
        <Route path="/bike-insurance-step4">
          <Bikewrapper>
            <Bikestep4 />
          </Bikewrapper>
        </Route>
        <Route path="/bike-insurance-step5">
          <Bikewrapper>
            <Bikestep5 />
          </Bikewrapper>
        </Route>
        <Route path="/bike-insurance-step5s">
          <Bikewrapper>
            <Bikestep5s />
          </Bikewrapper>
        </Route>
        <Route path="/bike-insurance-step6">
          <Bikewrapper>
            <Bikstep6 />
          </Bikewrapper>
        </Route>
        <Route path="/bike-insurance-quotes">
          <BikeQuotes />
        </Route>
        <Route path="/bike-insurance-checkout">
          <CheckoutBike />
        </Route>
        <Route path="/users-dashboard">
          {!currentUser ? (
            <Login />
          ) : (
            <Dashboardwrapper>
              {currentUser.user.isAdmin ? <AdminDashboard /> : <Dashboard />}
            </Dashboardwrapper>
          )}
        </Route>
        <Route path="/users-claims">
          {!currentUser ? (
            <Login />
          ) : (
            <Dashboardwrapper>
              {currentUser.user.isAdmin ? <AdminClaims /> : <Claims />}
            </Dashboardwrapper>
          )}
        </Route>
        <Route path="/users-insurance-entities">
          {!currentUser ? (
            <Login />
          ) : (
            <Dashboardwrapper>
              {currentUser.user.isAdmin ? <AdminInsuranceEntities /> : <div />}
            </Dashboardwrapper>
          )}
        </Route>
        <Route path="/users-document">
          {!currentUser ? (
            <Login />
          ) : (
            <Dashboardwrapper>
              <Documents />
            </Dashboardwrapper>
          )}
        </Route>
        <Route path="/users-policies" render={(props)=>
          !currentUser ? 
            <Login />:
            <Dashboardwrapper>
              {currentUser.user.isAdmin ? <AdminPolicies {...props} /> : <Policies />}
            </Dashboardwrapper>
          }
        />
        <Route path="/users-profile">
          {!currentUser ? (
            <Login />
          ) : (
            <Dashboardwrapper>
              <Profile />
            </Dashboardwrapper>
          )}
        </Route>
        <Route path="/users-payments">
          {!currentUser ? (
            <Login />
          ) : (
            <Dashboardwrapper>
              <Payments />
            </Dashboardwrapper>
          )}
        </Route>
        <Route path="/insurance-claims">
          <ClaimWrapper>
            <InsuranceClaimsdetails />
          </ClaimWrapper>
        </Route>
        <Route path="/insurance-claims2">
          <ClaimWrapper>
            <Claimsstep2 />
          </ClaimWrapper>
        </Route>
        <Route path="/insurance-claims3">
          <ClaimWrapper>
            <Claimsstep3 />
          </ClaimWrapper>
        </Route>
        <Route path="/insurance-claims4">
          <ClaimWrapper>
            <Claimsstep4 />
          </ClaimWrapper>
        </Route>
        <Route path="/insurance-claims5">
          <ClaimWrapper>
            <Claimsstep5 />
          </ClaimWrapper>
        </Route>
        <Route path="/insurance-claims6">
     
            <SuccessClaim  />
        
        </Route>
        <Route path="/Gadget-insurance-step1">
          <Gadgetwrapper>
            <Gadgetstep1 />
          </Gadgetwrapper>
        </Route>
        <Route path="/Gadget-insurance-step2">
          <Gadgetwrapper>
            <Gadgetstep2 />
          </Gadgetwrapper>
        </Route>
        <Route path="/Gadget-insurance-step3">
          <Gadgetwrapper>
            <Gadgetstep3 />
          </Gadgetwrapper>
        </Route>
        <Route path="/Gadget-insurance-Quotes">
          <GadgetQuotes />
        </Route>
        <Route path="/Gadget-insurance-checkout">
          <CheckoutGadget />
        </Route>
        <Route path="/purchase-success">
          <SuccessPurchase />
        </Route>
        {/* <Route path="/car-insurance">
          <Carstepwrapper />;
        </Route> */}
      </Switch>
    </Router>
  );
}

export default App;
